<?php

defined('YII_DEBUG') or define('YII_DEBUG', false);

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'	 => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name'		 => 'GetMeet',

// preloading 'log' component
	'preload' => array('log'),

// autoloading model and component classes
	'import' => array(
		'application.models.*',
		'application.models.auto.*',
		'application.components.*',
	),

	'modules' => array(
		'api'	 => array(
			'defaultController' => 'default',
		),
		'admin',
		'deploy',
		// uncomment the following to enable the Gii tool
		/*
		  'gii'=>array(
		  'class'=>'system.gii.GiiModule',
		  'password'=>'Enter Your Password Here',
		  // If removed, Gii defaults to localhost only. Edit carefully to taste.
		  'ipFilters'=>array('127.0.0.1','::1'),
		  ),
		 */
		'gii'	 => array(
			'class'		 => 'system.gii.GiiModule',
			'password'	 => 'q1w2e3',
		),
	),
	// application components
	'components' => array(
		//enable URLs in path-format
		'urlManager' => array(
			'urlFormat'	 => 'path',
			'showScriptName' => false,
			'rules'		 => require(dirname(__FILE__) . '/route.php'),
		),
		// database settings are configured in database.php
		'db' => require(dirname(__FILE__) . '/database.php'),

		'errorHandler' => array(
			// use 'site/error' action to display errors
			'errorAction' => YII_DEBUG ? null : 'site/error',
		),

		'log' => array(
			'class'	 => 'CLogRouter',
			'routes' => array(
				array(
					'class'	 => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
			// uncomment the following to show log messages on web pages
			/*
			  array(
			  'class'=>'CWebLogRoute',
			  ),
			 */
			),
		),

		'curl' => array(
			'class' => 'ext.curl.Curl',
			'options' => array(/* additional curl options */),
		),
	),
	
	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params' => array(
		// this is used in contact page
		'adminEmail' => 'webmaster@example.com',
		
		'firebaseKey' => 'AAAAxpK9DpY:APA91bEDUbyCcnt1QpzL-MHNYxat1js5Voc0kYS3XZwQ13xmi5p6JixGbfXvanyPvs_bKlTFia7QkotRQEr6YC0cqV3FC511hvXjauqzdqAOXac6RTiP2U5s7AQ6vJstJokd1CdOxPjwhIoCHvmleYBuZohfr2M_CA',
		
		'pubnub-pub-key' => 'pub-c-e5b763d3-8052-458c-bdb6-e0f232ce7818',
		'pubnub-sub-key' => 'sub-c-76748a76-be5e-11e6-b38f-02ee2ddab7fe',
		'pubnub-sec-key' => 'sec-c-NjQ2OTUwNmUtODY3MS00YTQxLTg1NDAtNTU4MDllYTE1NmZh',
		'pubnub-message' => 'Вас приветствует команда Getmeet, добро пожаловать в приложение!
Вы являетесь одним из первых пользователей и нам очень ценно получить Вашу обратную связь. Своими комментариями вы можете внести вклад в продукт и сделать его лучше! Делитесь вашим мнением и идеями о приложении в этом чате!',

		'image-resize-w' => 1920,
		'image-resize-h' => 1080,
	),
);
