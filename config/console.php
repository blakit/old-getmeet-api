<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',

	// preloading 'log' component
	'preload'=>array('log'),
	
	'import' => array(
		'application.models.*',
		'application.models.auto.*',
		'application.components.*',
		'application.modules.api.*',
		'application.modules.api.components.*',
	),

	'modules' => array(
		'api'	 => array(
			'defaultController' => 'default',
		),
	),

	// application components
	'components'=>array(

		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params' => array(
		// this is used in contact page
		'adminEmail' => 'webmaster@example.com',
		'firebaseKey' => 'AAAA0HRtBGE:APA91bFWKKouNx7orNisEAHvYP3qCArXHHJZZjIOCLPyc1YvNUDG6cy12a85bnT-NYPmtiRG3i-OuO9r1kZFn7qzONRN0XKf747k7qyYLek0J_8zv_rCDIt0iKLGWgov2ERTS9CbVgXvGDqR_z50TIJQjKmqwOCUWQ',
	),

	'commandMap' => array(
		'test' => array(
			'class' => 'application.modules.api.commands.TestCommand',
		),
	),
);
