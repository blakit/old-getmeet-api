<?php

$config = array(

);

$config = CMap::mergeArray(
	require(dirname(__FILE__).'/main.php'),
	$config
);



if (file_exists(dirname(__FILE__) .'/local.php')) {
	$config = CMap::mergeArray(
		$config,
		require(dirname(__FILE__) .'/local.php')
	);
}

return $config;
