<?php

// change the following paths if necessary
define('ROOT_PATH', dirname(__FILE__) . '/../');

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

$yiidir = ROOT_PATH . 'protected/';

// change the following paths if necessary
$config = $yiidir . 'config/main.php';

if (file_exists($yiidir . 'vendor/autoload.php')) {
	require_once($yiidir . 'vendor/autoload.php');
}




$config=dirname(__FILE__).'/config/console.php';

if (file_exists($yiidir . 'vendor/yiisoft/yii/framework/yiic.php')) {
	require_once($yiidir . 'vendor/yiisoft/yii/framework/yiic.php');
} else {
	die('please run composer install');
}
