<?php

class SettingsController extends Controller
{
	public $layout = 'clear';


	public function actionUpdate()
	{
		$settings = [
			Settings::ITEM_CERT,
			Settings::ITEM_DISTANCE_MAX,
			Settings::ITEM_GROUP_TIMEOUT,
			Settings::ITEM_GEO_ENABLE,
		];

		foreach ($settings as $key) {
			$value = Yii::app()->request->getParam($key);

			Settings::set($key, $value);
		}

		$this->redirect('index');
	}


	public function actionIndex()
	{
		$this->render('index');
	}


}