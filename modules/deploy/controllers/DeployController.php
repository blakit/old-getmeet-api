<?php

class DeployController extends Controller
{
	public $layout = 'clear';

	const SECRET_KEY = 'jmK3Pmmxaf4fKIQEm4G3';

	public function actionPull()
	{
		if (Yii::app()->request->getParam('secret') != self::SECRET_KEY) {
			die('secret error');
		}

		$cmd = 'cd "' . ROOT_PATH . '"; git pull;';
		
		system($cmd);
	}

}