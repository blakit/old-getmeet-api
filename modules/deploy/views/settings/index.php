<style>
	.settings-row {
		margin-bottom: 20px;
	}
</style>

<h1>Настройки</h1>
<form action="<?= $this->createUrl('update')?>" method="POST">

	<div class="settings-row">
		<p>Сертификат:</p>
		<?= CHtml::dropDownList(Settings::ITEM_CERT, Settings::get(Settings::ITEM_CERT), array('production'=>'production', 'develop'=>'develop'), array()); ?>
	</div>

	<div class="settings-row">
		<p>Таймаут пользователей в группе (сек):</p>
		<?= CHtml::textField(Settings::ITEM_GROUP_TIMEOUT, Settings::get(Settings::ITEM_GROUP_TIMEOUT)); ?>
	</div>

	<div class="settings-row">
		<p>Максимальное расстояние между участниками в группе (метры):</p>
		<?= CHtml::textField(Settings::ITEM_DISTANCE_MAX, Settings::get(Settings::ITEM_DISTANCE_MAX)); ?>
	</div>

	<div class="settings-row">
		<p>Геолокация включена:</p>
		<?= CHtml::dropDownList(Settings::ITEM_GEO_ENABLE, Settings::get(Settings::ITEM_GEO_ENABLE), array('0'=>'disable', '1'=>'enable'), array()); ?>
	</div>

	<button type="submit">Сохранить</button>
</form>