<?php

class EventController extends BaseAdminController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';



	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	private function imageResize($image)
	{
		if (!$image || !file_exists($image->getPath())) {
			return false;
		}

		try {
			$resizer = new \Eventviva\ImageResize($image->getPath());
			$resizer->resizeToBestFit(Yii::app()->params['image-resize-w'], Yii::app()->params['image-resize-h']);
			$resizer->save($image->getPath());
		} catch (Exception $ex) {
			return false;
		}

		return true;
	}

	/**
	 *
	 * @param Event $model
	 * @param type $name
	 */
	public function uploadImage(&$model, $name)
	{
		if (Yii::app()->request->getParam($name . 'remove_')) {
			if ($model->image) {
				$model->image->delete();
			}

			$model->image_id = null;
			$model->image = null;
		}


		if ( ($image = Image::upload($name, $this->getUser(), true)) ) {
			if ($model->image) {
				$model->image->delete();
			}

			$this->imageResize($image);

			$model->image = $image;
			$model->image_id = $image->id;

			return true;
		}

		return false;
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Event;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Event']))
		{
			$model->attributes = $_POST['Event'];
			$model->admin_user_id = $this->getUserId();
			$model->created_date = DateHelper::TimeSql();

			// Тип события
			if (empty($model->type)) {
				$model->type = substr($model->category_id, 0, 1);
			}

			$this->uploadImage($model, 'image_');
			
			if ($model->save()) {
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Event']))
		{
			$model->attributes=$_POST['Event'];

			$this->uploadImage($model, 'image_');
			
			$model->type = substr($model->category_id, 0, 1);

			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->redirect(array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Event('search');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Event']))
			$model->attributes=$_GET['Event'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}


	/**
	 * Manages all models.
	 */
	public function actionImages($id)
	{
		$this->render('images',array(
			'model'=>$this->loadModel($id),
		));
	}



	/**
	 * Manages all models.
	 */
	public function actionUpload($id)
	{
		$model = $this->loadModel($id);

		$names = array('image_1', 'image_2', 'image_3', 'image_4', 'image_5');

		foreach ($names as $name) {
			if ( ($image = Image::upload($name, $this->getUser(), true)) ) {
				$this->imageResize($image);
				
				$insert = new EventImage();
				$insert->image_id = $image->id;
				$insert->event_id = $model->id;
				$insert->save();
			}
		}

		$this->redirect(array('images', 'id'=>$model->id));
	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Event the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Event::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Event $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='event-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
