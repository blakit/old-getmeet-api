<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $userID = null;
	private $user = null;
	
	public function authenticate()
	{
		/* @var $record Admin */
		$record = User::model()->findByAttributes(array('email' => $this->username));

		if ($record === null) {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		} else if (!CPasswordHelper::verifyPassword($this->password, $record->password)) {
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		} else {
			$this->userID  = $record->id;
			
			$this->errorCode = self::ERROR_NONE;
		}


		return !$this->errorCode;
	}

	public function getId()
	{
		return $this->userID;
	}	
}