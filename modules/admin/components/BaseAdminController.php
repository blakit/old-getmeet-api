<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
abstract class BaseAdminController extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='/layouts/main';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	private static $user = null;
	/**
	 * @return UserDTO
	 * @throws ApiException on missed or invalid token
	 */
	public function getUser()
	{
		$funcLogin = function() {
			if (!Yii::app()->user || !Yii::app()->user->getId()) {
				return false;
			}
			$user = User::model()->findByPk(Yii::app()->user->getId());

			if (!$user) {
				return false;
			}

			return $user;
		};

		if (is_null(self::$user)) {
			self::$user = $funcLogin();
		}

		if (!self::$user) {
			$this->redirect(array('admin'));

			Yii::app()->end();
		}
		
		return self::$user;
	}

	public function getUserId()
	{
		return $this->getUser()->id;
	}

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl -login', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny',
				'users'=>array('?'),
			),
		);

	}
	
}