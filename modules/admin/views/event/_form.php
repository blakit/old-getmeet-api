<?php
/* @var $this EventController */
/* @var $model Event */
/* @var $form CActiveForm */

$baseUrl = Yii::app()->baseUrl;

/* @var $cs CClientScript */
$cs = Yii::app()->getClientScript();
$cs->registerCoreScript('jquery');

$cs->registerScriptFile('http://maps.google.com/maps/api/js?libraries=places&key=AIzaSyBP9ILKprZACbMsUtXOUledWpI9YUmNP8g');
$cs->registerScriptFile($baseUrl.'/js/locationpicker.jquery.js');

$cs->registerScriptFile($baseUrl.'/js/jquery.datetimepicker.js');
$cs->registerCssFile($baseUrl.'/css/jquery.datetimepicker.css');

?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'event-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>




	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows' => 6, 'cols' => 50)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>
	
<div >
	<div class="row">
		<label>Локация</label>
		<input type="text" id="us2-address" style="width: 400px; display: none;"/>

		<div id="us2" style="width: 500px; height: 400px;"></div>
	</div>
	
	<div class="row">
		<label>Широта / долгота</label>
		<?php echo $form->textField($model,'lat'); ?> <?php echo $form->textField($model,'lon'); ?>
		<?php echo $form->error($model,'lat'); ?> <?php echo $form->error($model,'lon'); ?>
	</div>



<?php /*
	<div class="row">
		<?php echo $form->labelEx($model,'location'); ?>
		<?php echo $form->textField($model,'location'); ?>
		<?php echo $form->error($model,'location'); ?>
	</div>
*/ ?>
</div>

	<div class="row">
		<label>Полный адрес</label>
		<button class="js-geo-to-address" type="button">↓ из координат в адрес</button> <button class="js-geo-to-coord" type="button">↑ из адреса в координаты</button><br />
		<?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>

	<div class="row">
		<label>Приватное событие</label>
		<?php echo $form->checkBox($model,'private'); ?>
		<?php echo $form->error($model,'private'); ?>
	</div>
<?php /*
	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->textField($model,'type'); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
		<?php echo $form->error($model,'created_date'); ?>
	</div>
*/ ?>

	<div class="row">
		<?php echo $form->labelEx($model,'start_date'); ?>
		<?php echo $form->textField($model,'start_date'); ?>
		<?php echo $form->error($model,'start_date'); ?>
	</div>

	<div class="row">
<?php
	$durDay  = floor($model->duration / 86400);
	$durHour = floor(($model->duration - ($durDay*86400)) / 3600);
	$durMin  = floor(($model->duration - ($durDay*86400) - ($durHour*3600)) / 60);
	$durSec  = floor($model->duration - ($durDay*86400) - ($durHour*3600) - ($durMin*60));
?>
		<label>дни, часы, минуты, секунды (Продолжительность события)</label>
		<input type="text" name="duration-day" value="<?= $durDay ?>" size="4" />
		<input type="text" name="duration-hour" value="<?= $durHour ?>" size="4" />
		<input type="text" name="duration-min" value="<?= $durMin ?>" size="4" />
		<input type="text" name="duration-sec" value="<?= $durSec ?>" size="4" />

		<?php echo $form->hiddenField($model,'duration'); ?>
		<?php echo $form->error($model,'duration'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'amount'); ?>
		<?php echo $form->textField($model,'amount'); ?>
		<?php echo $form->error($model,'amount'); ?>
	</div>

	<div class="row">
		<label>Стоимость/валюта</label>
		<?php echo $form->textField($model,'price'); ?> <?php echo $form->dropDownList($model, 'price_currency', ['', 'RUB', 'USD', 'EUR']); ?>
		<?php echo $form->error($model,'price'); ?>
		<?php echo $form->error($model,'price_currency'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rating'); ?>
		<?php echo $form->textField($model,'rating'); ?>
		<?php echo $form->error($model,'rating'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'website'); ?>
		<?php echo $form->textField($model,'website'); ?>
		<?php echo $form->error($model,'website'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone'); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>

	<div class="row">
<?php
$categories = EventCategory::model()->with('eventCategories')->findAll('t.parent_id IS NULL');

$list = buildCategories($categories);

function buildCategories($categories) {
	$result = [];

	foreach ($categories as $category) {

		/* @var $category EventCategory */
		if (!$category->parent_id) {
			$result[$category->name] =
				[strval($category->id) => $category->name,] 
				+ buildCategories($category->eventCategories);
		} else {
			$result[strval($category->id)] = ' - ' . $category->name;
		}
	}

	return $result;
}

?>
		<?php echo $form->labelEx($model,'category_id'); ?>

		<?php echo $form->dropDownList($model, 'category_id', $list); ?>
		<?php echo $form->error($model,'category_id'); ?>
	</div>

	<div class="row">
<?php
	if ($model->image) {
?>
		<label>Текущая картинка</label>
		<?= CHtml::image($model->image->getUrl(), '', array('width'=>'200')) ?>
		<p><input type="checkbox" name="image_remove_"> удалить картинку</p>

		<label>Новая картинка</label>
		<input type="file" name="image_" />
<?php
	} else {
?>
		<label>Картинка записи</label>
		<input type="file" name="image_" />
<?php
	}
?>
		
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<script>
$(function(){
	var $map = $('#us2').locationpicker({
		location: {
			latitude: <?= $model->isNewRecord ? '55.75372281' : $model->lat ?>,
			longitude: <?= $model->isNewRecord ? '37.61988244' : $model->lon ?>
		},
		radius: 0,
		inputBinding: {
			latitudeInput: $('#Event_lat'),
			longitudeInput: $('#Event_lon'),
			locationNameInput: $('#us2-address')
		},
		enableAutocomplete: true
	});

	$('#us2-address, #Event_lat, #Event_lon').on('keypress', function (e) {
		if(e.which === 13){
			e.preventDefault();
		}
	});

	$('#Event_duration').closest('form').submit(function(){
		var duration = 0,
			$form = $(this);

		duration += parseInt($form.find('input[name="duration-day"]').val()) * 86400;
		duration += parseInt($form.find('input[name="duration-hour"]').val()) * 3600;
		duration += parseInt($form.find('input[name="duration-min"]').val()) * 60;
		duration += parseInt($form.find('input[name="duration-sec"]').val());

		$('#Event_duration').val(duration);
	});

	$('#Event_start_date').datetimepicker({
		format:'Y-m-d H:i:s'
	});

	var geocoder = new google.maps.Geocoder();

	$('.js-geo-to-coord, .js-geo-to-address').click(function() {
		var mapObj = $map.locationpicker('map'),
			map = mapObj.map,
			marker = mapObj.marker;
			

		if ($(this).hasClass('js-geo-to-coord')) {
			geocoder.geocode({'address': $('#Event_address').val()}, function(results, status) {
				console.log(1);
				if (status === google.maps.GeocoderStatus.OK) {
					map.setCenter(results[0].geometry.location);
					marker.setPosition(results[0].geometry.location);

					google.maps.event.trigger(marker, 'dragend');
				} else {
					alert('Geocode was not successful for the following reason: ' + status);
				}
			});
		} else {
			geocoder.geocode({'location': marker.getPosition()}, function(results, status) {
				if (status === google.maps.GeocoderStatus.OK) {
					if (results[0]) {
						$('#Event_address').val(results[0].formatted_address)
					} else {
						window.alert('No results found');
					}
				} else {
					window.alert('Geocoder failed due to: ' + status);
				}
			});
		}

	});
});
</script>