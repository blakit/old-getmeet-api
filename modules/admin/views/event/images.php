<?php
/* @var $this EventController */
/* @var $model Event */

$this->breadcrumbs=array(
	'Events'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'Manage Event', 'url'=>array('admin')),
	array('label'=>'Create Event', 'url'=>array('create')),
	array('label'=>'Update Event', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Event', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	
);
?>

<h1>Manage event images #<?php echo $model->id; ?></h1>

<?php if (count($model->images)) { ?>
<table class="images-list" border="1">
	<?php foreach($model->images as $image) { ?>
	<tr>
		<td><?= CHtml::image($image->getUrl(), '', array('width'=>'200')) ?></td>
		<td>
			<?= $image->name ?> <br />
			<?= $image->date ?> <br />
			<a class="js-delete" href="<?= $this->createUrl('image/delete', array('id' => $image->id)) ?>">удалить</a>
		</td>
	</tr>
	<?php } ?>
</table>
<script>
	$(function(){
		$('.js-delete').click(function(e){
			e.preventDefault();

			$.get($(this).attr('href'), function( data ) {});

			$(this).closest('tr').slideUp();
		});
	})
</script>
<style>
	.images-list td{
		border: 1px solid darkgray;
	}
</style>
<?php } ?>



<h2>Upload new images</h2>

<form action="<?= $this->createUrl('upload', array('id' => $model->id)) ?>" method="POST" enctype="multipart/form-data">
<input type="file" name="image_1" /> <br /> <br />
<input type="file" name="image_2" /> <br /> <br />
<input type="file" name="image_3" /> <br /> <br />
<input type="file" name="image_4" /> <br /> <br />
<input type="file" name="image_5" /> <br /> <br />
<input type="submit" value="upload" />
</form>