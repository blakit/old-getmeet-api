<?php
/* @var $this InviteparamsController */
/* @var $model Inviteparams */

$this->breadcrumbs=array(
	'Inviteparams'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Inviteparams', 'url'=>array('index')),
	array('label'=>'Create Inviteparams', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#inviteparams-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Inviteparams</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'inviteparams-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'malequantity',
		'minage',
		'maxage',
		'lat',
		'lon',
		/*
		'responcetype',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
