<?php
/* @var $this InviteparamsController */
/* @var $model Inviteparams */

$this->breadcrumbs=array(
	'Inviteparams'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Inviteparams', 'url'=>array('index')),
	array('label'=>'Manage Inviteparams', 'url'=>array('admin')),
);
?>

<h1>Create Inviteparams</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>