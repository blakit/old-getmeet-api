<?php
/* @var $this InviteparamsController */
/* @var $model Inviteparams */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'inviteparams-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
		<?php echo $form->error($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'malequantity'); ?>
		<?php echo $form->textField($model,'malequantity'); ?>
		<?php echo $form->error($model,'malequantity'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'minage'); ?>
		<?php echo $form->textField($model,'minage'); ?>
		<?php echo $form->error($model,'minage'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'maxage'); ?>
		<?php echo $form->textField($model,'maxage'); ?>
		<?php echo $form->error($model,'maxage'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lat'); ?>
		<?php echo $form->textField($model,'lat'); ?>
		<?php echo $form->error($model,'lat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lon'); ?>
		<?php echo $form->textField($model,'lon'); ?>
		<?php echo $form->error($model,'lon'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'responcetype'); ?>
		<?php echo $form->textField($model,'responcetype'); ?>
		<?php echo $form->error($model,'responcetype'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->