<?php
/* @var $this InviteparamsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Inviteparams',
);

$this->menu=array(
	array('label'=>'Create Inviteparams', 'url'=>array('create')),
	array('label'=>'Manage Inviteparams', 'url'=>array('admin')),
);
?>

<h1>Inviteparams</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
