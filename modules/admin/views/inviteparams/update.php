<?php
/* @var $this InviteparamsController */
/* @var $model Inviteparams */

$this->breadcrumbs=array(
	'Inviteparams'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Inviteparams', 'url'=>array('index')),
	array('label'=>'Create Inviteparams', 'url'=>array('create')),
	array('label'=>'View Inviteparams', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Inviteparams', 'url'=>array('admin')),
);
?>

<h1>Update Inviteparams <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>