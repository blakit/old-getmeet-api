<?php
/* @var $this InviteparamsController */
/* @var $model Inviteparams */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'malequantity'); ?>
		<?php echo $form->textField($model,'malequantity'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'minage'); ?>
		<?php echo $form->textField($model,'minage'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'maxage'); ?>
		<?php echo $form->textField($model,'maxage'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lat'); ?>
		<?php echo $form->textField($model,'lat'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lon'); ?>
		<?php echo $form->textField($model,'lon'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'responcetype'); ?>
		<?php echo $form->textField($model,'responcetype'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->