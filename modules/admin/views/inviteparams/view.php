<?php
/* @var $this InviteparamsController */
/* @var $model Inviteparams */

$this->breadcrumbs=array(
	'Inviteparams'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Inviteparams', 'url'=>array('index')),
	array('label'=>'Create Inviteparams', 'url'=>array('create')),
	array('label'=>'Update Inviteparams', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Inviteparams', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Inviteparams', 'url'=>array('admin')),
);
?>

<h1>View Inviteparams #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'malequantity',
		'minage',
		'maxage',
		'lat',
		'lon',
		'responcetype',
	),
)); ?>
