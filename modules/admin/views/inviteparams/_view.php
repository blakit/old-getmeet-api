<?php
/* @var $this InviteparamsController */
/* @var $data Inviteparams */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('malequantity')); ?>:</b>
	<?php echo CHtml::encode($data->malequantity); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('minage')); ?>:</b>
	<?php echo CHtml::encode($data->minage); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('maxage')); ?>:</b>
	<?php echo CHtml::encode($data->maxage); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lat')); ?>:</b>
	<?php echo CHtml::encode($data->lat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lon')); ?>:</b>
	<?php echo CHtml::encode($data->lon); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('responcetype')); ?>:</b>
	<?php echo CHtml::encode($data->responcetype); ?>
	<br />


</div>