<?php

class AdminModule extends CWebModule
{
	public function init()
	{
		$this->setImport(array(
			'admin.models.*',
			'admin.components.*',
		));

		Yii::app()->user->loginUrl = Yii::app()->createUrl('/admin/site/login');
		Yii::app()->user->setStateKeyPrefix('_admin');
		Yii::app()->user->allowAutoLogin = true;
		
		Yii::app()->user->init();

	}
}
