<?php


class InviteController extends ApiController
{

	/**
	* @api {post} /invite/create Создать InviteDTO
	* @apiName InviteCreate
	* @apiGroup Invite
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/invite/create
	* POST: {InviteDTO}
	*
	* @apiParam {json} object InviteDTO
	*
	* @apiSuccess (Ответ) {json} object Созданный объект InviteDTO
	*
	* @apiSuccessExample Пример ответа
	*     InviteDTO
	*/
	public function actionCreate()
	{
		$data = Yii::app()->request->getRawBody();

		if (!$data) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed data body');
		}

		$object = new InviteDTO();

		if (!$object->fromJson($data, true)) {
			throw new ApiException(self::ERR_OBJECT_INVALID, 'Can not parse object');
		}

		$object->owner_id = $this->getUser()->id;

		$object->save();

		return $this->renderJson($object);
	}


	/**
	* @api {post} /invite/update Изменить InviteDTO
	* @apiName InviteUpdate
	* @apiGroup Invite
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/invite/update?id=[id]
	* POST: {InviteDTO}
	*
	* @apiParam {integer} id ID Invite
	* @apiParam {json} object InviteDTO
	*
	* @apiSuccess (Ответ) {json} object Изменненый объект InviteDTO
	*
	* @apiSuccessExample Пример ответа
	*     InviteDTO
	*/
	public function actionUpdate()
	{
		$objectId = Yii::app()->request->getParam('id');

		if (!$objectId) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed object id');
		}

		$data = Yii::app()->request->getRawBody();

		if (!$data) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed data body');
		}

		/* @var $object InviteDTO */
		$object = InviteDTO::model()->findByPk($objectId);

		if (!$object) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'Specified object is not found');
		}

		if ($object->admin_user_id != $this->getUser()->id) {
			throw new ApiException(self::ERR_OBJECT_ACCESS, 'Can not access to this object');
		}

		if (!$object->fromJson($data)) {
			throw new ApiException(self::ERR_OBJECT_INVALID, 'Can not parse object');
		}

		$object->owner_id= $this->getUser()->id;

		$object->save();

		return $this->renderJson($object);
	}


	/**
	 * @api {post} /invite/get Получить InviteDTO
	 * @apiName InviteGet
	 * @apiGroup Invite
	 *
	 * @apiUse ResponseHeaderAuth
	 *
	 * @apiExample Пример запроса
	 * /api/invite/get?id=[id]
	 *
	 * @apiParam {integer} id ID Invite
	 *
	 * @apiSuccess (Ответ) {json} object Объект InviteDTO
	 *
	 * @apiSuccessExample Пример ответа
	 *     InviteDTO
	 */
	public function actionGet()
	{
		$objectId = Yii::app()->request->getParam('id');

		if (!$objectId) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed object id');
		}

		/* @var $object InviteDTO */
		$object = InviteDTO::model()->findByPk($objectId);

		if (!$object) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'The specified object is not found');
		}

		return $this->renderJson($object);
	}
	
}

