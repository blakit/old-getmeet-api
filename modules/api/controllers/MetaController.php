<?php

class MetaController extends ApiController
{
	/**
	 * @api {get} /meta/full Мета данные
	 * @apiName MetaFull
	 * @apiGroup Meta
	 * @apiDescription Все возможные метаданные. Будет дополнятся.
	 *
	 * @apiSuccess (Ответ) {array} profileAppearance Массив ProfileAppearanceDTO (см /user/metaAppearances)
	 * @apiSuccess (Ответ) {array} profileRelation Массив ProfileRelationDTO (см /user/metaRelations)
	 * @apiSuccess (Ответ) {array} profileLanguage Массив ProfileLanguageDTO (см /user/metaLanguages)
	 * @apiSuccess (Ответ) {array} profileLike Массив заготовленных строк для ProfileDTO->like
	 * @apiSuccess (Ответ) {array} eventCategories Массив EventCategoryDTO (см /event/metaCategories)
	 * @apiSuccess (Ответ) {array} eventTypes Массив типов событий (только авторизованным)
	 *
	 * @apiExample Пример запроса
	 *     /api/meta/full
	 *
	 * @apiSuccessExample Пример ответа
	 *     {
	 *         profileAppearance: [ProfileAppearanceDTO,ProfileAppearanceDTO,...],
	 *         profileRelation: [ProfileRelationDTO,ProfileRelationDTO,...],
	 *         profileLanguage: [ProfileLanguageDTO,ProfileLanguageDTO,...],
	 *         profileLike: ['String', 'String', ...],
	 *         eventCategories: [EventCategoryDTO,EventCategoryDTO,...]
	 *         eventTypes: [1:{...}, 2:{...}, ...]
	 *     }
	 */
	public function actionFull()
	{
		$objectList = EventCategoryDTO::model()->findAll(['order'=>'parent_id DESC']);

		$objectList = array_map(function(EventCategoryDTO $item) {
			$item->setOutputFull(false);
			return $item;
		}, $objectList);


		return $this->renderJson(
			[
				'profileAppearance' => ProfileAppearanceDTO::model()->findAll(),
				'profileRelation' => ProfileRelationDTO::model()->findAll(),
				'profileLanguage' => ProfileLanguageDTO::model()->findAll(),
				'profileLike' => $this->getProfileLike(),
				'eventCategories' => $objectList,
				'eventTypes' => $this->getEventsType(),
			]
		);
	}

	/**
	 * @api {get} /meta/eventTypes Мета - типы событий
	 * @apiName MetaEventTypes
	 * @apiGroup Meta
	 * @apiDescription Массив типов событий (только авторизованным)
	 * @apiUse ResponseHeaderAuth
	 * 
	 * @apiExample Пример запроса
	 *     /api/meta/eventTypes
	 *
	 * @apiSuccessExample Пример ответа
	 *     {
	 *         {...},
	 *          ...
	 *     }
	 */

	public function actionEventTypes()
	{
		return $this->renderJson(
			$this->getEventsType()
		);
	}

	protected function getProfileLike()
	{
		return [
			'Getmeet',
			'Страну',
			'Жизнь',
		];
	}


	protected function getEventsType()
	{
		$eventTypes = array();

		if ($this->isUser()) {
			$data = EventCategory::model()->findAll(' parent_id IS NULL ');

			$categories = array();
			foreach ($data as $value) {
				$categories[$value->id] = $value;
			}
			unset($data);

			if ($this->getUser()->profile->birthdate) {
				$datetime = new DateTime($this->getUser()->profile->birthdate);

				$interval = $datetime->diff(new DateTime(date("Y-m-d")));

				$myAge = intval($interval->format("%Y"));
			} else {
				$myAge = null;
			}


			if (!empty($categories[EventDTO::TYPE_SPORT])) {
				$ageMin = null;
				$ageMax = null;
				
				if (!is_null($myAge)) {
					if ($myAge < 18) {
						$ageMin = $myAge - 2;
						$ageMax = $myAge + 2;

						if ($ageMin < 12) {
							$ageMin = 12;
						}

						if ($ageMax > 18) {
							$ageMax = 18;
						}
					} else {
						$ageMin = $myAge - 10;
						$ageMax = $myAge + 10;

						if ($ageMin < 16) {
							$ageMin = 16;
						}

						if ($myAge > 80) {
							$ageMax = 80;
						}
					}
				}

				$eventTypes[] = array(
					'id'	 => EventDTO::TYPE_SPORT,
					'name'	 => $categories[EventDTO::TYPE_SPORT]->name,
					'gender' => $this->getUser()->profile->sex == '1' ? 100 : 0,
					'age_min' => $ageMin,
					'age_max' => $ageMax,
					'default_private' => false,
					'delivery_count' => 10,
				);
			}

			if (!empty($categories[EventDTO::TYPE_EVENTS])) {
				$eventTypes[] = array(
					'id'	 => EventDTO::TYPE_EVENTS,
					'name'	 => $categories[EventDTO::TYPE_EVENTS]->name,
					'gender' => 50,
					'age_min' => 18,
					'age_max' => (is_null($myAge) ? null : ($myAge < 18 ? 18 : null)),
					'default_private' => false,
					'delivery_count' => 10,
				);
			}

			if (!empty($categories[EventDTO::TYPE_BLITZ])) {
				$eventTypes[] = array(
					'id'	 => EventDTO::TYPE_BLITZ,
					'name'	 => $categories[EventDTO::TYPE_BLITZ]->name,
					'gender' => $this->getUser()->profile->sex == '1' ? 0 : 100,
					'age_min' => (is_null($myAge) ? null : ($myAge < 18 ? 18 : null)),
					'age_max' => (is_null($myAge) ? null : ($myAge < 18 ? 18 : null)),
					'default_private' => true,
					'delivery_count' => 10,
				);
			}

			if (!empty($categories[EventDTO::TYPE_ACTIVITY])) {
				$ageMin = null;
				$ageMax = null;

				if (!is_null($myAge)) {
					if ($myAge < 18) {
						$ageMin = $myAge - 2;
						$ageMax = $myAge + 2;

						if ($ageMin < 12) {
							$ageMin = 12;
						}

						if ($ageMax > 18) {
							$ageMax = 18;
						}
					} else {
						$ageMin = $myAge - 5;
						$ageMax = $myAge + 5;

						if ($ageMin < 18) {
							$ageMin = 18;
						}

						if ($myAge > 75) {
							$ageMax = 75;
						}
					}
				}

				$eventTypes[] = array(
					'id'	 => EventDTO::TYPE_ACTIVITY,
					'name'	 => $categories[EventDTO::TYPE_ACTIVITY]->name,
					'gender' => 50,
					'age_min' => $ageMin,
					'age_max' => $ageMax,
					'default_private' => false,
					'delivery_count' => 10,
				);
			}
		}

		return $eventTypes;
	}
}

