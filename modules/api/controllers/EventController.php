<?php

class EventController extends ApiController
{

	protected $controllerBaseClass = 'EventDTO';

	/**
	 *
	 * @param type $checkAccess
	 * @param type $with
	 * @return EventDTO
	 * @throws ApiException
	 */
	protected function getObject($checkAccess = false, $with = array())
	{
		/* @var $object EventDTO */
		$object = parent::getObject($with);

		if ($checkAccess) {
			if ($object->adminUser->id != $this->getUser()->id) {
				throw new ApiException(self::ERR_OBJECT_ACCESS, 'Can not access to this object');
			}
		}

		return $object;
	}
	
	public function filters()
	{
		$filters = parent::filters();

		$filters = array_merge([
			'Access -Error -metaCategories',
		], $filters);

		return $filters;
	}

	/**
	 * @api {post} /event/create EventDTO - создать
	 * @apiName EventCreate
	 * @apiGroup Event
	 *
	 * @apiUse ResponseHeaderAuth
	 *
	 * @apiExample Пример запроса
	 * /api/event/create
	 * POST: {EventDTO}
	 *
	 * @apiParam {json} object EventDTO
	 *
	 * @apiSuccess (Ответ) {json} object Созданный объект EventDTO
	 *
	 * @apiSuccessExample Пример ответа
	 *     EventDTO
	 */
	public function actionCreate()
	{
		$data = Yii::app()->request->getRawBody();

		if (!$data) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed data body');
		}

		$object = new EventDTO('create');

		$object->adminUser = $this->getUser();
		$object->admin_user_id = $this->getUser()->id;
		$object->created_date = DateHelper::TimeSql();
		
		if (!$object->fromJson($data, true)) {
			throw new ApiException(self::ERR_OBJECT_INVALID, 'Can not parse object');
		}

		$object->save();

		$eventUser = new EventUser();
		$eventUser->user_id = $this->getUser()->id;
		$eventUser->event_id = $object->id;
		$eventUser->save();

		return $this->renderJson($object);
	}

	/**
	 * @api {post} /event/update EventDTO - изменить
	 * @apiName EventUpdate
	 * @apiGroup Event
	 *
	 * @apiUse ResponseHeaderAuth
	 *
	 * @apiExample Пример запроса
	 * /api/event/update?id=[id]
	 * POST: {EventDTO}
	 *
	 * @apiParam {integer} id ID Event
	 * @apiParam {json} object EventDTO
	 *
	 * @apiSuccess (Ответ) {json} object Изменненый объект EventDTO
	 *
	 * @apiSuccessExample Пример ответа
	 *     EventDTO
	 */
	public function actionUpdate()
	{
		$data = Yii::app()->request->getRawBody();

		if (!$data) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed data body');
		}

		$object = $this->getObject(true);

		if (!$object->fromJson($data)) {
			throw new ApiException(self::ERR_OBJECT_INVALID, 'Can not parse object');
		}

		$object->adminUser = $this->getUser();
		$object->admin_user_id = $this->getUser()->id;

		$object->save();

		return $this->renderJson($object);
	}

	private function getEventLike()
	{
		$object = $this->getObject();
		$user = $this->getUser();

		$eventLike = EventLike::model()->find(
			' event_id = :event_id AND user_id = :user_id ',
			array(
				'event_id' => $object->id,
				'user_id' => $user->id,
			)
		);

		return $eventLike;
	}

	/**
	 * @api {get} /event/like EventDTO - лайк
	 * @apiName EventLike
	 * @apiGroup Event
	 * @apiDescription Если событие было ранее помечено как спам, вернется ошибка ERR_METHOD_NOT_IMPLEMENTED - Event was previously marked as spam
	 *
	 * @apiUse ResponseHeaderAuth
	 *
	 * @apiExample Пример запроса
	 * /api/event/like?id=[id]
	 *
	 * @apiParam {integer} id ID Event
	 *
	 * @apiSuccessExample Пример ответа
	 *     EventDTO
	 */
	public function actionLike()
	{
		$object		 = $this->getObject();
		$user		 = $this->getUser();
		$admin		 = $object->adminUser;
		$eventLike	 = $this->getEventLike();

		if ($eventLike && !$eventLike->like) {
			throw new ApiException(self::ERR_METHOD_NOT_IMPLEMENTED, 'Event was previously marked as spam');
		} elseif ($eventLike) {
			return $this->renderJson($object);
		}
		
		$eventLike = new EventLike;
		$eventLike->user_id = $user->id;
		$eventLike->event_id = $object->id;
		$eventLike->like = true;
		$eventLike->save();

		$object->rating_like = $object->rating_like + 1;
		$object->save();

		$admin->profile->rating = $admin->profile->rating + 1;
		$admin->profile->save();

		return $this->renderJson($object);
	}


	/**
	 * @api {get} /event/unlike EventDTO - убрать лайк
	 * @apiName EventUnlike
	 * @apiGroup Event
	 * @apiDescription Если событие было ранее помечено как спам, вернется ошибка ERR_METHOD_NOT_IMPLEMENTED - Event was previously marked as spam
	 *
	 * @apiUse ResponseHeaderAuth
	 *
	 * @apiExample Пример запроса
	 * /api/event/unlike?id=[id]
	 *
	 * @apiParam {integer} id ID Event
	 *
	 * @apiSuccessExample Пример ответа
	 *     EventDTO
	 */
	public function actionUnlike()
	{
		$object		 = $this->getObject();
		$user		 = $this->getUser();
		$admin		 = $object->adminUser;
		$eventLike	 = $this->getEventLike();

		if (!$eventLike) {
			return $this->renderJson($object);
		} elseif ($eventLike && !$eventLike->like) {
			throw new ApiException(self::ERR_METHOD_NOT_IMPLEMENTED, 'Event was previously marked as spam');
		}

		$eventLike->delete();

		$object->rating_like = $object->rating_like - 1;
		
		if ($object->rating_like < 0) {
			$object->rating_like = 0;
		}
		$object->save();

		$admin->profile->rating = $admin->profile->rating - 1;

		if ($admin->profile->rating < 0) {
			$admin->profile->rating = 0;
		}
		$admin->profile->save();

		return $this->renderJson($object);
	}


	/**
	 * @api {get} /event/spam EventDTO - жалоба на спам
	 * @apiName EventSpam
	 * @apiGroup Event
	 *
	 * @apiUse ResponseHeaderAuth
	 *
	 * @apiExample Пример запроса
	 * /api/event/spam?id=[id]
	 *
	 * @apiParam {integer} id ID Event
	 *
	 * @apiSuccessExample Пример ответа
	 *     EventDTO
	 */
	public function actionSpam()
	{
		$object		 = $this->getObject();
		$user		 = $this->getUser();
		$admin		 = $object->adminUser;
		$eventLike	 = $this->getEventLike();

		if ($eventLike && !$eventLike->like) {
			return $this->renderJson($object);
		} elseif ($eventLike && $eventLike->like) {
			$object->rating_like = $object->rating_like - 1;
			
			if ($object->rating_like < 0) {
				$object->rating_like = 0;
			}

			$admin->profile->rating = $admin->profile->rating - 1;

			if ($admin->profile->rating < 0) {
				$admin->profile->rating = 0;
			}
		} else {
			$eventLike = new EventLike;
			$eventLike->user_id = $user->id;
			$eventLike->event_id = $object->id;
		}

		$eventLike->like = false;
		$eventLike->save();

		$object->rating_spam = $object->rating_spam + 1;
		$object->save();

		$admin->profile->rating = floor($admin->profile->rating / 2);
		$admin->profile->save();

		return $this->renderJson($object);
	}


	/**
	 * @api {get} /event/likeGet EventDTO - список лайков
	 * @apiName EventLikeGet
	 * @apiGroup Event
	 * @apiDescription Получить список пользователей лайкнувших событие
	 *
	 * @apiUse ResponseHeaderAuth
	 *
	 * @apiExample Пример запроса
	 * /api/event/likeGet?id=[id]
	 *
	 * @apiParam {integer} id ID Event
	 *
	 * @apiSuccessExample Пример ответа
	 *     [PersonDTO,PersonDTO,...]
	 */
	public function actionLikeGet()
	{
		$object = $this->getObject();

		$likedList = Yii::app()->db
			->createCommand('SELECT user_id FROM event_like WHERE event_id=? AND "like"=TRUE')
			->queryColumn(array($object->id));

		$user = UserDTO::model()->findAllByPk($likedList);

		return $this->renderJson($user);
	}

	/**
	 * @api {get} /event/cancel EventDTO - отменить
	 * @apiName EventCancel
	 * @apiGroup Event
	 *
	 * @apiUse ResponseHeaderAuth
	 *
	 * @apiExample Пример запроса
	 * /api/event/cancel?id=[id]
	 *
	 * @apiParam {integer} id ID Event
	 *
	 * @apiSuccessExample Пример ответа
	 *     /без данных/
	 */
	public function actionCancel()
	{
		$object = $this->getObject(true);

		$object->is_cancel = true;
		$object->save();

		return $this->renderJson();
	}


	/**
	 * @api {get} /event/get Получить EventDTO
	 * @apiName EventGet
	 * @apiGroup Event
	 *
	 * @apiUse ResponseHeaderAuth
	 *
	 * @apiExample Пример запроса
	 * /api/event/get?id=[id]
	 *
	 * @apiParam {integer} id ID Event
	 *
	 * @apiSuccess (Ответ) {json} object Объект EventDTO
	 *
	 * @apiSuccessExample Пример ответа
	 *     EventDTO
	 */
	public function actionGet()
	{
		$object = $this->getObject(false);

		return $this->renderJson($object);
	}


	/**
	 * @api {get} /event/metaCategories Мета - Категории
	 * @apiName EventMetaCategories
	 * @apiGroup Event
	 * @apiDescription Cписок зарегистрированных категорий (EventCategoriesDTO)
	 *
	 *
	 * @apiExample Пример запроса
	 * /api/event/metaCategories
	 *
	 * @apiSuccess (Ответ) {array} array Массив EventCategoriesDTO (рекурсивное значение parent будет отсутствовать)
	 *
	 * @apiSuccessExample Пример ответа
	 *     [EventCategoryDTO,EventCategoryDTO,...]
	 */
	public function actionMetaCategories()
	{
		$objectList = EventCategoryDTO::model()->findAll(['order'=>'parent_id DESC']);

		$objectList = array_map(function(EventCategoryDTO $item) {
			$item->setOutputFull(false);
			return $item;
		}, $objectList);

		return $this->renderJson($objectList);
	}

	/**
	 * @api {get} /event/searchMy EventDTO - поиск событий текущего пользователя
	 * @apiName EventSearchMy
	 * @apiGroup Event
	 * @apiDescription Вернет события в которых участвует текущий пользователь. Синоним вызова "/event/search?search[user]={CURRENT_USER_ID}"
	 *
	 * @apiUse ResponseHeaderAuth
	 * @apiUse RequestPagination
	 * @apiUse RequestEventSearch
	 *
	 * @apiExample Пример запроса
	 * /api/event/searchMy
	 *
	 * @apiSuccess (Ответ) {array} array Массив EventDTO
	 *
	 * @apiSuccessExample Пример ответа
	 *     [EventDTO,EventDTO,...]
	 */
	public function actionSearchMy()
	{
		$criteria = $this->searchMakeCriteria($this->getUser()->id);

		$objectList = EventDTO::model()->findAll($criteria);

		array_map(
			function($item) {
				$item->setOutputFull(false);
			},
			$objectList
		);

		return $this->renderJson($objectList);
	}
	
	/**
	 * @api {get} /event/search EventDTO - поиск
	 * @apiName EventSearch
	 * @apiGroup Event
	 *
	 * @apiUse ResponseHeaderAuth
	 * @apiUse RequestPagination
	 * @apiUse RequestEventSearch
	 *
	 * @apiExample Пример запроса
	 * /api/event/search?search[location][lat]=44.7262&search[location][lon]=37.7632&search[location][radius]=2000
	 *
	 * @apiSuccess (Ответ) {array} array Массив EventDTO
	 *
	 * @apiSuccessExample Пример ответа
	 *     [EventDTO,EventDTO,...]
	 */
	public function actionSearch()
	{
		$criteria = $this->searchMakeCriteria();

		$objectList = EventDTO::model()->findAll($criteria);

		array_map(function($item) {
			$item->setOutputFull(false);
		}, $objectList);
		
		return $this->renderJson($objectList);
	}

	
	/**
	* @apiDefine RequestEventSearch
	*
	* @apiParam (Поиск) {StringInteger} [search[type]] тип мероприятия (id через запятую), 1 - Спорт, 2 - Мероприятия, 3 - Блиц, 4 - Активности
	* @apiParam (Поиск) {TimestampUTC} [search[date_start]] вывести события начинающиеся после указанной даты (включительно)
	* @apiParam (Поиск) {string} [search[string]] поиск по строке (в хештегах, в названии, в описании)
	* @apiParam (Поиск) {TimestampUTC} [search[date_end]] вывести события начинающиеся до указанной даты (включительно)
	* @apiParam (Поиск) {String} [search[cancel]] 1 - вывести отмененные события, 0 - неотмененные события, -1 - все события (default - 0),
	* @apiParam (Поиск) {String} [search[status]] old - прошедшие, new - еще не начавшиеся, 
	*	 active - идущие в данный момент, cancel - отмененные, all - все (default - all),
	*	 допускается комбинировать через запятую
	* @apiParam (Поиск) {Array} [search[location]] LocationDTO фильтр по местоположению (default для radius - 200м)
	* @apiParam (Поиск) {StringInteger} [search[category]] фильтр по категориям (id через запятую)
	* @apiParam (Поиск) {Integer} [search[user]] ID юзера - события в которых участвует юзер (если ID текущего юзера - будут включены приватные события)
	* @apiParam (Поиск) {Integer} [search[owner]] ID юзера - события которые создал указанный юзер
	*/
	private function searchMakeCriteria($userId = null)
	{
		$search = Yii::app()->request->getParam('search');
		
		if (empty($search)) {
			$search = array();
		}

		$criteria = new CDbCriteria;
		$criteria->with		 = array(
			'users_count',
			'image'				 => array('alias' => 'event_image'),
			'category'			 => array('alias' => 'event_category'),
			'adminUser'			 => array('alias' => 'event_admin'),
			'adminUser.profile'	 => array('alias' => 'event_admin_profile'),
			'adminUser.profile.image' => array('alias' => 'event_admin_profile_image'),
		);
		$criteria->together	 = true;
		$criteria->order	 = 'start_date DESC';

		$condition	 = array();
		$params		 = array();

		if (!is_null($userId)) {
			$search['user'] = $userId;
		}

		$cancelStatus = false;
		$hidePrivate = true;

		foreach ($search as $searchKey => $searchValue) {
			if (empty($searchValue)) {
				continue;
			}

			switch ($searchKey) {
				case 'cancel':
					if ($searchValue == '1') {
						$cancelStatus = true;
					} elseif ($searchValue == '-1') {
						$cancelStatus = null;
					} else {
						$cancelStatus = false;
					}
					break;

				case 'owner':
					$condition[] = ' t.admin_user_id = :admin_user_id ';
					$params[':admin_user_id'] = $searchValue;
					break;

				case 'user':
					if (intval($searchValue) == $this->getUser()->id) {
						$hidePrivate = false;
					}

					$condition[] = '(ARRAY[ :user_id ]::integer[] <@ t.cache_user)';
					$params[':user_id'] = $searchValue;
					break;
				
				case 'status':
					$searchValue = explode(',', $searchValue);
					
					if (in_array('all', $searchValue)
						|| (in_array('new', $searchValue) && in_array('old', $searchValue) && in_array('active', $searchValue) && in_array('cancel', $searchValue))
						|| !count($searchValue)
					) {
						//do nothing - all events
					} else  {
						$someSql = array();
						
						if (in_array('old', $searchValue)) {
							$someSql[] = " (t.start_date + t.duration::integer * '00:00:01'::interval) < NOW() ";
						}

						if (in_array('new', $searchValue)) {
							$someSql[] = " (t.start_date) > NOW() ";
						}

						if (in_array('active', $searchValue)) {
							$someSql[] = " ((t.start_date) < NOW() AND (t.start_date + t.duration::integer * '00:00:01'::interval) > NOW()) ";
						}

						$condition[] = ' (' . implode(' OR ', $someSql) . ') ';
					}

					break;

				case 'date_start':
					$searchValue = intval($searchValue);
					$params[':date_start'] = DateHelper::ConvUnix2Sql($searchValue);

					$condition[] = " t.start_date >= :date_start ";
					break;

				case 'date_end':
					$searchValue = intval($searchValue);
					$params[':data_end'] = DateHelper::ConvUnix2Sql($searchValue);

					$condition[] = " (t.start_date + t.duration::integer * '00:00:01'::interval) <= :date_end ";
					break;

				case 'type':
					$searchValue = explode(',', $searchValue);
					$searchValue = array_map('intval', $searchValue);

					$condition[] = "t.type IN ('" . implode("', '", $searchValue) . "')";
					break;

				case 'string':
					$condition[] = 
						"(t.hashtags ILIKE '%" . addslashes($searchValue) . "%' " .
						" OR t.name ILIKE '%" . addslashes($searchValue) . "%' " .
						" OR t.description ILIKE '%" . addslashes($searchValue) . "%')";
					break;

				case 'category':
					$searchValue = explode(',', $searchValue);
					$searchValue = array_map('intval', $searchValue);
					
					$condition[] = "t.category_id IN ('" . implode("', '", $searchValue) . "')";
					break;

				case 'location':
					$location = new LocationDTO();
					
					if (!$location->fromJson($searchValue)) {
						throw new ApiException(self::ERR_OBJECT_INVALID, 'Can not parse location');
					}

					if (!$location->radius || $location->radius < 0) {
						$location->radius = 200;
					}

					$condition[] = GisHelper::sqlRadius($location, 't.location');
					break;

				default:
					break;
			}
		}

		if (!is_null($cancelStatus)) {
			$params[':is_cancel'] = $cancelStatus;

			$condition[] = " t.is_cancel = :is_cancel ";
		}

		if ($hidePrivate) {
			$condition[] = " t.private = FALSE ";
		}

		$criteria->condition = implode(' AND ', $condition);
		$criteria->params = $params;

		$this->searchLimitOffsetCriteria($criteria);

		return $criteria;
	}


	/**
	* @api {post} /event/imageUpload Изображения - загрузить
	* @apiName EventImageUpload
	* @apiGroup Event
	*
	* @apiUse ResponseHeaderAuth
	* @apiUse ErrorUpload
	*
	* @apiParam {integer} id ID EventDTO
	*
	* @apiExample Пример запроса
	* /api/event/imageUpload?id=[event id]
	* Content-Type: multipart/form-data
	*
	* POST: file=[uploadFile]
	*
	* @apiSuccess (Ответ) {json} object Объект ImageDTO
	*
	* @apiSuccessExample Пример ответа
	*     ImageDTO
	*/
	public function actionImageUpload()
	{
		/* @var $object EventDTO */
		$object = $this->getObject();

		// TODO: проверка доступа к событию

		if ( !($file = ImageDTO::upload('file', $this->getUser())) ) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'File not specified');
		}

		$model = new EventImage();
		$model->image_id = $file->id;
		$model->event_id = $object->id;
		$model->save();

		return $this->renderJson($file);
	}


	/**
	* @api {get} /event/imageAdd Изображения - добавить
	* @apiName EventImageAdd
	* @apiGroup Event
	* @apiDescription Добавить в событие ранее загруженное изображение
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiParam {integer} id ID EventDTO
	* @apiParam {integer} image_id ID ImageDTO
	*
	* @apiExample Пример запроса
	* /api/event/imageAdd?id=[event id]&image_id=[image_id]
	*
	* @apiSuccess (Ответ) {json} object Объект EventDTO
	*
	* @apiSuccessExample Пример ответа
	*     EventDTO
	*/
	public function actionImageAdd()
	{
		/* @var $object EventDTO */
		$object = $this->getObject();

		// TODO: проверка доступа к событию

		$imageId = Yii::app()->request->getParam('image_id');
		if (!$imageId) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed image id');
		}

		$image = ImageDTO::model()->findByPk($imageId);

		if (!$image) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'The specified image is not found');
		}

		if ($image->user_id != $this->getUser()->id) {
			throw new ApiException(self::ERR_OBJECT_ACCESS, 'You can not access to this image');
		}

		$model = new EventImage();
		$model->image_id = $image->id;
		$model->event_id = $object->id;
		$model->save();

		return $this->renderJson($image);
	}

	/**
	* @api {get} /event/imageRemove Изображения - удалить
	* @apiName EventimageRemove
	* @apiGroup Event
	*
	* @apiUse ResponseHeaderAuth
	* @apiParam {integer} id ID EventDTO
	* @apiParam {integer} image_id ID ImageDTO
	* @apiParam {boolean} [full] удалить изображение физически (см image/remove)
	*
	* @apiExample Пример запроса
	* /api/event/imageRemove?id=[event_id]&image_id=[image_id]
	*
	* @apiSuccessExample Пример ответа
	*     /без данных/
	*/
	public function actionImageRemove()
	{
		/* @var $object EventDTO */
		$object = $this->getObject();

		// TODO: проверка доступа к событию

		$eventImage = EventImage::model()->findByAttributes([
			'event_id' => $object->id,
			'image_id' => Yii::app()->request->getParam('image_id')
		]);

		if (Yii::app()->request->getParam('full')) {
			$eventImage->image->delete();
		}

		$eventImage->delete();

		return $this->renderJson();
	}
	

	/**
	* @api {get} /event/imageList Изображения - список
	* @apiName EventImageList
	* @apiGroup Event
	*
	* @apiUse ResponseHeaderAuth
	* @apiParam {integer} id ID EventDTO
	*
	* @apiExample Пример запроса
	* /api/event/imageList?id=[event id]
	*
	* @apiSuccess (Ответ) {array} object Массив ImageDTO
	*
	* @apiSuccessExample Пример ответа
	*     [ImageDTO,ImageDTO,...]
	*/
	public function actionImageList()
	{
		/* @var $object EventDTO */
		$object = $this->getObject();

		return $this->renderJson($object->images);
	}
	
	/**
	* @api {post} /event/coverUpload Обложка - загрузить
	* @apiName EventCoverUpload
	* @apiGroup Event
	*
	* @apiUse ResponseHeaderAuth
	* @apiUse ErrorUpload
	*
	* @apiParam {integer} id ID Event
	*
	* @apiExample Пример запроса
	* /api/event/coverUpload?id=[event id]
	* Content-Type: multipart/form-data
	*
	* POST: file=[uploadFile]
	*
	* @apiSuccess (Ответ) {json} object Объект EventDTO
	*
	* @apiSuccessExample Пример ответа
	*     EventDTO
	*/
	public function actionCoverUpload()
	{
		/* @var $object EventDTO */
		$object = $this->getObject(true);

		if ( !($file = ImageDTO::upload('file', $this->getUser())) ) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'File not specified');
		}

		$object->image = $file;
		$object->image_id = $file->id;
		$object->save();

		return $this->renderJson($object);
	}

	/**
	* @api {get} /event/coverSet Обложка - установить
	* @apiName EventCoverSet
	* @apiGroup Event
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiParam {integer} id ID Event
	* @apiParam {integer} id ID ImageDTO
	*
	* @apiExample Пример запроса
	* /api/event/coverSet?id=[event id]&image_id=[image_id]
	*
	* @apiSuccess (Ответ) {json} object Объект EventDTO
	*
	* @apiSuccessExample Пример ответа
	*     EventDTO
	*/
	public function actionCoverSet()
	{
		/* @var $object EventDTO */
		$object = $this->getObject(true);

		$imageId = Yii::app()->request->getParam('image_id');
		if (!$imageId) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed image id');
		}

		$image = ImageDTO::model()->with($with)->findByPk($objectId);

		if (!$image) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'The specified image is not found');
		}

		if ($image->user_id != $this->getUser()->id) {
			throw new ApiException(self::ERR_OBJECT_ACCESS, 'You can not access to this image');
		}

		$object->image = $image;
		$object->image_id = $image->id;
		$object->save();

		return $this->renderJson($object);
	}

	/**
	* @api {get} /event/coverRemove Обложка - удалить
	* @apiName EventCoverRemove
	* @apiGroup Event
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiParam {integer} id ID Event
	* @apiParam {boolean} [full] удалить изображение физически (см image/remove)
	*
	* @apiExample Пример запроса
	* /api/event/coverRemove?id=[event id]
	*
	* @apiSuccess (Ответ) {json} object Объект EventDTO
	*
	* @apiSuccessExample Пример ответа
	*     EventDTO
	*/
	public function actionCoverRemove()
	{
		/* @var $object EventDTO */
		$object = $this->getObject(true);

		if ($object->image) {
			$object->image->delete();
		}

		$object->image = null;
		$object->image_id = null;
		$object->save();

		return $this->renderJson($object);
	}


	/**
	* @api {get} /event/userInvite Пользователи - пригласить
	* @apiName EventUserInvite
	* @apiGroup Event
	* @apiDescription Пригласить пользователя в событие.
	* Пользователю будет отправлено приглашение которое он должен принять (/event/userAccept) или отклонить (/event/userReject)
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiParam {integer} id ID Event
	* @apiParam {string} user_id ID пользователей приглашаемых в событие, через запятую (не более 50 за один запрос).
	* @apiParam {boolean} [test] (debugmodeonly) не отправлять приглашение а сразу добавлять в событие
	*
	* @apiExample Пример запроса
	* /api/event/userInvite?id=[event id]&user_id=[users id comma separated]
	*
	* @apiSuccess (Ответ) {integer} integer Фактическое количество созданных новых приглашений
	* @apiSuccessExample Пример ответа
	*     13
	*/
	public function actionUserInvite()
	{
		/* @var $object EventDTO */
		$object = $this->getObject();

		$eventUser = EventUser::model()->find(' user_id = ? AND event_id = ? ', array($this->getUser()->id, $object->id));
		if (!$eventUser) {
			throw new ApiException(self::ERR_OBJECT_ACCESS, 'Can not access to this object');
		}

		$eventReposted = EventReposted::model()->find(' user_id = ? AND event_id = ? ', array($this->getUser()->id, $object->id));
		if ($eventReposted) {
			throw new ApiException(self::ERR_METHOD_DONE, 'You have already invited members to this event');
		}

		$usersId = Yii::app()->request->getParam('user_id');
		if (!$usersId) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed user id');
		}

		$usersId = explode(',', $usersId);
		$usersId = array_map('trim', $usersId);

		foreach ($usersId as $key => $value) {
			if (empty($value) || !is_numeric($value)) {
				unset($usersId[$key]);
			}
		}

		if (!count($usersId) || count($usersId) > 50) {
			throw new ApiException(self::ERR_ARGUMENT_INVALID, 'Invalid user id list');
		}

		$users = User::model()->findAllByPk($usersId);
		
		if (count($usersId) != count($users)) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'Some of specified users is not found');
		}

		$counter = 0;
		$pushSend = array();
		foreach ($users as $user) {
			if (EventUser::model()->find('event_id = ? AND user_id = ?', array($object->id, $user->id))) {
				//Уже в событии
				continue;
			}

			if (YII_DEBUG && Yii::app()->request->getParam('test')) {
				//TODO remove it
				$userEvent = new EventUser();
				$userEvent->user_id = $user->id;
				$userEvent->event_id = $object->id;
				$userEvent->save();
			} else {
				$pushSend[] = $user;
				
				if (EventInvite::model()->find('event_id = ? AND user_id = ?', array($object->id, $user->id))) {
					//Уже приглашен
					continue;
				}

				$invite = new EventInvite();
				$invite->event_id = $object->id;
				$invite->user_id = $user->id;
				$invite->save();
			}

			$counter++;
		}

		$eventReposted = new EventReposted();
		$eventReposted->event_id = $object->id;
		$eventReposted->user_id = $this->getUser()->id;
		$eventReposted->save();

		if (count($pushSend)) {
			$push = PushSender::init();
			$push->addDevice($pushSend);

			if ($object->type == EventDTO::TYPE_BLITZ) {
				$push->send(PushSender::PUSH_EVENT_BLITS_ADD, array(
					'event_id' => $object->id,
					'event_name' => $object->name,
				));
			} else {
				$push->send(PushSender::PUSH_EVENT_ADD, array(
					'event_id' => $object->id,
					'event_name' => $object->name,
				));
			}
		}

		return $this->renderJson($counter);
	}

	/**
	* @api {get} /event/userIsRepost Пользователи - делал ли репост
	* @apiName EventUserIsRepost
	* @apiGroup Event
	* @apiDescription Проверить делал ли текущий юзер репост какого либо события с помощью
	* метода event/userInvite
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiParam {integer} id ID Event
	*
	* @apiExample Пример запроса
	* /api/event/userIsRepost?id=[event id]
	*
	* @apiSuccess (Ответ) {boolean} boolean True - юзер делал репост, false - юзер не делал репост
	* @apiSuccessExample Пример ответа
	*     true
	*/
	public function actionUserIsRepost()
	{
		/* @var $object EventDTO */
		$object = $this->getObject(false);

		$eventReposted = EventReposted::model()->find(' user_id = ? AND event_id = ? ', array($this->getUser()->id, $object->id));
		
		if ($eventReposted) {
			return $this->renderJson(true);
		} else {
			return $this->renderJson(false);
		}
	}


	/**
	* @api {get} /event/userAccept Пользователи - принять приглашение
	* @apiName EventUserAccept
	* @apiGroup Event
	* @apiDescription Принять присланное приглашение на вступление в событие
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiParam {integer} id ID Event
	*
	* @apiExample Пример запроса
	* /api/event/userAccept?id=[event id]
	*
	* @apiSuccessExample Пример ответа
	*     /без данных/
	*/
	public function actionUserAccept()
	{
		/* @var $object EventDTO */
		$object = $this->getObject(false);

		$invite = EventInvite::model()->find('event_id = ? AND user_id = ?', array($object->id, $this->getUser()->id));

		if (!$invite) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'You do not invited in this group');
		}

		$userEvent = new EventUser();
		$userEvent->user_id = $this->getUser()->id;
		$userEvent->event_id = $object->id;
		$userEvent->save();

		$invite->delete();
		
		return $this->renderJson();
	}

	/**
	* @api {get} /event/userReject Пользователи - отклонить приглашение
	* @apiName EventUserReject
	* @apiGroup Event
	* @apiDescription Отклонить присланное приглашение на вступление в событие
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiParam {integer} id ID Event
	*
	* @apiExample Пример запроса
	* /api/event/userReject?id=[event id]
	*
	* @apiSuccessExample Пример ответа
	*     /без данных/
	*/
	public function actionUserReject()
	{
		/* @var $object EventDTO */
		$object = $this->getObject(false);

		$invite = EventInvite::model()->find('event_id = ? AND user_id = ?', array($object->id, $this->getUser()->id));

		if (!$invite) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'You do not invited in this group');
		}

		$invite->delete();

		return $this->renderJson();
	}



	/**
	* @api {get} /event/userList Пользователи - список
	* @apiName EventUserList
	* @apiGroup Event
	* @apiDescription Список пользователей участвующих в группе
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiParam {integer} id ID Event
	* @apiParam {boolean} [invite] включить в список приглашенных (но еще не принявших заявку), 0 - по умолчанию
	*
	* @apiExample Пример запроса
	* /api/event/userList?id=[event id]
	*
    * @apiSuccess (Ответ) {array} array Массив PersonDTO
	* @apiSuccessExample Пример ответа
	*     [PersonDTO,PersonDTO,...]
	*/
	public function actionUserList()
	{
		/* @var $object EventDTO */
		$object = $this->getObject(false);

		$users = $object->users;

		if (Yii::app()->request->getParam('invite') == '1') {
			$invites = EventInvite::model()->findAll('event_id = ?', array($object->id));

			foreach ($invites as $invite) {
				$users[] = $invite->user;
			}
		}

		return $this->renderJson($users);
	}


	/**
	* @api {get} /event/userInviteList Пользователи - список приглашений
	* @apiName EventUserList
	* @apiGroup Event
	* @apiDescription Список необработанных приглашений текущего пользователя.
	* Для каждого приглашения нужно отправить либо event/userAccept либо event/userReject.
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiParam {integer} id ID Event
	*
	* @apiExample Пример запроса
	* /api/event/userInviteList
	*
	* @apiSuccess (Ответ) {array} array Массив EventDTO
	* @apiSuccessExample Пример ответа
	*     [EventDTO,EventDTO,...]
	*/
	public function actionUserInviteList()
	{
		$result = [];

		$invites = EventInvite::model()->with('event')->findAll('t.user_id = ?', array($this->getUser()->id));
		
		foreach ($invites as $invite) {
			$result[] = $invite->event;
		}

		return $this->renderJson($result);
	}


	/**
	* @api {get} /event/userRemove Пользователи - удалить
	* @apiName EventUserRemove
	* @apiGroup Event
	* @apiDescription Только создатель. Удалить пользователя (в т.ч. еще не принявшего приглашение) из события.
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiParam {integer} id ID Event
	* @apiParam {integer} user_id ID User
	*
	* @apiExample Пример запроса
	* /api/event/userRemove?id=[event id]&user_id=[user id]
	*
	* @apiSuccessExample Пример ответа
	*     /без данных/
	*/
	public function actionUserRemove()
	{
		/* @var $object EventDTO */
		$object = $this->getObject(true);

		$userId = Yii::app()->request->getParam('user_id');
		if (!$userId) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed user id');
		}

		$user = User::model()->findByPk($userId);
		if (!$user) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'The specified object is not found');
		}

		$counter = 0;
		if ( ($row = EventUser::model()->find('event_id = ? AND user_id = ?', array($object->id, $user->id))) ) {
			$row->delete();
			$counter++;
		}

		if ( ($row = EventInvite::model()->find('event_id = ? AND user_id = ?', array($object->id, $user->id))) ) {
			$row->delete();
			$counter++;
		}

		if (!$counter) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'The specified user is not in this event');
		}

		$push =
			PushSender::init()
				->addDevice($user->getTokenPush())
				->send(PushSender::PUSH_EVENT_REMOVE, array(
					'event_id' => $object->id,
					'event_name' => $object->name,
				));

		return $this->renderJson();
	}

	/**
	* @api {get} /event/userEnter Пользователи - войти
	* @apiName EventUserEnter
	* @apiGroup Event
	* @apiDescription Войти в событие
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiParam {integer} id ID Event
	*
	* @apiExample Пример запроса
	* /api/event/userEnter?id=[event id]
	*
	* @apiSuccessExample Пример ответа
	*     /без данных/
	*/
	public function actionUserEnter()
	{
		/* @var $object EventDTO */
		$object = $this->getObject(false);

		if ($object->private) {
			throw new ApiException(self::ERR_OBJECT_ACCESS, 'This event is private');
		}

		if (EventUser::model()->find('event_id = ? AND user_id = ?', array($object->id, $this->getUser()->id))) {
			throw new ApiException(self::ERR_OBJECT_EXIST, 'You already in event');
		}

		$eventUser = new EventUser();
		$eventUser->event_id = $object->id;
		$eventUser->user_id = $this->getUser()->id;
		$eventUser->save();

		return $this->renderJson();
	}

	/**
	* @api {get} /event/userExit Пользователи - выйти
	* @apiName EventUserExit
	* @apiGroup Event
	* @apiDescription Выйти из события
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiParam {integer} id ID Event
	*
	* @apiExample Пример запроса
	* /api/event/userExit?id=[event id]
	*
	* @apiSuccessExample Пример ответа
	*     /без данных/
	*/
	public function actionUserExit()
	{
		/* @var $object EventDTO */
		$object = $this->getObject();

		$counter = 0;
		if ( ($row = EventUser::model()->find('event_id = ? AND user_id = ?', array($object->id, $this->getUser()->id))) ) {
			$row->delete();
			$counter++;
		}

		if ( ($row = EventInvite::model()->find('event_id = ? AND user_id = ?', array($object->id, $this->getUser()->id))) ) {
			$row->delete();
			$counter++;
		}

		if (!$counter) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'The specified user is not in this event');
		}

		return $this->renderJson();
	}
}

