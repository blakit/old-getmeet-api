<?php

class TesterController extends Controller
{
	const BASE_PREFIX = 'api/';
	const DEBUG = true;
	const TOKEN = '12345678901234567890123456789012';

	protected function api($method, $token = null, $get = null, $post = null) {
		$url = Yii::app()->getBaseUrl(true) . '/';
		
		$url = $url . self::BASE_PREFIX . $method;

		if ($get) {
			if (is_array($get)) {
				$get = http_build_query($get);
			}
			$url = $url . '?' . $get;
		}


		$headers = array();
		
		if ($token) {
			$headers['Authorization'] = $token;
		}

		$debug = self::DEBUG;

		/* @var $curl Curl */
		$curl = Yii::app()->curl;

		$debugFile = null;
		if ($debug) {
			$debugFile = fopen(Yii::app()->getRuntimePath() . '/curl_debug.txt', 'w+');
				
			$curl->setOptions([
				CURLOPT_VERBOSE        => 1,
				CURLOPT_STDERR         => $debugFile,
			]);
		}

		if (!$post) {
			$result = $curl
				->setHeaders($headers)
				->get($url, array(), true);
		} else {
			$result = $curl
				->setHeaders($headers)
				->post($url, $post, array(), true);
		}

		if ($debugFile) {
			fclose($debugFile);
		}

		return $result;
	}

	function actionIndex() {
		$this->render('index');
	}

	function actionUserGet() {
		if (!$id = Yii::app()->request->getParam('id')) {
			$id = 9;
		}

		$user = UserDTO::model()->findByPk($id);
		$token = $user->token;

		$result = $this->api('user/get', $token);

		$this->render('response', array(
			'response' => $result,
		));
	}

	function actionUserUpdate() {
		$user = UserDTO::model()->findByPk('9');
		$token = $user->token;

		$user->email = 'test7@ssss.ss';
		$user->profile->name = 'Тест';
		$user->profile->secondname = 'Тестович';

		$result = $this->api('user/update', $token, null, json_encode($user));

		$this->render('response', array(
			'response' => $result,
		));
	}

	function actionUserRegister() {
		$user = new UserDTO();
		$user->email = 'tes282' . rand(0, 9999999). '@ssss.ss';

		
		$user = json_decode(json_encode($user), true);
		$user['password'] = '123';

		$result = $this->api('user/register', null, null, json_encode($user));

		
		$this->render('response', array(
			'response' => $result,
		));
	}

	function actionAlbumUpload() {
		$filePath = Yii::app()->basePath . '/data/test.jpg';
		
		if (function_exists('curl_file_create')) { // php 5.6+
			$cFile = curl_file_create($filePath);
		} else { //
			$cFile = '@' . realpath($filePath);
		}

		$result = $this->api('album/imageUpload', self::TOKEN, ['id' => 3], ['file' => $cFile]);

		$this->render('response', array(
			'response' => $result,
		));
	}

	function actionUserLogin() {

		$result = $this->api('user/login?email=tmp1@site.com&password=tmp1');

		$this->render('response', array(
			'response' => $result,
		));
	}

	function actionEventCreate() {
		$event = EventDTO::model()->findByPk('1');

		$data = '{"ageEnd":0,"ageStart":0,"amount":5,"category_id":1,"created_date":0,"description":"hhhhh\n","duration":0,"genderPercent":0,"getChatTypeImage":0,"id":0,"images":[],"private":false,"lat":42.09967319999999,"lon":19.103996199999997,"name":"tmp","price":0,"price_currency":0,"randomUsers":[{"birthdate":"1985-01-01","id":39,"sex":true},{"birthdate":"1989-07-23","id":1,"name":"Dmitriy","secondname":"Amirov","sex":true,"thirdname":""}],"rating":0,"start_date":1476306075,"type":1}';
		$result = $this->api('event/create', self::TOKEN, null, $data);

		$this->render('response', array(
			'response' => $result,
		));
	}

}

