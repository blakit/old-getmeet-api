<?php

class UserController extends ApiController
 {
	const ERR_USER_LOGIN_FAIL					 = 1101;
	
	const ERR_USER_REGISTER_EMAIL_INUSE			 = 1201;
	const ERR_USER_REGISTER_EMAIL_INVALID		 = 1202;
	const ERR_USER_REGISTER_PASSWORD_INVALID	 = 1203;

	private static $registredSocial = array('vk', 'fb', 'tw', 'mm');

	public function filters()
	{
		$filters = parent::filters();

		$filters = array_merge([
			'Access -Login Register SocialLogin SocialRegister Get Error MetaRelations MetaAppearances Meta',
		], $filters);

		return $filters;
	}

	private function checkUserObject(User $user, $isSocial = null)
	{
		if (is_null($isSocial)) {
			$isSocial = false;
		}

		if (
			$user->vk_id
			|| $user->fb_id
			|| $user->tw_id
			|| $user->mm_id
		) {
			$isSocial = true;
		}

		if (!$isSocial) {
			if (!$user->email) {
				throw new ApiException(self::ERR_USER_REGISTER_EMAIL_INVALID, 'Missed email');
			}

			$validator = new CEmailValidator;
			if(!$validator->validateValue($user->email)) {
				throw new ApiException(self::ERR_USER_REGISTER_EMAIL_INVALID, 'Email invalid');
			}
		}
		
		return true;
	}

	protected function afterRegisterUser($user)
	{
		// Создать чат с админом
		$adminId = 2;
		$admin = UserDTO::model()->findByPk($adminId);
		
		if ($admin) {
			$object = Chat::createPrivate($admin, $user);

			if ($object) {
				PubnubHelper::publish($object->id, Yii::app()->params['pubnub-message'], $adminId);
			}
		}
	}

	/**
	 * @api {get} /user/metaRelations Мета - отношения
	 * @apiName UserMetaRelations
	 * @apiGroup User
	 *
	 *
	 * @apiSuccess (Ответ) {array} array Массив ProfileRelationDTO
	 *
	 * @apiExample Пример запроса
	 *     /api/user/metaRelations
	 *
	 * @apiSuccessExample Пример ответа
	 *     [ProfileRelationDTO,ProfileRelationDTO,...]
	 */
	public function actionMetaRelations()
	{
		$objects = ProfileRelationDTO::model()->findAll();

		return $this->renderJson(
			$objects
		);
	}


		/**
	 * @api {get} /user/metaLanguages Мета - языки
	 * @apiName UserMetaLanguages
	 * @apiGroup User
	 *
	 *
	 * @apiSuccess (Ответ) {array} array Массив ProfileLanguageDTO
	 *
	 * @apiExample Пример запроса
	 *     /api/user/metaLanguages
	 *
	 * @apiSuccessExample Пример ответа
	 *     [ProfileLanguageDTO,ProfileLanguageDTO,...]
	 */
	public function actionMetaLanguages()
	{
		$objects = ProfileLanguageDTO::model()->findAll();

		return $this->renderJson(
			$objects
		);
	}


	/**
	 * @api {get} /user/metaAppearances Мета - внешность
	 * @apiName UserMetaAppearances
	 * @apiGroup User
	 *
	 * @apiSuccess (Ответ) {array} array Массив ProfileAppearanceDTO
	 *
	 * @apiExample Пример запроса
	 *     /api/user/metaAppearances
	 *
	 * @apiSuccessExample Пример ответа
	 *     [ProfileAppearanceDTO,ProfileAppearanceDTO,...]
	 */
	public function actionMetaAppearances()
	{
		$objects = ProfileAppearanceDTO::model()->findAll();

		return $this->renderJson(
			$objects
		);
	}

	/**
	 * @api {get} /user/login Пользователь - аутентификация
	 * @apiName UserLogin
	 * @apiGroup User
	 *
	 * @apiParam email Email пользователя
	 * @apiParam password Пароль пользователя
	 *
	 * @apiSuccess (Ответ) {json} object UserDTO
	 *
	 * @apiError (Ошибка) 1101 ERR_USER_LOGIN_FAIL
	 *
	 * @apiExample Пример запроса
	 *     /api/user/login?email=some@email.com&password=somepassword
	 * @apiSuccessExample Пример ответа
	 *     UserDTO
	 */
	public function actionLogin()
	{
		$email = Yii::app()->request->getParam('email');
		$password = Yii::app()->request->getParam('password');

		if (!$email || !$password) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed email or password');
		}

		/* @var $user UserDTO */
		$user = UserDTO::model()->find('LOWER(email)=? AND is_deleted=FALSE', [mb_strtolower($email, 'UTF-8')]);

		if ($user && PasswordHelper::verifyPassword($password, $user->password)) {
			$user->logged = true;
			$user->last_date = DateHelper::TimeSql();

			if (!$user->token) {
				$user->token = UserDTO::tokenGenerate();
			}

			$user->save();

			$this->setUser($user);

			return $this->renderJson(
				$user->setOutputDefaultPersonDTO(false)
			);
		}

		throw new ApiException(self::ERR_USER_LOGIN_FAIL);
	}

	/**
	 * @api {get} /user/token Пользователь - PUSH-токен
	 * @apiName UserToken
	 * @apiGroup User
	 * @apiDescription Обновить Push токен пользователя
	 *
	 * @apiParam os ОС клиента (andriod, ios)
	 * @apiParam push PUSH-токен
	 *
	 * @apiExample Пример запроса
	 *     /api/user/token?os=android&push=12345678901234567890123456789012
	 *
	 * @apiSuccessExample Пример ответа
	 *     /без данных/
	 */
	public function actionToken()
	{
		/* @var $object UserDTO */
		$object = $this->getUser();

		$userToken = $object->getTokenPush();

		if (!$userToken) {
			$userToken = new UserToken;
			$userToken->user_id = $object->id;
		}

		$os = Yii::app()->request->getParam('os');
		$token = Yii::app()->request->getParam('push');

		if (!$os || !$token) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed os or token');
		}

		if (!in_array($os, array(UserToken::TYPE_ANDROID, UserToken::TYPE_IOS))) {
			throw new ApiException(self::ERR_ARGUMENT_INVALID, 'Invalid os id');
		}

		$userToken->type = $os;
		$userToken->token = $token;
		$userToken->time = DateHelper::TimeSql();
		$userToken->save();

		return $this->renderJson();
	}

	/**
	 * @api {post} /user/register Пользователь - регистрация
	 * @apiName UserRegister
	 * @apiGroup User
	 *
	 * @apiParam UserDTO Объект UserDTO, с обязательно заполненными полями email и password
	 *
	 * @apiSuccess (Ответ) {json} object обновленный UserDTO с токеном
	 *
	 * @apiError (Ошибка) 1201 ERR_USER_REGISTER_EMAIL_INUSE
	 * @apiError (Ошибка) 1202 ERR_USER_REGISTER_EMAIL_INVALID
	 * @apiError (Ошибка) 1203 ERR_USER_REGISTER_PASSWORD_INVALID
	 *
	 * @apiExample Пример запроса
	 *     /api/user/register
	 *     POST: UserDTO
	 * 
	 * @apiSuccessExample Пример ответа
	 *     UserDTO
	 */
	public function actionRegister()
	{
		$data = Yii::app()->request->getRawBody();

		if (!$data) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed data body');
		}

		$user = new UserDTO();

		if (!$user->fromJson($data, true)) {
			throw new ApiException(self::ERR_ARGUMENT_INVALID, 'Can not parse object');
		}

		if (!$user->password || PasswordHelper::verifyPassword('', $user->password)) {
			throw new ApiException(self::ERR_USER_REGISTER_PASSWORD_INVALID, 'Missed password');
		}

		$this->checkUserObject($user);

		if (User::model()->find('LOWER(email)=?', [mb_strtolower($user->email)])) {
			throw new ApiException(self::ERR_USER_REGISTER_EMAIL_INUSE, 'Email in use');
		}

		if (!$user->profile) {
			$user->profile = new ProfileDTO();
		}
		$user->profile->setIsNewRecord(true);
		$user->profile->email = $user->email;
		$user->profile->save();

		$user->profile_id = $user->profile->getPrimaryKey();
		$user->date_registred = DateHelper::TimeSql();

		if (!$user->token) {
			$user->token = UserDTO::tokenGenerate();
		}

		$user->save();
		
		$this->setUser($user);
		$this->afterRegisterUser($user);

		return $this->renderJson(
			$user->setOutputDefaultPersonDTO(false)
		);
	}

	/**
	 * @api {get} /user/socialLogin Соцсети - Аутентификация
	 * @apiName UserSocialLogin
	 * @apiGroup User
	 * @apiDescription Аутентификация пользователя через соцсеть
	 *
	 * @apiParam (enum) social Соц сеть (vk,fb,tw,mm)
	 * @apiParam (string) id Идентификатор соц сети юзера
	 *
	 * @apiSuccess (Ответ) {json} object UserDTO
	 *
	 * @apiError (Ошибка) 1101 ERR_USER_LOGIN_FAIL
	 *
	 * @apiExample Пример запроса
	 *     /api/user/socialLogin?social=vk&id=somesocialid
	 * @apiSuccessExample Пример ответа
	 *     UserDTO
	 */
	public function actionSocialLogin()
	{
		$social = Yii::app()->request->getParam('social');
		if (!in_array($social, UserDTO::$registredSocial)) {
			throw new ApiException(self::ERR_ARGUMENT_INVALID, 'Unknown social');
		}

		$socialId = Yii::app()->request->getParam('id');
		if (empty($socialId)) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Social id was not specified');
		}

		$column = $social . '_id';

		/* @var $user UserDTO */
		$user = UserDTO::model()->find($column . ' = ? AND is_deleted=FALSE', [$socialId]);

		if ($user) {
			$user->logged = true;
			$user->last_date = DateHelper::TimeSql();

			if (!$user->token) {
				$user->token = UserDTO::tokenGenerate();
			}

			$user->save();

			$this->setUser($user);

			return $this->renderJson(
				$user->setOutputDefaultPersonDTO(false)
			);
		}

		throw new ApiException(self::ERR_USER_LOGIN_FAIL);
	}

	/**
	 * @api {post} /user/socialRegister Соцсети - регистрация
	 * @apiName UserSocialRegister
	 * @apiGroup User
	 * @apiDescription Регистрация пользователя через соцсеть
	 *
	 * @apiParam (enum) social Соц сеть (vk,fb,tw,mm)
	 * @apiParam (json) UserDTO Объект UserDTO, с обязательно заполненным полем [social]_id
	 *
	 * @apiSuccess (Ответ) {json} object обновленный UserDTO с токеном
	 *
	 * @apiExample Пример запроса
	 *     /api/user/socialRegister?social=vk
	 *     POST: UserDTO
	 * 
	 * @apiSuccessExample Пример ответа
	 *     UserDTO
	 */
	public function actionSocialRegister()
	{
		$data = $this->getJsonBody();

		if (!$data) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed data body');
		}

		$social = Yii::app()->request->getParam('social');
		if (!in_array($social, UserDTO::$registredSocial)) {
			throw new ApiException(self::ERR_ARGUMENT_INVALID, 'Unknown social');
		}

		$user = new UserDTO();
		if (!$user->fromJson($data, true)) {
			throw new ApiException(self::ERR_ARGUMENT_INVALID, 'Can not parse object');
		}
		
		$column = $social . '_id';
		if (!$user->$column) {
			throw new ApiException(self::ERR_ARGUMENT_INVALID, 'Value ' . $column . ' was not specified');
		}

		if (User::model()->find($column . ' = ?', [$user->$column])) {
			throw new ApiException(self::ERR_ARGUMENT_INVALID, 'Social id in use');
		}

		if (!$user->profile) {
			$user->profile = new ProfileDTO();
		}
		$user->profile->setIsNewRecord(true);
		$user->profile->save();

		$user->profile_id = $user->profile->getPrimaryKey();
		$user->date_registred = DateHelper::TimeSql();

		if (!$user->token) {
			$user->token = UserDTO::tokenGenerate();
		}

		$user->save();

		$this->setUser($user);
		$this->afterRegisterUser($user);

		return $this->renderJson(
			$user->setOutputDefaultPersonDTO(false)
		);
	}

	/**
	 * @api {get} /user/get PersonDTO - получить
	 * @apiName UserGet
	 * @apiGroup User
	 * @apiDescription Получить публичную информацию о каком-то пользователе
	 *
	 * @apiParam {integer} id ID пользователя
	 * 
	 * @apiExample Пример запроса
	 * /api/user/get?id=1
	 *
	 * @apiSuccess (Ответ) {json} object PersonDTO
	 *
	 * @apiSuccessExample Пример ответа
	 *     PersonDTO
	 */
	public function actionGet()
	{
		$id = Yii::app()->request->getParam('id');

		$objectId = Yii::app()->request->getParam('id');

		if (!$objectId) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed user id');
		}

		$object = UserDTO::model()->findByPk($objectId);

		if (!$object) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'The specified object is not found');
		}

		return $this->renderJson($object->toPersonDTO());
	}

	/**
	 * @api {get} /user/getProfile ProfileDTO - получить
	 * @apiName UserGetProfile
	 * @apiGroup User
	 * @apiDescription Получить публичную информацию о каком-то пользователе
	 *
	 * @apiParam {integer} id ID пользователя
	 *
	 * @apiExample Пример запроса
	 * /api/user/getProfile?id=1
	 *
	 * @apiSuccess (Ответ) {json} object ProfileDTO
	 *
	 * @apiSuccessExample Пример ответа
	 *     ProfileDTO
	 */
	public function actionGetProfile()
	{
		$id = Yii::app()->request->getParam('id');

		$objectId = Yii::app()->request->getParam('id');

		if (!$objectId) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed user id');
		}

		$object = UserDTO::model()->findByPk($objectId);

		if (!$object) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'The specified object is not found');
		}

		return $this->renderJson($object->profile);
	}

	/**
	 * @api {get} /user/getMe UserDTO - получить текущий
	 * @apiName UserGetMe
	 * @apiGroup User
	 * 
	 * @apiDescription Получить объект UserDTO текущего пользователя
	 *
	 * @apiUse ResponseHeaderAuth
	 *
	 * @apiExample Пример запроса
	 * /api/user/getMe
	 *
	 * @apiSuccess (Ответ) {json} object UserDTO
	 *
	 * @apiSuccessExample Пример ответа
	 *     UserDTO
	 */
	public function actionGetMe()
	{
		return $this->renderJson(
			$this->getUser()->setOutputDefaultPersonDTO(false)
		);
	}

	/**
	 * @api {post} /user/updateMe UserDTO - изменить текущий
	 * @apiName UserUpdateMe
	 * @apiGroup User
	 *
	 * @apiUse ResponseHeaderAuth
	 *
	 * @apiExample Пример запроса
	 * /api/user/updateMe
	 * POST: {UserDTO}
	 *
	 * @apiParam {json} object UserDTO
	 * 
	 * @apiSuccess (Ответ) {json} object Измененый объект UserDTO
	 *
	 * @apiSuccessExample Пример ответа
	 *     UserDTO
	 */
	public function actionUpdateMe()
	{
		$data = Yii::app()->request->getRawBody();

		if (!$data) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed data body');
		}
		
		if (!$this->getUser()->fromJson($data)) {
			throw new ApiException(self::ERR_ARGUMENT_INVALID, 'Can not parse object');
		}

		$this->checkUserObject($this->getUser());

		$this->getUser()->fromJson($data);
		$this->getUser()->save();

		if ($this->getUser()->profile) {
			$this->getUser()->profile->save();
		}

		return $this->renderJson(
			$this->getUser()->setOutputDefaultPersonDTO(false)
		);
	}


	/**
	 * @api {get} /user/search Поиск пользователей
	 * @apiName UserSearch
	 * @apiGroup User
	 *
	 * @apiUse ResponseHeaderAuth
	 * @apiUse RequestPagination
	 * @apiUse RequestUserSearch
	 *
	 * @apiExample Пример запроса
	 * /api/user/search?search[location][lat]=44.7262&search[location][lon]=37.7632&search[location][radius]=2000
	 *
	 * @apiSuccess (Ответ) {array} array Массив PersonDTO
	 *
	 * @apiSuccessExample Пример ответа
	 *     [PersonDTO,PersonDTO,...]
	 */
	public function actionSearch()
	{
		$criteria = $this->searchMakeCriteria($this->getUser()->id);

		$objectList = UserDTO::model()->findAll($criteria);

		return $this->renderJson($objectList);
	}


	/**
	* @apiDefine RequestUserSearch
	*
	* @apiParam (Поиск) {Boolean} [search[friends]] 1 - поиск среди друзей, 0 - по всем (default)
	* @apiParam (Поиск) {string} [search[name]] поиск по имени (не злоупотреблять, желательно использовать только для поиска среди друзей)
	* @apiParam (Поиск) {Boolean} [search[sex]] пол: 1 - муж, 0 - жен
	* @apiParam (Поиск) {Integer} [search[age_start]] Возраст от (включительно)
	* @apiParam (Поиск) {Integer} [search[age_end]] Возраст до (включительно)
	* @apiParam (Поиск) {Json} [search[location]] LocationDTO фильтр по местоположению (default для radius - 200м)
	*/
	private function searchMakeCriteria($userId = null)
	{
		$search = Yii::app()->request->getParam('search');

		if (empty($search)) {
			$search = array();
		}

		$criteria = new CDbCriteria;
		$criteria->with		 = array( 'profile' );
		$criteria->together	 = true;
		//$criteria->order	 = 'start_date DESC';

		$condition	 = array();
		$params		 = array();

		foreach ($search as $searchKey => $searchValue) {
			if (empty($searchValue)) {
				continue;
			}

			switch ($searchKey) {
				case 'friends':
					if ($searchValue == '1') {
						if (!is_null($userId)) {
							$criteria->join .= 'LEFT JOIN friend AS user_friends ON (user_friends.user_id = :user_id AND user_friends.friend_id = t.id)';

							$condition[] = 'user_friends.user_id = :user_id';
							$params[':user_id'] = $userId;
						}
					}
					break;

				case 'name':
					$params[':name'] = '%' . $searchValue . '%';
					$condition[] = "btrim(lower((((profile.name::text || ' '::text) || profile.secondname::text) || ' '::text) || profile.thirdname::text)) LIKE :name";
					break;

				case 'sex':
					$params[':sex'] = $searchValue;
					$condition[] = 'profile.sex = :sex';
					break;

				case 'age_start':
					$condition[] = "profile.birthdate <= (NOW() - interval '" . intval($searchValue) . " year')";
					break;

				case 'age_end':
					$condition[] = "profile.birthdate >= (NOW() - interval '" . intval($searchValue) . " year')";
					break;

				case 'location':
					$location = new LocationDTO();

					if (!$location->fromJson($searchValue)) {
						throw new ApiException(self::ERR_OBJECT_INVALID, 'Can not parse location');
					}

					if (!$location->radius || $location->radius < 0) {
						$location->radius = 200;
					}

					$condition[] = GisHelper::sqlRadius($location);
					break;

				default:
					break;
			}
		}

		$condition[] = 't.is_deleted = false';

		$criteria->condition = implode(' AND ', $condition);
		$criteria->params = $params;

		$this->searchLimitOffsetCriteria($criteria);

		return $criteria;
	}

	/**
	* @api {post} /user/avatarUpload Аватар - загрузить
	* @apiName UserAvatarUpload
	* @apiGroup User
	*
	* @apiUse ResponseHeaderAuth
	* @apiUse ErrorUpload
	*
	* @apiParam {boolean} [main=1] установить аватар основным (0 - не устанавливать)
	* @apiParam {boolean} [collection=1] добавить в коллекцию аватарок (0 - не добавлять)
	*
	* @apiExample Пример запроса
	* #картинка будет добавлена в коллекцию аватарок и установлена в качестве основной
	* /api/user/avatarUpload
	* Content-Type: multipart/form-data
	*
	* POST: file=[uploadFile]
	* 
	*
	* @apiExample Пример запроса
	* #картинка будет добавлена только в коллекцию аватарок
	* /api/user/avatarUpload?main=0
	* Content-Type: multipart/form-data
	*
	* POST: file=[uploadFile]
	*
	* @apiSuccess (Ответ) {json} object Объект UserDTO
	*
	* @apiSuccessExample Пример ответа
	*     UserDTO
	*/
	public function actionAvatarUpload()
	{
		/* @var $object UserDTO */
		$object = $this->getUser();

		if ( !($image = ImageDTO::upload('file', $this->getUser())) ) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'File not specified');
		}

		$profile = $object->profile;

		if ( Yii::app()->request->getParam('main') !== '0' ) {
			$profile->image = $image;
			$profile->image_id = $image->id;
		}

		if ( Yii::app()->request->getParam('collection') !== '0' ) {
			$profile->image_list = array_merge($profile->image_list, array($image->id));
		}

		$profile->save();

		return $this->renderJson($object->setOutputDefaultPersonDTO(false));
	}

	/**
	* @api {get} /user/avatarSet Аватар - установить
	* @apiName UserAvatarSet
	* @apiGroup User
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiParam {integer} id ID ImageDTO
	* @apiParam {boolean} [main=1] установить аватар основным (0 - не устанавливать)
	* @apiParam {boolean} [collection=1] добавить в коллекцию аватарок (0 - не добавлять)
	*
	* @apiExample Пример запроса
	* /api/user/avatarSet?image_id=[image_id]
	*
	* @apiSuccess (Ответ) {json} object Объект UserDTO
	*
	* @apiSuccessExample Пример ответа
	*     UserDTO
	*/
	public function actionAvatarSet()
	{
		/* @var $object UserDTO */
		$object = $this->getUser();
		$profile = $object->profile;

		$objectId = Yii::app()->request->getParam('image_id');
		if (!$objectId) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed image id');
		}

		$image = ImageDTO::model()->findByPk($objectId);

		if (!$image) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'The specified image is not found');
		}

		if ($image->user_id != $this->getUser()->id) {
			throw new ApiException(self::ERR_OBJECT_ACCESS, 'You can not access to this image');
		}

		$profile = $object->profile;

		if ( Yii::app()->request->getParam('main') !== '0' ) {
			$profile->image = $image;
			$profile->image_id = $image->id;
		}

		if ( Yii::app()->request->getParam('collection') !== '0' ) {
			$profile->image_list = array_merge($profile->image_list, array($image->id));
		}

		$profile->save();

		return $this->renderJson($object->setOutputDefaultPersonDTO(false));
	}

	/**
	* @api {get} /user/avatarRemove Аватар - удалить
	* @apiName UserAvatarRemove
	* @apiGroup User
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiParam {boolean} [full] удалить изображение физически (см image/remove)
	* @apiParam {integer} [image_id] удалить аватарку из коллекции аватарок (если не указано - удалится основная)
	*
	* @apiExample Пример запроса
	* /api/user/avatarRemove
	*
	* @apiSuccess (Ответ) {json} object Объект UserDTO
	*
	* @apiSuccessExample Пример ответа
	*     UserDTO
	*/
	public function actionAvatarRemove()
	{
		/* @var $object UserDTO */
		$object = $this->getUser();
		$profile = $object->profile;


		if ( Yii::app()->request->getParam('image_id') ) {
			$deletedImageId = intval(Yii::app()->request->getParam('image_id'));
		} else {
			$deletedImageId = $profile->image_id;
		}

		// Удаление основной картинки
		if ($profile->image_id == $deletedImageId) {
			$profile->image = null;
			$profile->image_id = null;
		}

		// Удаление картинки из коллекции
		$imageList = $profile->image_list;

		foreach ($imageList as $key => $value) {
			if ($value instanceof Image) {
				if ($value->id == $deletedImageId) {
					unset($imageList[$key]);
				}
			} else {
				if ($value == $deletedImageId) {
					unset($imageList[$key]);
				}
			}
		}
		
		$imageList = array_values($imageList);
		$profile->image_list = $imageList;

		// Устанавливаем новую картинку из коллекции
		if (!$profile->image_id && count($imageList)) {
			$imageId = $imageList[0];
			$image = ImageDTO::model()->findByPk($imageId);

			if ($image) {
				$profile->image = $image;
				$profile->image_id = $image->id;
			}
		}

		// Полное удаление указанной картинки
		if (Yii::app()->request->getParam('full')) {
			$image = ImageDTO::model()->findByPk($deletedImageId);
			
			if ($image) {
				$image->delete();
			}
		}
		
		$profile->save();

		return $this->renderJson($object->setOutputDefaultPersonDTO(false));
	}


	/**
	* @api {get} /user/delete UserDTO - удалить
	* @apiName UserDelete
	* @apiGroup User
	* @apiDescription Удалить аккаунт из системы. Использовать с осторожностью.
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/user/delete
	*
	* @apiSuccessExample Пример ответа
	*     /без данных/
	*/
	public function actionDelete()
	{
		/* @var $object UserDTO */
		$object = $this->getUser();

		if (!$object->is_deleted) {
			if ($object->email) {
				$object->email = md5($object->email) . '_' . mt_rand(0, 99999999) . '@deleted';
			}
			$object->token = null;
			$object->push_id = null;
			$object->logged = false;
			$object->password = PasswordHelper::hashPassword(microtime());
			$object->vk_id = null;
			$object->fb_id = null;
			$object->tw_id = null;
			$object->mm_id = null;
			$object->is_deleted = true;
			$object->save();

			$object->profile->appearance_id = null;
			$object->profile->relation_id = null;
			$object->profile->birthdate = null;
			$object->profile->sex = null;
			$object->profile->about = null;
			$object->profile->phone = null;
			if ($object->profile->email) {
				$object->profile->email = md5($object->profile->email) . '_' . mt_rand(0, 99999999) . '@deleted';
			}
			$object->profile->rating = 0;
			$object->profile->lat = null;
			$object->profile->lon = null;
			$object->profile->image_id = null;
			$object->profile->address = null;
			$object->profile->language = null;
			$object->profile->category_fav = null;
			$object->profile->like = null;
			$object->profile->image_list = null;
			$object->profile->save();
		}
		
		return $this->renderJson();
	}
	
}

