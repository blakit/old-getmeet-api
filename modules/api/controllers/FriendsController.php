<?php


class FriendsController extends ApiController
{
	/**
	* @api {get} /friends/add Друзья - добавить
	* @apiName FriendsAdd
	* @apiGroup Friends
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/friends/add?user_id=[user_id comma separated]
	*
	* @apiParam {integer} user_id ID User, через запятую (не более 50 за один запрос).
	*
	* @apiSuccess (Ответ) {integer} integer Фактическое количество добавленных в друзья пользователей
	* @apiSuccessExample Пример ответа
	*     17
	*/
	public function actionAdd()
	{
		$usersId = Yii::app()->request->getParam('user_id');
		if (!$usersId) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed user id');
		}

		$usersId = explode(',', $usersId);
		$usersId = array_map('trim', $usersId);

		foreach ($usersId as $key => $value) {
			if (empty($value) || !is_numeric($value)) {
				unset($usersId[$key]);
			}
		}

		if (!count($usersId) || count($usersId) > 50) {
			throw new ApiException(self::ERR_ARGUMENT_INVALID, 'Invalid user id list');
		}

		$users = User::model()->findAllByPk($usersId);

		if (count($usersId) != count($users)) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'Some of specified users is not found');
		}

		$counter = 0;
		$pushSend = array();
		
		foreach ($users as $user) {
			/* @var $user UserDTO */
			if (Friend::model()->find(' user_id = ? AND friend_id = ? ', array($this->getUser()->id, $user->id))) {
				//Уже в друзьях
				continue;
			}

			$friend = new Friend();
			$friend->friend_id = $user->id;
			$friend->user_id = $this->getUser()->id;
			$friend->save();

			$pushSend[] = $user;
			$counter++;
		}

		if (count($pushSend)) {
			PushSender::init()
				->addDevice($pushSend)
				->send(PushSender::PUSH_FRIENDS_ADD, array(
					'user_id' => $this->getUser()->id,
					'user_name' => $this->getUser()->profile->name,
				));
		}

		return $this->renderJson($counter);
	}


	/**
	* @api {get} /friends/remove Друзья - удалить
	* @apiName FriendsRemove
	* @apiGroup Friends
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/friends/remove?user_id=[user_id]
	*
	* @apiParam {integer} user_id ID User
	*
	* @apiSuccessExample Пример ответа
	*     /без данных/
	*/
	public function actionRemove()
	{
		$userId = Yii::app()->request->getParam('user_id');

		if (!$userId) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed user id');
		}

		/* @var $user UserDTO */
		$user = UserDTO::model()->findByPk($userId);
		if (!$user) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'Specified user is not found');
		}

		$friend = Friend::model()->find(' user_id = ? AND friend_id = ? ', array($this->getUser()->id, $user->id));

		if (!$friend) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'This user is not in your friends');
		}

		$friend->delete();

		return $this->renderJson();
	}

	/**
	* @api {get} /friends/list Друзья - список
	* @apiName FriendsList
	* @apiGroup Friends
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/friends/list
	*
	* @apiSuccessExample Пример ответа
	*     [PersonDTO,PersonDTO,...]
	*/
	public function actionList()
	{
		$users = $this->getUser()->friends;

		return $this->renderJson($users);
	}


	/**
	* @api {post} /friends/contacts Контакты - список по телефонам
	* @apiName FriendsContacts
	* @apiGroup Friends
	* @apiDescription Возвращает список пользователей зарегестрированных в системе по списку номеров телефона. Не более 1000 телефонов за раз.
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/friends/contacts
	*
	* POST: [79881112233,79881112233,...]
	*
	* @apiSuccessExample Пример ответа
	*     [PersonDTO,PersonDTO,...]
	*/
	public function actionContacts()
	{
		$phones = $this->getJsonBody();
		if (!$phones || !count($phones)) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed phone list');
		}

		$phones = array_unique($phones);
		if (count($phones) > 1000) {
			throw new ApiException(self::ERR_ARGUMENT_INVALID, 'Too many phone numbers');
		}

		foreach ($phones as $value) {
			if (!preg_match('#^[0-9]{3,18}$#', $value)) {
				throw new ApiException(self::ERR_ARGUMENT_INVALID, 'Invalid phone format');
			}
		}

		$phones = array_map('addslashes', $phones);
		
		$users = UserDTO::model()->with('profile')->findAll("profile.phone IN ('" . implode("', '", $phones) . "')");

		return $this->renderJson($users);
	}


	/**
	* @api {post} /friends/contactsName Контакты - список по имени
	* @apiName FriendsContactsName
	* @apiGroup Friends
	* @apiDescription Возвращает список пользователей зарегестрированных в системе по списку ФИО. Не более 1000 ФИО за раз.
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/friends/contactsName
	*
	* POST: ["Иван Иванов","Сергей Сергеев Сергеевич",...]
	*
	* @apiSuccessExample Пример ответа
	*     [PersonDTO,PersonDTO,...]
	*/
	public function actionContactsName()
	{
		$names = $this->getJsonBody();
		if (!$names || !count($names)) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed name list');
		}

		$names = array_unique($names);
		if (count($names) > 1000) {
			throw new ApiException(self::ERR_ARGUMENT_INVALID, 'Too many names');
		}

		$names = array_map(function($value){
			return trim(mb_strtolower($value, 'UTF-8'));
		}, $names);
		
		$names = array_map('addslashes', $names);

		$users = UserDTO::model()
			->with('profile')
			->findAll("
				btrim(lower((COALESCE(name::text || ' '::text, ''::text) || COALESCE(secondname::text || ' '::text, ''::text)) || COALESCE(thirdname::text, ''::text))) 
				IN ('" . implode("', '", $names) . "')
			");

		return $this->renderJson($users);
	}


	/**
	* @api {post} /friends/contactsSocial Контакты - список по соцсетям
	* @apiName FriendsContactsSocial
	* @apiGroup Friends
	* @apiDescription Возвращает список пользователей зарегестрированных в системе по списку ID указанной соц сети. Не более 1000 ID за раз.
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiParam {string} social Тип соц сети (vk,fb,tw,mm)
	*
	* @apiExample Пример запроса
	* /api/friends/contactsSocial?social=vk
	*
	* POST: ["1111111111111","222222222222222",...]
	*
	* @apiSuccessExample Пример ответа
	*     [PersonDTO,PersonDTO,...]
	*/
	public function actionContactsSocial()
	{
		$social = Yii::app()->request->getParam('social');
		if (!in_array($social, UserDTO::$registredSocial)) {
			throw new ApiException(self::ERR_ARGUMENT_INVALID, 'Unknown social');
		}
		
		$names = $this->getJsonBody();
		if (!$names || !count($names)) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed id list');
		}

		$names = array_unique($names);
		if (count($names) > 1000) {
			throw new ApiException(self::ERR_ARGUMENT_INVALID, 'Too many id');
		}

		$names = array_map('addslashes', $names);
		
		$column = $social . '_id';

		$users = UserDTO::model()
			->with('profile')
			->findAll("
				" . $column . "
				IN ('" . implode("', '", $names) . "')
			");

		return $this->renderJson($users);
	}

}

