<?php


class ChatController extends ApiController
{

	protected $controllerBaseClass = 'ChatDTO';

	/**
	 *
	 * @param type $checkAccess
	 * @param type $with
	 * @return ChatDTO
	 * @throws ApiException
	 */
	protected function getObject($checkAccess = false, $with = array())
	{
		/* @var $object ChatDTO */
		$object = parent::getObject($with);

		if ($checkAccess) {
			if ($object->admin->id != $this->getUser()->id) {
				throw new ApiException(self::ERR_OBJECT_ACCESS, 'Can not access to this object');
			}
		}

		return $object;
	}

	/**
	* @api {post} /chat/create ChatDTO - Создать
	* @apiDescription Deprecated. Использовать CreatePrivate, CreateEvent, etc методы
	* @apiName ChatCreate
	* @apiGroup Chat
	*/

	/**
	 * @api {post} /chat/update ChatDTO - Изменить
	 * @apiName ChatUpdate
	 * @apiGroup Chat
	 *
	 * @apiUse ResponseHeaderAuth
	 *
	 * @apiExample Пример запроса
	 * /api/chat/update?id=[id]
	 * POST: {ChatDTO}
	 *
	 * @apiParam {integer} id ID Chat
	 * @apiParam {json} object ChatDTO
	 *
	 * @apiSuccess (Ответ) {json} object Изменненый объект ChatDTO
	 *
	 * @apiSuccessExample Пример ответа
	 *     ChatDTO
	 */
	public function actionUpdate()
	{
		$data = Yii::app()->request->getRawBody();

		if (!$data) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed data body');
		}

		$object = $this->getObject(true);

		if (!$object->fromJson($data)) {
			throw new ApiException(self::ERR_OBJECT_INVALID, 'Can not parse object');
		}

		$object->admin = $this->getUser();
		$object->admin_id = $this->getUser()->id;

		$object->save();

		return $this->renderJson($object);
	}

	
	/**
	* @api {get} /chat/createEvent ChatDTO - создать Event
	* @apiDescription Создать чат типа Event. Если такой чат существует - вернется существующий.
	* @apiName ChatCreateEvent
	* @apiGroup Chat
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/chat/createEvent?event_id=[id]
	*
	* @apiParam {integer} event_id ID Event
	*
	* @apiSuccess (Ответ) {json} object Созданный объект ChatDTO
	*
	* @apiSuccessExample Пример ответа
	*     ChatDTO
	*/
	public function actionCreateEvent()
	{
		$eventId = Yii::app()->request->getParam('event_id');

		/* @var $event EventDTO*/
		$event = EventDTO::model()->findByPk($eventId);
		if (!$event) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'Specified object is not found');
		}

		$eventUser = EventUser::model()->find(' event_id = ? AND user_id = ? ', array($event->id, $this->getUser()->id));
		
		if (!$eventUser) {
			throw new ApiException(self::ERR_OBJECT_ACCESS, 'You can not acces to this event');
		}

		$object = ChatDTO::model()->find(' event_id = ? ', array($eventId));

		if (!$object) {
			$object = new ChatDTO();
			$object->admin_id = $event->admin_user_id;
			$object->admin = $event->adminUser;
			$object->event_id = $event->id;
			$object->type = ChatDTO::TYPE_EVENT;
			$object->save();

			$this->sendInvitedPush($object, null, $this->getUser(), $this->getUser());
		} else {
			if ( ($item = ChatExit::model()->find(' chat_id = ? AND user_id = ? ', array($object->id, $this->getUser()->id))) ) {
				$item->delete();
			}
		}
		
		return $this->renderJson($object);
	}

	/**
	* @api {post} /chat/createGroup ChatDTO - создать Group
	* @apiDescription Создать груповой чат между несколькими людьми. Если такой чат существует - вернется существующий.
	* @apiName ChatCreateGroup
	* @apiGroup Chat
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/chat/createGroup
	* POST: [10,20,...]
	*
	* @apiParam {json} array ID users list to join chat
	*
	* @apiSuccess (Ответ) {json} object Созданный объект ChatDTO
	*
	* @apiSuccessExample Пример ответа
	*     ChatDTO
	*/
	public function actionCreateGroup()
	{
		$usersId = $this->getJsonBody();
		if (!$usersId || !is_array($usersId) || !count($usersId)) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed data body');
		}
		
		$usersId[] = $this->getUser()->id;
		$usersId[] = array_unique($usersId);

		$object = ChatDTO::model()->findByUsers($usersId, ChatDTO::TYPE_GROUP);
		
		if (!$object) {
			if (count($usersId) < 2) {
				throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed data body');
			}

			$users = User::model()->findAllByPk($usersId);
			if (count($users) != count($usersId)) {
				throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'One or more users not found');
			}

			$object = new ChatDTO();
			$object->admin_id = $this->getUser()->id;
			$object->admin = $this->getUser();
			$object->type = ChatDTO::TYPE_GROUP;
			$object->save();

			foreach ($users as $user) {
				$chatUser = new ChatUser();
				$chatUser->chat_id = $object->id;
				$chatUser->user_id = $user->id;
				$chatUser->save();
			}
			
			$this->sendInvitedPush($object, null, $this->getUser(), $this->getUser());
		}

		return $this->renderJson($object);
	}

	private function sendInvitedPush(Chat $chat, $users = null, $fromUser = null, $exclude = null)
	{
		$push = PushSender::init();

		if (is_null($users)) {
			if ($chat->type == Chat::TYPE_EVENT && $chat->event) {
				$users = $chat->event->users;
			} else {
				$users = $chat->users;
			}
		}

		$push->addDevice($users);
		if (!$exclude) {
			$push->addExcludeDevice($chat->admin);
		} else {
			$push->addExcludeDevice($exclude);
		}

		$fromName = null;
		
		if ($fromUser instanceof User) {
			$fromName = $fromUser->profile->name;
		} elseif ($chat->admin && $chat->admin->profile) {
			$fromName = $chat->admin->profile->name;
		}

		$push->send(PushSender::PUSH_CHAT_ADD, array(
			'chat_id' => $chat->id,
			'chat_type' => $chat->type,
			'from_name' => $fromName,
		));
	}



	/**
	* @api {get} /chat/createPrivate ChatDTO - создать Private
	* @apiDescription Создать привантый чат между двумя людьми. Если такой чат с таким набором пользователей существует - вернется существующий.
	* @apiName ChatCreatePrivate
	* @apiGroup Chat
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/chat/createPrivate?user_id=[id]
	*
	* @apiParam {integer} user_id ID User
	*
	* @apiSuccess (Ответ) {json} object Созданный объект ChatDTO
	*
	* @apiSuccessExample Пример ответа
	*     ChatDTO
	*/
	public function actionCreatePrivate()
	{
		$userId = Yii::app()->request->getParam('user_id');
		if (!$userId) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed user id');
		}

		$user = UserDTO::model()->findByPk($userId);
		if (!$user) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'Specified object is not found');
		}

		if ($user->id == $this->getUser()->id) {
			throw new ApiException(self::ERR_OBJECT_INVALID, 'Can not start chat with yourself');
		}

		$object = Chat::createPrivate($this->getUser(), $user, $created);
		
		if ($created) {
			$this->sendInvitedPush($object, null, $this->getUser(), $this->getUser());
		}

		return $this->renderJson($object);
	}


	/**
	* @api {get} /chat/get ChatDTO - получить
	* @apiName ChatGet
	* @apiGroup Chat
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/chat/get?id=[id]
	*
	* @apiParam {integer} id ID Chat
	*
	* @apiSuccess (Ответ) {json} object Объект ChatDTO
	*
	* @apiSuccessExample Пример ответа
	*     ChatDTO
	*/
	public function actionGet()
	{
		$object = $this->getObject();

		return $this->renderJson($object);
	}


	/**
	* @api {get} /chat/exit ChatDTO - выйти
	* @apiName ChatExit
	* @apiGroup Chat
	* @apiDescription Для приватного/событийного чата - чат будет скрыт из общего списка чатов.
	* Для групового чата - пользователь выйдет из чата.
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/chat/exit?id=[id]
	*
	* @apiParam {integer} id ID Chat
	*
	* @apiSuccessExample Пример ответа
	*     /без данных/
	*/
	public function actionExit()
	{
		$object = $this->getObject();

		if ( ($object->type = ChatDTO::TYPE_GROUP) ) {
			$item = ChatUser::model()->find(' chat_id = ? AND user_id = ? ', array($object->id, $this->getUser()->id));
			
			if ($item) {
				$item->delete();
			}
		} else {
			$item = ChatExit::model()->find(' chat_id = ? AND user_id = ? ', array($object->id, $this->getUser()->id));
			
			if (!$item) {
				$item = new ChatExit();
				$item->chat_id = $object->id;
				$item->user_id = $this->getUser()->id;
				$item->save();
			}
		}

		return $this->renderJson();
	}


	/**
	* @api {get} /chat/delete ChatDTO - удалить
	* @apiName ChatDelete
	* @apiGroup Chat
	* @apiDescription Для приватного/групового чата - чат будет удален из общего списка чатов.
	* Для приватного чата - вернется ошибка ERR_METHOD_NOT_IMPLEMENTED
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/chat/delete?id=[id]
	*
	* @apiParam {integer} id ID Chat
	*
	* @apiSuccessExample Пример ответа
	*     /без данных/
	*/
	public function actionDelete()
	{
		$object = $this->getObject(true);

		if ($object->type == ChatDTO::TYPE_PRIVATE) {
			throw new ApiException(self::ERR_METHOD_NOT_IMPLEMENTED, 'Private chat can not delete');
		}

		$object->is_deleted = true;
		$object->save();

		return $this->renderJson();
	}


	/**
	* @api {get} /chat/list ChatDTO - список
	* @apiDescription Получить чаты в которых учавствует данный аккаунт
	* @apiName ChatList
	* @apiGroup Chat
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/chat/list
	*
	* @apiSuccess (Ответ) {json} object Объект ChatDTO
	*
	* @apiSuccessExample Пример ответа
	*     [ChatDTO,ChatDTO,...]
	*/
	public function actionList()
	{
		$criteria = new CDbCriteria;
		$criteria->with		 = array(
			'admin'						 => array('alias' => 'chat_admin'),
			'admin.profile'				 => array('alias' => 'chat_admin_profile'),
			'admin.profile.image'		 => array('alias' => 'chat_admin_profile_image'),
			'users',
			'users.profile'				 => array('alias' => 'chat_users_profile'),
			'users.profile.image'		 => array('alias' => 'chat_users_profile_image'),
		);
		$criteria->together	 = true;
		$criteria->join .= " LEFT JOIN chat_exit AS check_chat_exit ON (check_chat_exit.chat_id = t.id  AND check_chat_exit.user_id = :user_id ) ";
		$criteria->condition = " (ARRAY[ :user_id ]::integer[] <@ cache_user) AND (check_chat_exit.user_id IS NULL) AND (t.is_deleted=false) ";
		$criteria->params = array('user_id' => $this->getUser()->id);

		$chatList = ChatDTO::model()->findAll($criteria);
		array_map(function($item) {
			$item->setOutputFull(true);
		}, $chatList);

		
		$criteria = new CDbCriteria;
		$criteria->with		 = array(
			'admin'						 => array('alias' => 'chat_admin'),
			'admin.profile'				 => array('alias' => 'chat_admin_profile'),
			'admin.profile.image'		 => array('alias' => 'chat_admin_profile_image'),
			'event',
			'event.image'				 => array('alias' => 'event_image'),
			'event.category'			 => array('alias' => 'event_category'),
			'event.adminUser'			 => array('alias' => 'event_admin'),
			'event.adminUser.profile'	 => array('alias' => 'event_admin_profile'),
			'event.adminUser.profile.image' => array('alias' => 'event_admin_profile_image'),
			'event.users_count',
		);
		$criteria->together	 = true;
		$criteria->join .= " LEFT JOIN chat_exit AS check_chat_exit ON (check_chat_exit.chat_id = t.id  AND check_chat_exit.user_id = :user_id ) ";
		$criteria->condition = " t.event_id IN (SELECT event_user.event_id FROM event_user WHERE user_id = :user_id) AND (t.is_deleted=false) ";
		$criteria->params = array('user_id' => $this->getUser()->id);

		$chatEvents = ChatDTO::model()->findAll($criteria);
		array_map(function($item) {
			$item->setOutputFull(false);
		}, $chatEvents);

		$chats = array_merge($chatList, $chatEvents);

		$chatIds = array();
		$result = array();

		foreach ($chats as $chat) {
			if (in_array($chat->id, $chatIds)) {
				continue;
			}

			$chatIds[] = $chat->id;
			$result[] = $chat;
		}

		return $this->renderJson($result);
	}




	/**
	* @api {post} /chat/imageUpload Изображения - загрузить
	* @apiName ChatImageUpload
	* @apiGroup Chat
	*
	* @apiUse ResponseHeaderAuth
	* @apiUse ErrorUpload
	*
	* @apiParam {integer} id ID ChatDTO
	*
	* @apiExample Пример запроса
	* /api/chat/imageUpload?id=[chat id]
	* Content-Type: multipart/form-data
	*
	* POST: file=[uploadFile]
	*
	* @apiSuccess (Ответ) {json} object Объект ImageDTO
	*
	* @apiSuccessExample Пример ответа
	*     ImageDTO
	*/
	public function actionImageUpload()
	{
		/* @var $object ChatDTO */
		$object = $this->getObject();

		// TODO: проверка доступа к чату

		if ( !($file = ImageDTO::upload('file', $this->getUser())) ) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'File not specified');
		}

		$model = new ChatImage();
		$model->image_id = $file->id;
		$model->chat_id = $object->id;
		$model->save();

		return $this->renderJson($file);
	}


	/**
	* @api {get} /chat/imageAdd Изображения - добавить
	* @apiName ChatImageAdd
	* @apiGroup Chat
	* @apiDescription Добавить в событие ранее загруженное изображение
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiParam {integer} id ID ChatDTO
	* @apiParam {integer} image_id ID ImageDTO
	*
	* @apiExample Пример запроса
	* /api/chat/imageAdd?id=[chat id]&image_id=[image_id]
	*
	* @apiSuccess (Ответ) {json} object Объект ChatDTO
	*
	* @apiSuccessExample Пример ответа
	*     ChatDTO
	*/
	public function actionImageAdd()
	{
		/* @var $object ChatDTO */
		$object = $this->getObject();

		// TODO: проверка доступа к чату

		$imageId = Yii::app()->request->getParam('image_id');
		if (!$imageId) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed image id');
		}

		$image = ImageDTO::model()->with($with)->findByPk($objectId);

		if (!$image) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'The specified image is not found');
		}

		if ($image->user_id != $this->getUser()->id) {
			throw new ApiException(self::ERR_OBJECT_ACCESS, 'You can not access to this image');
		}

		$model = new ChatImage();
		$model->image_id = $image->id;
		$model->chat_id = $object->id;
		$model->save();

		return $this->renderJson($image);
	}

	/**
	* @api {get} /chat/imageRemove Изображения - удалить
	* @apiName ChatimageRemove
	* @apiGroup Chat
	*
	* @apiUse ResponseHeaderAuth
	* @apiParam {integer} id ID ChatDTO
	* @apiParam {integer} image_id ID ImageDTO
	* @apiParam {boolean} [full] удалить изображение физически (см image/remove)
	*
	* @apiExample Пример запроса
	* /api/chat/imageRemove?id=[chat_id]&image_id=[image_id]
	*
	* @apiSuccessExample Пример ответа
	*     /без данных/
	*/
	public function actionImageRemove()
	{
		/* @var $object ChatDTO */
		$object = $this->getObject();

		// TODO: проверка доступа к чату

		$chatImage = ChatImage::model()->findByAttributes([
			'chat_id' => $object->id,
			'image_id' => Yii::app()->request->getParam('image_id')
		]);

		if (Yii::app()->request->getParam('full')) {
			$chatImage->image->delete();
		}

		$chatImage->delete();

		return $this->renderJson();
	}


	/**
	* @api {get} /chat/imageList Изображения - список
	* @apiName ChatImageList
	* @apiGroup Chat
	*
	* @apiUse ResponseHeaderAuth
	* @apiParam {integer} id ID ChatDTO
	*
	* @apiExample Пример запроса
	* /api/chat/imageList?id=[chat id]
	*
	* @apiSuccess (Ответ) {array} object Массив ImageDTO
	*
	* @apiSuccessExample Пример ответа
	*     [ImageDTO,ImageDTO,...]
	*/
	public function actionImageList()
	{
		/* @var $object ChatDTO */
		$object = $this->getObject();

		return $this->renderJson($object->images);
	}
}

