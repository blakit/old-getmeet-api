<?php


class AlbumController extends ApiController
{
	protected $controllerBaseClass = 'AlbumDTO';
	
	protected function getObject($checkAccess = false, $with = array())
	{
		$object = parent::getObject($with);

		if ($checkAccess) {
			if ($object->user_id != $this->getUser()->id) {
				throw new ApiException(self::ERR_OBJECT_ACCESS, 'Can not access to this object');
			}
		}

		return $object;
	}

	/**
	* @api {post} /album/create Создать AlbumDTO
	* @apiName AlbumCreate
	* @apiGroup Album
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/album/create
	* POST: {AlbumDTO}
	*
	* @apiParam {json} object AlbumDTO
	*
	* @apiSuccess (Ответ) {json} object Созданный объект AlbumDTO
	*
	* @apiSuccessExample Пример ответа
	*     AlbumDTO
	*/
	public function actionCreate()
	{
		$data = Yii::app()->request->getRawBody();

		if (!$data) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed data body');
		}

		$object = new AlbumDTO();
		$object->user_id = $this->getUser()->id;
		
		if (!$object->fromJson($data, true)) {
			throw new ApiException(self::ERR_OBJECT_INVALID, 'Can not parse object');
		}

		$object->user_id = $this->getUser()->id;
		$object->save();

		return $this->renderJson($object);
	}


	/**
	* @api {post} /album/update Изменить AlbumDTO
	* @apiName AlbumUpdate
	* @apiGroup Album
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/album/update?id=[id]
	* POST: {AlbumDTO}
	*
	* @apiParam {integer} id ID AlbumDTO
	* @apiParam {json} object AlbumDTO
	*
	* @apiSuccess (Ответ) {json} object Изменненый объект AlbumDTO
	*
	* @apiSuccessExample Пример ответа
	*     AlbumDTO
	*/
	public function actionUpdate()
	{
		/* @var $object AlbumDTO */
		$object = $this->getObject(true);

		$data = Yii::app()->request->getRawBody();

		if (!$data) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed data body');
		}

		if (!$object->fromJson($data)) {
			throw new ApiException(self::ERR_OBJECT_INVALID, 'Can not parse object');
		}

		$object->user_id= $this->getUser()->id;

		$object->save();

		return $this->renderJson($object);
	}


	/**
	* @api {get} /album/get Получить AlbumDTO
	* @apiName AlbumGet
	* @apiGroup Album
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/album/get?id=[id]
	*
	* @apiParam {integer} id ID AlbumDTO
	*
	* @apiSuccess (Ответ) {json} object Объект AlbumDTO
	*
	* @apiSuccessExample Пример ответа
	*     AlbumDTO
	*/
	public function actionGet()
	{
		/* @var $object AlbumDTO */
		$object = $this->getObject();

		return $this->renderJson($object);
	}

	/**
	* @api {get} /album/list Мои альбомы
	* @apiName AlbumList
	* @apiGroup Album
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/album/list
	*
	* @apiSuccess (Ответ) {array} object Массив AlbumDTO
	*
	* @apiSuccessExample Пример ответа
	*     [AlbumDTO,AlbumDTO,...]
	*/
	public function actionList()
	{
		/* @var $object AlbumDTO */
		$albums = AlbumDTO::model()
			->findAllByAttributes(['user_id' => $this->getUser()->id]);

		return $this->renderJson($albums);
	}

	/**
	* @api {post} /album/imageUpload Изображения - загрузить
	* @apiName AlbumImageUpload
	* @apiGroup Album
	*
	* @apiUse ResponseHeaderAuth
	* @apiUse ErrorUpload
	* 
	* @apiParam {integer} id ID AlbumDTO
	*
	* @apiExample Пример запроса
	* /api/album/imageUpload?id=[album id]
	* Content-Type: multipart/form-data
	*
	* POST: file=[uploadFile]
	*
	* @apiSuccess (Ответ) {json} object Объект ImageDTO
	*
	* @apiSuccessExample Пример ответа
	*     ImageDTO
	*/
	public function actionImageUpload()
	{
		/* @var $object AlbumDTO */
		$object = $this->getObject(true);

		if ( !($file = ImageDTO::upload('file', $this->getUser())) ) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'File not specified');
		}

		$model = new AlbumImage();
		$model->image_id = $file->id;
		$model->album_id = $object->id;
		$model->save();

		return $this->renderJson($file);
	}


	/**
	* @api {get} /album/imageAdd Изображения - добавить
	* @apiName AlbumImageAdd
	* @apiGroup Album
	* @apiDescription Добавить в альбом ранее загруженное изображение
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiParam {integer} id ID AlbumDTO
	* @apiParam {integer} image_id ID ImageDTO
	*
	* @apiExample Пример запроса
	* /api/album/imageAdd?id=[album id]&image_id=[image_id]
	*
	* @apiSuccess (Ответ) {json} object Объект AlbumDTO
	*
	* @apiSuccessExample Пример ответа
	*     AlbumDTO
	*/
	public function actionImageAdd()
	{
		/* @var $object AlbumDTO */
		$object = $this->getObject(true);

		$imageId = Yii::app()->request->getParam('image_id');
		if (!$imageId) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed image id');
		}

		$image = ImageDTO::model()->with($with)->findByPk($objectId);

		if (!$image) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'The specified image is not found');
		}

		if ($image->user_id != $this->getUser()->id) {
			throw new ApiException(self::ERR_OBJECT_ACCESS, 'You can not access to this image');
		}

		$model = new AlbumImage();
		$model->image_id = $image->id;
		$model->album_id = $object->id;
		$model->save();

		return $this->renderJson($image);
	}

	/**
	* @api {get} /album/imageRemove Изображения - удалить
	* @apiName AlbumimageRemove
	* @apiGroup Album
	*
	* @apiUse ResponseHeaderAuth
	* @apiParam {integer} id ID AlbumDTO
	* @apiParam {integer} image_id ID ImageDTO
	* @apiParam {boolean} [full] удалить изображение физически (см image/remove)
	*
	* @apiExample Пример запроса
	* /api/album/imageRemove?id=[album_id]&image_id=[image_id]
	*
	* @apiSuccessExample Пример ответа
	*     /без данных/
	*/
	public function actionImageRemove()
	{
		/* @var $object AlbumDTO */
		$object = $this->getObject(true);

		$albumImage = AlbumImage::model()->findByAttributes([
			'album_id' => $object->id,
			'image_id' => Yii::app()->request->getParam('image_id')
		]);

		if (Yii::app()->request->getParam('full')) {
			$albumImage->image->delete();
		}

		$albumImage->delete();

		return $this->renderJson();
	}

	/**
	* @api {get} /album/imageList Изображения - список
	* @apiName AlbumImageList
	* @apiGroup Album
	*
	* @apiUse ResponseHeaderAuth
	* @apiParam {integer} id ID AlbumDTO
	*
	* @apiExample Пример запроса
	* /api/album/imageList?id=[album id]
	*
	* @apiSuccess (Ответ) {array} object Массив ImageDTO
	*
	* @apiSuccessExample Пример ответа
	*     [ImageDTO,ImageDTO,...]
	*/
	public function actionImageList()
	{
		/* @var $object AlbumDTO */
		$object = $this->getObject();

		return $this->renderJson($object->images);
	}

	
	/**
	* @api {post} /album/coverUpload Обложка - загрузить
	* @apiName AlbumCoverUpload
	* @apiGroup Album
	*
	* @apiUse ResponseHeaderAuth
	* @apiUse ErrorUpload
	*
	* @apiParam {integer} id ID AlbumDTO
	*
	* @apiExample Пример запроса
	* /api/album/coverUpload?id=[album id]
	* Content-Type: multipart/form-data
	*
	* POST: file=[uploadFile]
	*
	* @apiSuccess (Ответ) {json} object Объект AlbumDTO
	*
	* @apiSuccessExample Пример ответа
	*     AlbumDTO
	*/
	public function actionCoverUpload()
	{
		/* @var $object AlbumDTO */
		$object = $this->getObject(true);

		if ( !($file = ImageDTO::upload('file', $this->getUser())) ) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'File not specified');
		}

		$object->image = $file;
		$object->image_id = $file->id;
		$object->save();
		
		return $this->renderJson($object);
	}

	/**
	* @api {get} /album/coverSet Обложка - установить
	* @apiName AlbumCoverSet
	* @apiGroup Album
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiParam {integer} id ID AlbumDTO
	* @apiParam {integer} id ID ImageDTO
	*
	* @apiExample Пример запроса
	* /api/album/coverSet?id=[album id]&image_id=[image_id]
	*
	* @apiSuccess (Ответ) {json} object Объект AlbumDTO
	*
	* @apiSuccessExample Пример ответа
	*     AlbumDTO
	*/
	public function actionCoverSet()
	{
		/* @var $object AlbumDTO */
		$object = $this->getObject(true);

		$imageId = Yii::app()->request->getParam('image_id');
		if (!$imageId) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed image id');
		}

		$image = ImageDTO::model()->with($with)->findByPk($objectId);

		if (!$image) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'The specified image is not found');
		}

		if ($image->user_id != $this->getUser()->id) {
			throw new ApiException(self::ERR_OBJECT_ACCESS, 'You can not access to this image');
		}

		$object->image = $image;
		$object->image_id = $image->id;
		$object->save();

		return $this->renderJson($object);
	}

	/**
	* @api {get} /album/coverRemove Обложка - удалить
	* @apiName AlbumCoverRemove
	* @apiGroup Album
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiParam {integer} id ID AlbumDTO
	* @apiParam {boolean} [full] удалить изображение физически (см image/remove)
	*
	* @apiExample Пример запроса
	* /api/album/coverRemove?id=[album id]
	*
	* @apiSuccess (Ответ) {json} object Объект AlbumDTO
	*
	* @apiSuccessExample Пример ответа
	*     AlbumDTO
	*/
	public function actionCoverRemove()
	{
		/* @var $object AlbumDTO */
		$object = $this->getObject(true);

		if ($object->image) {
			$object->image->delete();
		}

		$object->image = null;
		$object->image_id = null;
		$object->save();

		return $this->renderJson($object);
	}
}

