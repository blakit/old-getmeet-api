<?php


class ImageController extends ApiController
{
	protected $controllerBaseClass = 'ImageDTO';
	
	protected function getObject($checkAccess = false, $with = array())
	{
		$object = parent::getObject($with);

		if ($checkAccess) {
			if ($object->user_id != $this->getUser()->id) {
				throw new ApiException(self::ERR_OBJECT_ACCESS, 'Can not access to this object');
			}
		}

		return $object;
	}

	/**
	* @api {post} /image/upload Загрузить
	* @apiName ImageUpload
	* @apiGroup Image
	* @apiDescription Одна загруженная картинка может быть фактически в
	* нескольких событиях/группах/альбомах.
	*
	* @apiUse ResponseHeaderAuth
	* @apiUse ErrorUpload
	*
	* @apiExample Пример запроса
	* /api/image/upload
	* Content-Type: multipart/form-data
	*
	* POST: file=[uploadFile]
	*
	* @apiSuccess (Ответ) {json} object Объект AlbumDTO
	*
	* @apiSuccessExample Пример ответа
	*     ImageDTO
	*/
	public function actionUpload()
	{
		if ( !($file = ImageDTO::upload('file', $this->getUser())) ) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'File not specified');
		}
		
		return $this->renderJson($file);
	}

	/**
	* @api {get} /image/remove Удалить
	* @apiName ImageRemove
	* @apiGroup Image
	* @apiDescription Этот метод физически удалит картинку с сервера.
	* Если данная картинка используется в качестве обложки события/группы/альбома, либо
	* же размещена в событии/группе/альбоме, то запись об этой картинке пропадет из события/группы/альбома,
	* сами же события/группы/альбомы удалены не будут.
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiParam {integer} id ID ImageDTO
	*
	* @apiExample Пример запроса
	* /api/image/remove?id=[image id]
	*
	* @apiSuccessExample Пример ответа
	*     /без данных/
	*/
	public function actionRemove()
	{
		/* @var $object ImageDTO */
		$object = $this->getObject(true);
		$object->delete();


		return $this->renderJson();
	}


	/**
	* @api {get} /image/list Список
	* @apiName ImageList
	* @apiGroup Image
	* @apiDescription Все загруженные пользователем изображения
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/image/list
	*
	* @apiSuccessExample Пример ответа
	*     [ImageDTO,ImageDTO,...]
	*/
	public function actionList()
	{
		/* @var $object ImageDTO[] */
		$object = $this->getUser()->images;

		return $this->renderJson($object);
	}
}

