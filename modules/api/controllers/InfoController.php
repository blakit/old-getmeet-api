<?php

class InfoController extends ApiController
{
	/**
	 *
	 * @api {get} /info/version Версия АПИ
	 * @apiName VersionInfo
	 * @apiGroup Info
	 * @apiDescription Возвращает версию АПИ
	 *
	 * @apiSuccess (Ответ) {String} version Версия АПИ
	 *
	 * @apiSuccessExample Пример ответа
	 *    '1.0.0'
	 *
	 */
	public function actionVersion()
	{
		$this->renderJson(self::API_VERSION);
	}
}

