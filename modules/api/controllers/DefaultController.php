<?php

class DefaultController extends ApiController
{
	public function actionError()
	{
		if($error = Yii::app()->errorHandler->error) {
			if ($error['code'] != 404) {
				throw new RuntimeException($error['type'] . ': ' . $error['message'], $error['code']);
			}
		}
		
		throw new ApiException(self::ERR_METHOD_INVALID, 'Unknown method');
	}
}

