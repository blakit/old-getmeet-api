<?php



class GroupController extends ApiController
{
	public function filters()
	{
		$filters = parent::filters();

		$filters = array_merge([
			'Access -Error',
		], $filters);

		return $filters;
	}

	/**
	* @api {post} /group/create Создать GroupDTO
	* @apiName GroupCreate
	* @apiGroup Group
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/group/create
	* POST: {GroupDTO}
	*
	* @apiParam {json} object GroupDTO
	*
	* @apiSuccess (Ответ) {json} object Созданный объект GroupDTO
	*
	* @apiSuccessExample Пример ответа
	*     GroupDTO
	*/
	public function actionCreate()
	{
		$data = Yii::app()->request->getRawBody();

		if (!$data) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed data body');
		}

		$object = new GroupDTO();

		if (!$object->fromJson($data, true)) {
			throw new ApiException(self::ERR_OBJECT_INVALID, 'Can not parse object');
		}

		$object->admin_user_id = $this->getUser()->id;

		$object->save();

		return $this->renderJson($object);
	}


	/**
	 * @api {post} /group/update Изменить GroupDTO
	 * @apiName GroupUpdate
	 * @apiGroup Group
	 *
	 * @apiUse ResponseHeaderAuth
	 *
	 * @apiExample Пример запроса
	 * /api/group/update?id=[id]
	 * POST: {GroupDTO}
	 *
	 * @apiParam {integer} id ID Group
	 * @apiParam {json} object GroupDTO
	 *
	 * @apiSuccess (Ответ) {json} object Изменненый объект GroupDTO
	 *
	 * @apiSuccessExample Пример ответа
	 *     GroupDTO
	 */
	public function actionUpdate()
	{
		$objectId = Yii::app()->request->getParam('id');

		if (!$objectId) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed object id');
		}

		$data = Yii::app()->request->getRawBody();

		if (!$data) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed data body');
		}

		/* @var $object GroupDTO */
		$object = GroupDTO::model()->findByPk($objectId);

		if (!$object) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'Specified object is not found');
		}

		if ($object->admin_user_id != $this->getUser()->id) {
			throw new ApiException(self::ERR_OBJECT_ACCESS, 'Can not access to this object');
		}

		if (!$object->fromJson($data)) {
			throw new ApiException(self::ERR_OBJECT_INVALID, 'Can not parse object');
		}

		$object->admin_user_id= $this->getUser()->id;

		$object->save();

		return $this->renderJson($object);
	}


	/**
	* @api {get} /group/get Получить GroupDTO
	* @apiName GroupGet
	* @apiGroup Group
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/group/get?id=[id]
	*
	* @apiParam {integer} id ID Group
	*
	* @apiSuccess (Ответ) {json} object Объект GroupDTO
	*
	* @apiSuccessExample Пример ответа
	*     GroupDTO
	*/
	public function actionGet()
	{
		$objectId = Yii::app()->request->getParam('id');

		if (!$objectId) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed object id');
		}
		
		/* @var $object GroupDTO */
		$object = GroupDTO::model()->findByPk($objectId);

		if (!$object) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'The specified object is not found');
		}

		return $this->renderJson($object);
	}

	/**
	* @api {get} /group/userList Получить список пользователей
	* @apiName GroupUserList
	* @apiGroup Group
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/group/userList?id=[id]
	*
	* @apiParam {integer} id ID Group
	*
	* @apiSuccess (Ответ) {json} array Массив PersonDTO
	*
	* @apiSuccessExample Пример ответа
	*     [PersonDTO,PersonDTO,...]
	*/
	public function actionUserList()
	{
		$objectId = Yii::app()->request->getParam('id');

		if (!$objectId) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed object id');
		}

		/* @var $object GroupDTO */
		$object = GroupDTO::model()->findByPk($objectId);

		if (!$object) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'Specified object is not found');
		}

		return $this->renderJson($object->users);
	}


	/**
	* @api {get} /group/userAdd Добавить пользователя
	* @apiName GroupUserAdd
	* @apiGroup Group
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/group/userAdd?id=[id]&user_id=[user_id]
	*
	* @apiParam {integer} id ID Group
	* @apiParam {integer} id ID User
	*
	* @apiSuccessExample Пример ответа
	*     /без данных/
	*/
	public function actionUserAdd()
	{
		$objectId = Yii::app()->request->getParam('id');
		$userId = Yii::app()->request->getParam('user_id');

		if (!$objectId) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed object id');
		}
		if (!$userId) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed user id');
		}

		/* @var $object GroupDTO */
		$object = GroupDTO::model()->findByPk($objectId);
		if (!$object) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'Specified object is not found');
		}


		if ($object->admin_user_id != $this->getUser()->id) {
			throw new ApiException(self::ERR_OBJECT_ACCESS, 'Can not access to this object');
		}


		/* @var $user UserDTO */
		$user = UserDTO::model()->findByPk($userId);
		if (!$user) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'Specified user is not found');
		}

		$object->users = array_merge($object->users, array($user));
		$object->saveWithRelated('users');

		return $this->renderJson();
	}


	/**
	* @api {get} /group/userRemove Удалить пользователя
	* @apiName GroupUserRemove
	* @apiGroup Group
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/group/userAdd?id=[id]&user_id=[user_id]
	*
	* @apiParam {integer} id ID Group
	* @apiParam {integer} id ID User
	*
	* @apiSuccessExample Пример ответа
	*     /без данных/
	*/
	public function actionUserRemove()
	{
		$objectId = Yii::app()->request->getParam('id');
		$userId = Yii::app()->request->getParam('user_id');

		if (!$objectId) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed object id');
		}
		if (!$userId) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed user id');
		}

		/* @var $object GroupDTO */
		$object = GroupDTO::model()->findByPk($objectId);
		if (!$object) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'Specified object is not found');
		}


		if ($object->admin_user_id != $this->getUser()->id) {
			throw new ApiException(self::ERR_OBJECT_ACCESS, 'Can not access to this object');
		}


		/* @var $user UserDTO */
		$user = UserDTO::model()->findByPk($userId);
		if (!$user) {
			throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'Specified user is not found');
		}

		$userList = $object->users;
		foreach ($userList as $key => $value) {
			if ($value->id == $user->id) {
				unset($userList[$key]);
			}
		}
		
		$object->users = $userList;
		$object->saveWithRelated('users');

		return $this->renderJson();
	}


	/**
	* @api {get} /group/userJoin Вступить в группу
	* @apiName GroupUserJoin
	* @apiGroup Group
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/group/userJoin?id=[id]
	*
	* @apiParam {integer} id ID Group
	*
	* @apiSuccessExample Пример ответа
	*     /без данных/
	*/
	public function actionUserJoin()
	{
		$objectId = Yii::app()->request->getParam('id');

		if (!$objectId) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed object id');
		}

		$object->users = array_merge($object->users, array($this->getUser()));
		
		$object->saveWithRelated('users');

		return $this->renderJson();
	}


	/**
	* @api {get} /group/userGetOut Выйти из группы
	* @apiName GroupUserGetOut
	* @apiGroup Group
	*
	* @apiUse ResponseHeaderAuth
	*
	* @apiExample Пример запроса
	* /api/group/userGetOut?id=[id]
	*
	* @apiParam {integer} id ID Group
	*
	* @apiSuccessExample Пример ответа
	*     /без данных/
	*/
	public function actionUserGetOut()
	{
		$objectId = Yii::app()->request->getParam('id');

		if (!$objectId) {
			throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed object id');
		}

		$userList = $object->users;
		foreach ($userList as $key => $value) {
			if ($value->id == $this->getUser()->id) {
				unset($userList[$key]);
			}
		}

		$object->users = $userList;
		$object->saveWithRelated('users');

		return $this->renderJson();
	}
	
}

