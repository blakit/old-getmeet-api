<?php


class ApiException extends Exception {
	public function __construct($code, $message = null, $previous = null)
	{
		parent::__construct($message, $code, $previous);
	}
}