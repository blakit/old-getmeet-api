<?php
/**
 * @apiDefine ResponseHeaderAuth
 * 
 * @apiHeader (Заголовок) {String} authorization Авторизациионный токен (см. /user/login/)
 */

/**
 * @apiDefine ErrorUpload
 *
 * @apiError (Ошибки загрузки) 10051 ERR_UPLOAD_FORMAT Недопустимое расширение файла
 * @apiError (Ошибки загрузки) 10052 ERR_UPLOAD_SIZE Слишком большой размер файла
 * @apiError (Ошибки загрузки) 10053 ERR_UPLOAD_SERVER Ошибка сервера (к примеру закончилось место на диске)
 */

/**
 * @api {method} / Общий формат запроса
 * @apiName RequestFormat
 * @apiGroup Common
 *
 * @apiDescription Используются GET или POST, в зависимости от метода АПИ. Все объекты кодируются в формат JSON.
 *
 * @apiExample Пример GET
 *		/api/group/method?param1=[value]&param2=[value]
 * @apiExample Пример POST
 *		/api/group/method
 *		POST: param1=[value]&param2=[value]
 * @apiExample Пример POST c JSON объектом
 *		/api/group/method
 *		POST: {"id":1,...}
 */


/**
 * @api {method} / Общий формат ответа
 * @apiName ResponseFormat
 * @apiGroup Common
 *
 * @apiDescription Пример ответа сервера. Весь ответ кодируется в JSON.
 * 
 * @apiError (Общие ошибки) 0 ERR_UNKNOWN
 * @apiError (Общие ошибки) 1 ERR_SERVER_ERROR
 * @apiError (Общие ошибки) 11 ERR_NEED_AUTH
 *
 * @apiError (Общие ошибки) 10001 ERR_METHOD_INVALID
 * @apiError (Общие ошибки) 10002 ERR_METHOD_DEPRECATED
 * @apiError (Общие ошибки) 10003 ERR_METHOD_DONE этот метод был выполнен ранее и не может быть выполнен повторно
 * @apiError (Общие ошибки) 10004 ERR_METHOD_NOT_IMPLEMENTED этот метод не может быть вызван в данных условиях
 *
 * @apiError (Общие ошибки) 10005 ERR_ARGUMENT_MISSED
 * @apiError (Общие ошибки) 10006 ERR_ARGUMENT_INVALID
 *
 * @apiError (Общие ошибки) 10011 ERR_OBJECT_NOT_FOUND не найден запрошенный объект (например для методов get)
 * @apiError (Общие ошибки) 10012 ERR_OBJECT_ACCESS нет доступа к запрошенному объекту
 * @apiError (Общие ошибки) 10013 ERR_OBJECT_INVALID формат входящего DTO объекта (например для методов update/create) неверен
 * @apiError (Общие ошибки) 10014 ERR_OBJECT_EXIST такая запись уже есть
 *
 * @apiSuccessExample Успешный ответ
 *     {
 *       "status": "success",
 *       "data" : [ ... ] //[optional] данные ответа
 *     }
 * 
 * @apiErrorExample Ошибочный ответ
 *     {
 *       "status": "error",
 *       "error" : "Error number",
 *       "message" : "Some message" //[optional]
 *     }
 */
abstract class ApiController extends Controller {

	const ERR_UNKNOWN			 = 0;
	const ERR_SERVER_ERROR		 = 1;

	const ERR_NEED_AUTH			 = 11;
	
	const ERR_METHOD_INVALID	 = 10001;
	const ERR_METHOD_DEPRECATED	 = 10002;
	const ERR_METHOD_DONE		 = 10003;
	const ERR_METHOD_NOT_IMPLEMENTED = 10004;

	const ERR_ARGUMENT_MISSED	 = 10005;
	const ERR_ARGUMENT_INVALID	 = 10006;

	const ERR_OBJECT_NOT_FOUND	 = 10011;
	const ERR_OBJECT_ACCESS		 = 10012;
	const ERR_OBJECT_INVALID	 = 10013;
	const ERR_OBJECT_EXIST		 = 10014;

	const ERR_UPLOAD_FORMAT		 = 10051;
	const ERR_UPLOAD_SIZE		 = 10052;
	const ERR_UPLOAD_SERVER		 = 10053;
	
	const API_VERSION = '0.0.1';

	const PAGINATION_LIMIT_DEFAULT = 20;
	const PAGINATION_LIMIT_MAX = 100;

	public function filters()
	{
		return array(
			'UpdateLastTime',
		);
	}

	private $authUser = null;
	private $authStatus = null;

	protected function setUser($user)
	{
		if ($user) {
			$this->authUser = $user;
			$this->authStatus = true;
		}
		return $this;
	}

	/**
	 * @return UserDTO
	 * @throws ApiException on missed or invalid token
	 */
	protected function getUser()
	{
		if (!$this->authUserTry()) {
			throw new ApiException(self::ERR_NEED_AUTH, 'Need auth');
		}

		return $this->authUser;
	}

	/**
	 * @return boolean
	 * @throws ApiException on invalid token
	 */
	protected function isUser()
	{
		return ($this->authUserTry());
	}

	private function authUserTry()
	{
		if (is_null($this->authStatus)) {
			$this->authStatus = false;

			$token = null;
			
			if(isset($_SERVER['HTTP_AUTHORIZATION'])) {
				$token = $_SERVER['HTTP_AUTHORIZATION'];
			}

			if (YII_DEBUG && !$token) {
				$token = Yii::app()->request->getParam('token');
			}

			if ($token) {
				if (!preg_match('#^[a-f0-9]{32}$#i', $token)) {
					throw new ApiException(self::ERR_NEED_AUTH, 'Token invalid');
				}
				
				$user = UserDTO::model()->findByToken($token);

				if (!$user) {
					throw new ApiException(self::ERR_NEED_AUTH, 'Unknown token');
				}

				$this->setUser($user);
				$this->authStatus = true;
			}
		}

		return $this->authStatus;
	}

	public function filterAccess($filterChain)
	{
		if (!$this->authUserTry()) {
			throw new ApiException(self::ERR_NEED_AUTH, 'Missed token');
		}

		$filterChain->run();
	}

	public function getJsonBody()
	{
		$data = null;
		
		if (YII_DEBUG) {
			if (Yii::app()->request->getParam('body')) {
				$data = Yii::app()->request->getParam('body');
			}
		}

		if (!$data) {
			$data = Yii::app()->request->getRawBody();
			if (!$data) {
				return false;
			}

			$data = JsonHelper::decode($data);
		}

		if (!$data) {
			return false;
		}

		return $data;
	}

	public function filterUpdateLastTime($filterChain)
	{
		/* TODO: Подумать, точно ли это нужно */
		if ($this->isUser()) {
			// обновляем last_date при каждом обращении к АПИ
			$this->getUser()->last_date = DateHelper::TimeSql();
		}

		$filterChain->run();
	}

	private $dbTransaction = null;

	public function run($actionID)
	{
		$this->dbTransaction = false;
		if (!Yii::app()->db->currentTransaction) {
		    $this->dbTransaction = Yii::app()->db->beginTransaction();
		}

		$result = false;
		
		try {
			parent::run($actionID);

			$result = true;
		} catch (ApiException $e) {
			$this->renderJsonError($e->getCode(), $e->getMessage());
		} catch (Exception $e) {
			if (YII_DEBUG) {
				throw $e;
				//$this->renderJsonError(self::ERR_SERVER_ERROR, array('Server error: [%d] %s', $e->getCode(), $e->getMessage()) ) ;
			} else {
				Yii::log(__METHOD__ . ': [' . $e->getCode() . '] ' . $e->getMessage(), 'error');
				
				$this->renderJsonError(self::ERR_SERVER_ERROR, 'Server error, try again later');
			}
		}

		if ($this->dbTransaction) {
			if ($result) {
				$this->dbTransaction->commit();
			} else {
				$this->dbTransaction->rollback();
			}
		}
	}
	
	public function afterAction($action)
	{
		if ($this->isUser()) {
			$this->getUser()->save();
		}

		parent::afterAction($action);
	}

	protected function errorGetMessage($errorNum, $errorMessage = null)
	{
		if (is_array($errorMessage)) {
			if (count($errorMessage)) {
				$params = [];
				
				foreach ($errorMessage as $key => $value) {
					$params[$key] = TranslateHelper::translate($value);
				}

				$errorMessage = call_user_func_array('sprintf', $params);
			} else {
				$errorMessage = null;
			}
		} else {
			$errorMessage = TranslateHelper::translate($errorMessage);
		}

		if ($errorNum) {
			/* TODO */
		}

		if (is_null($errorMessage)) {
			$errorMessage = TranslateHelper::translate('Unknown error');
		}

		return $errorMessage;
	}

	protected function errorHttpCode($errorNum)
	{
		switch ($errorNum) {
			case self::ERR_METHOD_INVALID:
				return 404;
				break;

			case self::ERR_NEED_AUTH:
				return 401;
				break;

			default:
				return 400;
				break;
		}
	}

	protected $controllerBaseClass = 'CModel';
	private $controllerObject = null;
	
	protected function getObject($with = array())
	{
		if (!$this->controllerObject) {
			$objectId = Yii::app()->request->getParam('id');

			if (!$objectId) {
				throw new ApiException(self::ERR_ARGUMENT_MISSED, 'Missed object id');
			}

			$baseModel = $this->controllerBaseClass;
			/* @var $object CModel */
			$object = $baseModel::model()->with($with)->findByPk($objectId);

			if (!$object) {
				throw new ApiException(self::ERR_OBJECT_NOT_FOUND, 'The specified object is not found');
			}

			$this->controllerObject = $object;
		}

		return $this->controllerObject;
	}
	

	protected function renderJson($data = null)
	{
		$response = ['status' => 'success'];

		if (!is_null($data)) {
			$response['data'] = $data;
		}

		$data = JsonHelper::encode($response);
		
		if (!headers_sent()) {
			header('Content-Type: application/json');
			http_response_code(200);
		}

		echo($data);
	}

	protected function renderJsonError($errorNum = self::ERR_UNKNOWN, $errorMessage = null)
	{
		$errorMessage = $this->errorGetMessage($errorNum, $errorMessage);

		$response = ['status' => 'error', 'error' => $errorNum];
		
		if ($errorMessage) {
			$response['message'] = $errorMessage;
		}

		$data = JsonHelper::encode($response);
		
		if (!headers_sent()) {
			header('Content-Type: application/json');
			http_response_code($this->errorHttpCode($errorNum));
		}

		echo($data);
	}


	/**
	* @apiDefine RequestPagination
	*
	* @apiParam (Пагинация) {Integer} [offset] смещение
	* @apiParam (Пагинация) {Integer} [limit] количество (не больше 100) записей
	*/
	protected function searchLimitOffset(&$offset, &$limit)
	{
		$offset = Yii::app()->request->getParam('offset');
		$limit = Yii::app()->request->getParam('limit');

		if (empty($offset)) {
			$offset = 0;
		}

		if (empty($limit)) {
			$limit = self::PAGINATION_LIMIT_DEFAULT;
		}

		$offset = (int)$offset;
		$limit = (int)$limit;

		if ($offset < 0) {
			throw new ApiException(self::ERR_ARGUMENT_INVALID, 'Offset out of range');
		}

		if ($limit < 0 || $limit > self::PAGINATION_LIMIT_MAX) {
			throw new ApiException(self::ERR_ARGUMENT_INVALID, 'Limit out of range');
		}

		return true;
	}


	protected function searchLimitOffsetCriteria(&$criteria)
	{
		$this->searchLimitOffset($offset, $limit);

		$criteria->offset = $offset;
		$criteria->limit = $limit;

		return true;
	}
}
