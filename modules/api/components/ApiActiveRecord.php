<?php

class ApiActiveRecord extends CActiveRecord
	implements JsonSerializable
{
	use JsonModel;

	protected function locationModelDefault()
	{
		return 'LocationDTO';
	}
	
	/**
	 * Переопределяем загрузку связей на DTO объекты
	 */
	private static $_md=array();
	
	public function getMetaData()
	{
		$className=get_class($this);

		if(!array_key_exists($className,self::$_md)) {
			self::$_md[$className]=null; // preventing recursive invokes of {@link getMetaData()} via {@link __get()}
			self::$_md[$className]=new ApiActiveRecordMetaData($this);
		}

		return self::$_md[$className];
	}
}