<?php

trait JsonModel {
	public function jsonSerialize()
	{
		return $this->toJson(false);
	}
	
	public function toJson($encode = true)
	{
		$data = $this->getAttributes();

		if (method_exists($this, 'toJsonFilter')) {
			$data = $this->toJsonFilter($data);
		}

		if ($encode) {
			return JsonHelper::encode($data);
		}

		return $data;
	}

	public function fromJson($data, $newObject = false, $validate = true)
	{
		if (is_scalar($data)) {
			$data = JsonHelper::decode($data);
		}

		if (!is_array($data)) {
			return false;
		}

		if (method_exists($this, 'setIsNewRecord')) {
			if ($newObject) {
				$this->setIsNewRecord(true);
			} else {
				$this->setIsNewRecord(false);

				if (
					isset($data['id'])
					&& ($data['id'] != $this->getPrimaryKey())
				) {
					return false;
				}
			}
		}

		$data = $this->fromJsonFilter($data, $newObject = false);

		$this->setAttributes($data, true);

		if ($validate) {
			if (!$this->validate()) {
				$errors = [];

				foreach($this->getErrors() as $error) {
					$errors[] = array_pop($error);
				}

				if (count($errors)) {
					throw new ApiException(ApiController::ERR_OBJECT_INVALID, 'Object invalid: '. implode(' | ', $errors));
				}
			}
		}

		return true;
	}


	protected function fromJsonFilter($data, $newObject = false)
	{
		if (array_key_exists('id', $data)) {
			unset($data['id']);
		}

		return $data;
	}

	protected function toJsonFilter($data)
	{
		return $data;
	}

}