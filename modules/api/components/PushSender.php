<?php

use sngrl\PhpFirebaseCloudMessaging\Client;
use sngrl\PhpFirebaseCloudMessaging\Message;
use sngrl\PhpFirebaseCloudMessaging\Recipient\Device;
use sngrl\PhpFirebaseCloudMessaging\Notification;

class PushSender {
	private $devices = array();
	private $exclude = array();
	private $apiKey = null;
	private $data = null;
	private $priority = 'high';
	private $title = null;
	private $body = null;

	public static function init() {
		return new self();
	}

	public function __construct()
	{
		$this->apiKey = Yii::app()->params['firebaseKey'];
	}

	public function addDevice($deviceToken)
	{
		if (is_array($deviceToken)) {
			$this->devices = array_merge($this->devices, $deviceToken);
		} else {
			$this->devices[] = $deviceToken;
		}

		return $this;
	}

	public function addExcludeDevice($deviceToken)
	{
		$tokenList = $this->getDeviceToken($deviceToken);

		$this->exclude = array_merge($this->exclude, $tokenList);

		return $this;
	}

	public function setData($data)
	{
		$this->data = $data;
		return $this;
	}

	public function setMessage($title, $body)
	{
		$this->title = $title;
		$this->body = $body;
		return $this;
	}

	public function setPriority($priority)
	{
		$this->priority = $priority;
		return $this;
	}

	private function getDeviceToken($object)
	{
		
		if (is_array($object)) {
			$result = array();

			foreach ($object as $item) {
				$result = array_merge($result, $this->getDeviceToken($item));
			}

			return $result;
		} else if ($object instanceof UserToken) {
			return [$object->token];
		} elseif ($object instanceof User) {
			if ( ($item = $object->getTokenPush()) ) {
				return $this->getDeviceToken($item);
			}
		} elseif (is_scalar($object)) {
			return [$object];
		}

		return [];
	}

	private function addRecipientList($message, $device)
	{
		try {
			$tokenList = $this->getDeviceToken($device);
			$tokenList = array_unique($tokenList);

			foreach ($tokenList as $token) {
				if (in_array($token, $this->exclude)) {
					continue;
				}
				
				$message->addRecipient( new Device($token) );
			}

		} catch(Exception $ex) {
			Yii::log($ex->getCode() . ': '. $ex->getMessage(), CLogger::LEVEL_WARNING);
			if ($ex->getPrevious()) {
				$ex = $ex->getPrevious();
				Yii::log($ex->getCode() . ': '. $ex->getMessage(), CLogger::LEVEL_WARNING);
			}
			
			return false;
		}

		return true;
	}

	private function sendPush($data, $devices, $title, $body)
	{
		try {
			$message = new FirebaseMessageMultiple();
			$message->setPriority($this->priority);
			$this->addRecipientList($message, $devices);

			if ($title) {
				$message->setNotification(new Notification($title, $body));
			}
			if ($data) {
				$message->setData($data);
			}

			if (count($message->getRecipients())) {
				$httpClient = new \GuzzleHttp\Client([
					'verify' => false,
				]);

				$client = new Client();
				$client->setApiKey($this->apiKey);
				$client->injectGuzzleHttpClient($httpClient);

				$response = $client->send($message);

				if (YII_DEBUG) {
					Yii::log($response->getStatusCode(), CLogger::LEVEL_WARNING);
					Yii::log($response->getBody()->getContents(), CLogger::LEVEL_WARNING);
				}

				return true;
			} else {
				return false;
			}
		} catch (Exception $ex) {
			if ($ex instanceof \GuzzleHttp\Exception\RequestException) {
				if (YII_DEBUG) {
					throw $ex;
				}

				Yii::log($ex->getResponse()->getStatusCode(), CLogger::LEVEL_WARNING);
				Yii::log($ex->getResponse()->getBody()->getContents(), CLogger::LEVEL_WARNING);

				return false;
			}

			Yii::log($ex->getCode() . ': '. $ex->getMessage(), CLogger::LEVEL_WARNING);
			
			if ($ex->getPrevious()) {
				$ex = $ex->getPrevious();
				Yii::log($ex->getCode() . ': '. $ex->getMessage(), CLogger::LEVEL_WARNING);
			}

			if (YII_DEBUG) {
				throw $ex;
			}

			return false;
		}
	}

	public function sendRaw($data = null, $devices = null, $title = null, $body = null)
	{
		if (is_null($title)) {
			$title = $this->title;
		}
		if (is_null($body)) {
			$body = $this->body;
		}
		if (is_null($data)) {
			$data = $this->data;
		}
		if (is_null($devices)) {
			$devices = $this->devices;
		}
	
		if (is_null($devices)) {
			return false;
		}

		return $this->sendPush($data, $devices, $title, $body);
	}

	const PUSH_CHAT_MESSAGE	 = 1;
	const PUSH_EVENT_ADD	 = 2;
	const PUSH_EVENT_BLITS_ADD = 3;
	const PUSH_CHAT_ADD		 = 4;
	const PUSH_CHAT_REMOVE	 = 5;
	const PUSH_EVENT_REMOVE	 = 6;
	const PUSH_FRIENDS_ADD	 = 7;
	const PUSH_EVENT_START	 = 8;

	public function send($pushId, $data = array(), $devices = null)
	{
		if (!is_null($this->data)) {
			$data = array_merge($this->data, $data);
		}

		if (empty($data['push_id'])) {
			$data['push_id'] = $pushId;
		}
	
		return $this->sendRaw($data, $devices);
	}
}