<?php

abstract class ApiModel extends CFormModel
	implements JsonSerializable
{
	use JsonModel;

	/**
	 * Заготовка метода для наполнения модели
	 */
	public function import($object)
	{
		
	}

	/**
	 *
	 * @param CModel $object
	 * @param array $param
	 */
	protected function importDataMake($object, $param)
	{
		if (!$object instanceof CModel) {
			return [];
		}
		
		$result = array();

		$atttributes = $object->attributeNames();

		foreach ($param as $attr) {
			if (in_array($attr, $atttributes)) {
				$result[$attr] = $object->$attr;
			}
		}

		return $result;
	}
}