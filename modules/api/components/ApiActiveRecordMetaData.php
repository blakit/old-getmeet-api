<?php

/**
 *  Переопределяем связи на использование DTO моделей если такие существуют
 */
class ApiActiveRecordMetaData extends CActiveRecordMetaData {
	
	public function addRelation($name, $config)
	{
		$result = $config;

		if(isset($result[0], $result [1], $result [2])) {
			if (substr($result [1], -3) != 'DTO') {
				$newclass = $result [1] . 'DTO';

				try {
					if (yii::import('api.models.' . $newclass, true)) {
						$result[1] = $newclass;
					}
				} catch (Exception $ex) {

				}
				
			}
		}

		return parent::addRelation($name,$result );
	}
	
}