<?php

class ApiModule extends CWebModule
{
	public function init()
	{
		$this->setImport(array(
			'api.models.*',
			'api.components.*',
		));

		Yii::import('api.components.AplicationActiveRecord');

		Yii::app()->setComponents(array(
			'errorHandler'=>array(
				'errorAction'=>'api/default/error',
			),
		));
	}


}
