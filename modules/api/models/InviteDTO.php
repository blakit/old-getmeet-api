<?php

/**
 * @api {dto} / InviteDTO
 * @apiGroup DTO
 * @apiName InviteDTO
 *
 * @apiParam (Значения) {integer} id
 * @apiParam (Значения) {integer} type тип приглашения
 * @apiParam (Значения) {integer} innvite_params_id параметры приглашения
 * @apiParam (Значения) {integer} event_id мероприятие
 * @apiParam (Значения) {integer} owner_id создатель инвайта
 */
class InviteDTO extends Invite
{
	
}
