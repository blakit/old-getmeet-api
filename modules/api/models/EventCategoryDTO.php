<?php

/**
 * @api {dto} / EventCategoryDTO
 * @apiGroup DTO
 * @apiName EventCategoryDTO
 *
 * @apiParam (Значения) {integer} id
 * @apiParam (Значения) {string} name название категории
 * @apiParam (Значения) {integer} [parent_id] Родительский EventCategoryDTO, если NULL - текущая категория корневая
 * @apiParam (Значения) {object} [parent] EventCategoryDTO рекурсивно, если NULL - текущая категория корневая
 */
class EventCategoryDTO
	extends EventCategory
{

	private $outputFull = true;

	/** Формировать полную модель
	 *
	 * @param boolean $value
	 * @return EventDTO
	 */
	public function setOutputFull($value = true)
	{
		$this->outputFull = $value;

		return $this;
	}

	protected function toJsonFilter($data)
	{			
		if ($this->outputFull) {
			$data['parent'] = $this->parent;
		} else {
			unset($data['parent']);
		}

		return $data;
	}
}