<?php
/*

@property\s([^\s]+)\s\$
@apiParam (Значения) {$1} 

\t*const\s+(.+?)\s+\=\s+(.+?);\s
\t * @apiError (Ошибка) $2 $1\n

*/

/**
 * @api {dto} / UserDTO
 * @apiGroup DTO
 * @apiName UserDTO
 *
 * @apiParam (Значения) {integer} id уникальный идентификатор пользователя
 * @apiParam (Значения) {string} token токен доступа
 * @apiParam (Значения) {boolean} logged true если пользователь авторизирован, и false если нет
 * @apiParam (Значения) {ProfileDTO} [profile] Объект ProfileDTO связанный с данным пользователем.
 *    При вызове метода /user/update, все изменения данного объекта также сохраняются.
 * @apiParam (Значения) {TimestampUTC} last_date (readonly) время последней активности
 * @apiParam (Значения) {TimestampUTC} date_registred (readonly) дата регистрации пользователя в системе
 * @apiParam (Значения) {boolean} status true - пользователь онлайн, false - офлайн
 * @apiParam (Значения) {string} email Email пользователя
 * @apiParam (Значения) {string} [password] Хеш пароля пользователя. Сервер не отправляет хеш пользователя.
 *     При получении клиентом данных с сервера, это поле будет пустым. Если вы хотите изменить пароль,
 *     то при вызове метода /user/update, передайте новый пароль в это поле.
 * @apiParam (Значения) {string} vk_id ID вконтакте
 * @apiParam (Значения) {string} fb_id ID фейсбук
 * @apiParam (Значения) {string} tw_id ID твитер
 * @apiParam (Значения) {string} mm_id ID мой мир
 */
class UserDTO extends User {
	private $outputPersonDTO = true;

	/** Формировать PersonDTO по умолчанию
	 *
	 * @param boolean $value
	 * @return UserDTO
	 */
	public function setOutputDefaultPersonDTO($value = true)
	{
		$this->outputPersonDTO = $value;
		return $this;
	}

	/**
	 * @return PersonDTO
	 */
	public function toPersonDTO()
	{
		$object = new PersonDTO();

		$object->import($this);
		
		return $object;
	}

	public function toJson($encode = true)
	{
		if ($this->outputPersonDTO) {
			return $this->toPersonDTO()->toJson($encode);
		}
		
		return parent::toJson($encode);
	}
	

	protected function toJsonFilter($data)
	{
		$data = parent::toJsonFilter($data);

		if (isset($data['password'])) {
			$data['password'] = null;
		}

		if (isset($data['profile_id'])) {
			$objectId = $data['profile_id'];
			unset($data['profile_id']);

			$data['profile'] = $this->profile;
		}

		if (!empty($data['last_date'])) {
			$data['last_date'] = DateHelper::ConvSql2Unix($data['last_date']);
		}

		if (!empty($data['date_registred'])) {
			$data['date_registred'] = DateHelper::ConvSql2Unix($data['date_registred']);
		}

		return $data;
	}

	protected function fromJsonFilter($data, $newObject = false)
	{
		$data = parent::fromJsonFilter($data);

		if (isset($data['password'])) {
			if (!PasswordHelper::isHash($data['password'])) {
				$data['password'] = PasswordHelper::hashPassword($data['password']);
			}
		}

		if (!empty($data['last_date'])) {
			unset($data['last_date']);
		}

		if (!empty($data['date_registred'])) {
			unset($data['date_registred']);
		}

		if (array_key_exists('is_deleted', $data)) {
			unset($data['is_deleted']);
		}

		return $data;
	}

	public function fromJson($data, $newObject = false, $validate = true)
	{
		if (is_scalar($data)) {
			$data = JsonHelper::decode($data);
		}

		if (!is_array($data) || empty($data)) {
			return false;
		}

		if (isset($data['profile'])) {
			$profileData = $data['profile'];
			unset($data['profile']);
		} else {
			$profileData = null;
		}

		if (!parent::fromJson($data, $newObject)) {
			return false;
		}

		if ($newObject) {
			$profile = ProfileDTO::model();
		} elseif ($this->profile) {
			$profile = $this->profile;
		} else {
			$profile = ProfileDTO::model()->findByPk($this->profile_id);
		}

		if ($profileData) {
			if (!$profile->fromJson($profileData, $newObject)) {
				return false;
			}
		}

		$this->profile = $profile;

		return true;
	}
}
