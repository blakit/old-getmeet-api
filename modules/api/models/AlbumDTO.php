<?php

/**
 * @api {dto} / AlbumDTO
 * @apiGroup DTO
 * @apiName AlbumDTO
 * 
 * @apiParam (Значения) {integer} id
 * @apiParam (Значения) {string} name имя альбома
 * @apiParam (Значения) {object} user владлец
 * @apiParam (Значения) {ImageDTO} image (readonly) заглавная картинка
 */
class AlbumDTO extends Album
{
	protected function toJsonFilter($data)
	{
		$data = parent::toJsonFilter($data);

		if (isset($data['password'])) {
			$data['password'] = null;
		}

		if (isset($data['user_id'])) {
			unset($data['user_id']);

			$data['user'] = $this->user;
		}

		if (isset($data['image_id'])) {
			unset($data['image_id']);

			$data['image'] = $this->image;
		}

		return $data;
	}


	protected function fromJsonFilter($data, $newObject = false)
	{
		$data = parent::fromJsonFilter($data, $newObject);

		// Image
		if (isset($data['image_id'])) {
			unset($data['image_id']);
		}

		if (isset($data['image'])) {
			unset($data['image']);
		}

		return $data;
	}
}
