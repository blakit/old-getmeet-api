<?php


/**
 * @api {dto} / ProfileDTO
 * @apiGroup DTO
 * @apiName ProfileDTO
 * @apiDescription Вложен в UserDTO
 *
 * @apiParam (Значения) {integer} id (readonly)
 * @apiParam (Значения) {integer} id_user (readonly) id пользователя
 * @apiParam (Значения) {string} nickname
 * @apiParam (Значения) {string} name имя пользователя
 * @apiParam (Значения) {string} secondname фамилия
 * @apiParam (Значения) {string} thirdname отчество
 * @apiParam (Значения) {string} birthdate дата рождения yyyy-mm-dd
 * @apiParam (Значения) {boolean} sex пол, true - мужской, false - женский
 * @apiParam (Значения) {string} about о себе
 * @apiParam (Значения) {integer} phone телефон
 * @apiParam (Значения) {string} email email
 * @apiParam (Значения) {ImageDTO} image фото профиля
 * @apiParam (Значения) {array} image_list все (ImageDTO) аватрки пользователя
 * @apiParam (Значения) {integer} rating рейтинг пользователя
 * @apiParam (Значения) {double} lat
 * @apiParam (Значения) {double} lon
 * @apiParam (Значения) {integer} appearance=NULL id ProfileAppearanceDTO
 * @apiParam (Значения) {integer} relation=NULL id ProfileRelationDTO
 * @apiParam (Значения) {string} address адрес
 * @apiParam (Значения) {array} language языки пользователя
 * @apiParam (Значения) {array} info (readonly) 
 * <ul>
 *		<li>events: (integer) кол-во событий в которых юзер учавствует, </li>
 *		<li>groups: (integer) кол-во груп в которых юзер учавствует, </li>
 *		<li>friends: (integer) кол-во друзей, </li>
 *		<li>event_type_fav: (integer) любимый тип событий (null - если событий еще нет),</li>
 *		<li>event_types: (object) кол-во созданных событий по их кол-ву (например {1:10,2:3,3:1} - 10 событий спорт, 3 мероприятия, 1 блиц, 0 активностей)</li>
 *		<li>limit_send: (integer) лимит рассылки, </li>
 * </ul>
 * @apiParam (Значения) {integer} category_fav любимая категория
 * @apiParam (Значения) {string} like люблю
 */

class ProfileDTO extends Profile {
	protected function toJsonFilter($data)
	{
		$data = parent::toJsonFilter($data);

		if (array_key_exists('location', $data)) {
			unset($data['location']);
		}

		if (array_key_exists('appearance_id', $data)) {
			$data['appearance'] = $data['appearance_id'];
			unset($data['appearance_id']);
		}

		if (array_key_exists('relation_id', $data)) {
			$data['relation'] = $data['relation_id'];
			unset($data['relation_id']);
		}

		if (array_key_exists('image_id', $data)) {
			$data['image'] = $this->image;
			
			unset($data['image_id']);
		} else {
			$data['image'] = null;
		}

		if (array_key_exists('image_list', $data) && is_array($data['image_list'])) {
			$data['image_list'] = ImageDTO::model()->findAllByPk($data['image_list']);
		} else {
			$data['image_list'] = array();
		}

		if (!empty($data['language'])) {
			if (!is_array($data['language'])) {
				$data['language'] = PostgreHelper::convSqlToArray($data['language']);
				$data['language'] = array_map('intval', $data['language']);
			}
		} else {
			$data['language'] = array();
		}

		$userId = $this->user->id;

		$data['id_user'] = $userId;

		$info = array(
			'events' => 0,
			'groups' => 0,
			'friends' => 0,
		);

		if ($userId) {
			// TODO: кешировать эту инфу в отдельную таблицу
			$info['events'] = intval(Yii::app()->db->createCommand('SELECT COUNT(event_id) FROM event_user WHERE user_id=:user')->queryScalar(array('user' => $userId)));
			$info['groups'] = intval(Yii::app()->db->createCommand('SELECT COUNT(group_id) FROM group_user WHERE user_id=:user')->queryScalar(array('user' => $userId)));
			$info['friends'] = intval(Yii::app()->db->createCommand('SELECT COUNT(friend_id) FROM friend WHERE user_id=:user')->queryScalar(array('user' => $userId)));

			
			$statEvents = Yii::app()->db->createCommand('SELECT type, COUNT(id) AS cnt FROM event WHERE admin_user_id=:user GROUP BY type')->queryAll(true, array('user' => $userId));
			$eventTypes = array();
			
			foreach($statEvents as $row) {
				$eventTypes[$row['type']] = intval($row['cnt']);
			}

			$info['event_types'] = $eventTypes;
			asort($eventTypes);
			$favEventTypes = array_keys($eventTypes);

			$info['event_type_fav'] = array_pop($favEventTypes);
			
			$info['limit_send'] = $this->user->getLimitSend();
		}

		$data['info'] = $info;

		return $data;
	}


	protected function fromJsonFilter($data, $newObject = false)
	{
		$data = parent::fromJsonFilter($data);

		if (array_key_exists('location', $data)) {
			unset($data['location']);
		}

		if (array_key_exists('id_user', $data)) {
			unset($data['id_user']);
		}

		if (array_key_exists('appearance', $data)) {
			$data['appearance_id'] = $data['appearance'];
			unset($data['appearance']);
		} else {
			$data['appearance_id'] = null;
		}

		if (!empty($data['appearance_id'])) {
			if (!ProfileAppearance::model()->findByPk($data['appearance_id'])) {
				throw new ApiException(ApiController::ERR_OBJECT_INVALID, 'Unknown appearance');
			}
		} else {
			$data['appearance_id'] = null;
		}

		if (array_key_exists('relation', $data)) {
			$data['relation_id'] = $data['relation'];
			unset($data['relation']);
		} else {
			$data['relation_id'] = null;
		}

		if (!empty($data['relation_id'])) {
			if (!ProfileRelation::model()->findByPk($data['relation_id'])) {
				throw new ApiException(ApiController::ERR_OBJECT_INVALID, 'Unknown relation');
			}
		} else {
			$data['relation_id'] = null;
		}

		if (array_key_exists('info', $data)) {
			unset($data['info']);
		}

		if (!empty($data['lat'])) {
			$data['lat'] = floatval($data['lat']);
		} else {
			$data['lat'] = null;
		}

		if (!empty($data['lon'])) {
			$data['lon'] = floatval($data['lon']);
		} else {
			$data['lon'] = null;
		}

		if (array_key_exists('image_id', $data)) {
			unset($data['image_id']);
		}

		if (array_key_exists('birthdate', $data)) {
			$birthdate = $data['birthdate'];
			
			if (!$birthdate) {
				$birthdate = null;
			}
			
			if ($birthdate) {
				if (preg_match('#^(\d{4})\-(\d{1,2})\-(\d{1,2})$#i', $birthdate, $mth)) {
					if (checkdate($mth[2], $mth[3], $mth[1])) {
						// do nothing
					} else {
						throw new ApiException(ApiController::ERR_OBJECT_INVALID, 'Invalid birthdate date');
					}
				} else {
					throw new ApiException(ApiController::ERR_OBJECT_INVALID, 'Invalid birthdate format');
				}
			}

			$data['birthdate'] = $birthdate;
		}

		if (array_key_exists('image', $data)) {
			$imageMain = $data['image'];
			$imageId = null;

			if (empty($imageMain)) {
				$imageId = null;
			} elseif (is_scalar($imageMain)) {
				$imageId = $imageMain;
			} elseif (is_array($imageMain) && !empty($imageMain['id'])) {
				$imageId = $imageMain['id'];
			}

			if ($imageId) {
				$imageMain = Image::model()->findByPk($imageId);
				
				if (!$imageMain) {
					throw new ApiException(ApiController::ERR_OBJECT_NOT_FOUND, 'Unknown main image');
				}

				if (!$this->user || !Image::checkAccess($imageMain, $this->user->id)) {
					throw new ApiException(ApiController::ERR_OBJECT_ACCESS, 'You can not access to specified main image');
				}

				$data['image_id'] = $imageMain->id;
			} else {
				$data['image_id'] = null;
			}
		}

		if (array_key_exists('image_list', $data)) {
			$imageList = $data['image_list'];
			$imageListId = array();

			if (is_scalar($imageList)) {
				$imageList = explode(',', $imageList);
			}

			if (is_array($imageList)) {
				foreach ($imageList as $image) {
					$imageId = null;

					if (is_scalar($image)) {
						$imageId = $image;
					} elseif (is_array($image) && !empty($image['id'])) {
						$imageId = $image['id'];
					}

					if (!$imageId) {
						continue;
					}

					$image = Image::model()->findByPk($imageId);

					if (!$imageMain) {
						throw new ApiException(ApiController::ERR_OBJECT_NOT_FOUND, 'Unknown main image');
					}

					if (!$this->user || !Image::checkAccess($image, $this->user->id)) {
						throw new ApiException(ApiController::ERR_OBJECT_ACCESS, 'You can not access to specified main image');
					}

					$imageListId[] = $image->id;
				}
			}

			$data['image_list'] = $imageListId;
		}


		if (!empty($data['language'])) {
			if (!is_array($data['language'])) {
				throw new ApiException(ApiController::ERR_OBJECT_INVALID, 'Invalid language list');
			} else {
				$data['language'] = array_map('intval', $data['language']);
				$data['language'] = array_unique($data['language']);
				foreach ($data['language'] as $key => $value) {
					if (empty($value)) {
						unset($data['language'][$key]);
					}
				}
				
				$data['language'] = PostgreHelper::convArrayToSql($data['language']);
			}
		} else {
			$data['language'] = null;
		}

		return $data;
	}
}
