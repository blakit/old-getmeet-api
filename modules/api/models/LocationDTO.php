<?php

/**
 * @api {dto} / LocationDTO
 * @apiGroup DTO
 * @apiName LocationDTO
 * @apiDescription Местоположения
 *
 * @apiParam (Значения) {double} lat
 * @apiParam (Значения) {double} lon
 * @apiParam (Значения) {integer} [radius] радиус в метрах (используется для поиска)
 */

class LocationDTO extends Location
	implements JsonSerializable
{
	use JsonModel;

	
}
