<?php

/**
 * @api {dto} / ImageDTO
 * @apiGroup DTO
 * @apiName ImageDTO
 * 
 * @apiParam (Значения) {integer} id
 * @apiParam (Значения) {string} uid uid картинки на сервере
 * @apiParam (Значения) {string} name оригинальное имя файла
 * @apiParam (Значения) {string} date дата загрузки картинки
 * @apiParam (Значения) {string} ext расширение
 * @apiParam (Значения) {string} url URL картинки
 */
class ImageDTO extends Image
{
	protected function toJsonFilter($data)
	{
		$data = parent::toJsonFilter($data);

		if (!isset($data['url'])) {
			$data['url'] = $this->getUrl();
		}

		return $data;
	}

	protected function validateUploadFile(CUploadedFile $file)
	{
		if (!in_array($this->ext, $this->allowedExtension)) {
			throw new ApiException(ApiController::ERR_UPLOAD_FORMAT, 'Invalid file extension');
		}

		if ($file->size > $this->allowedMaxSize) {
			throw new ApiException(ApiController::ERR_UPLOAD_SIZE, 'Max file size reached (allowed ' . $this->allowedMaxSize . ')');
		}
		
		if (!parent::validateUploadFile($file)) {
			throw new ApiException(ApiController::ERR_UPLOAD_SERVER, 'Server problem, try again later');
		}

		return true;
	}

	protected function saveUploadFile(CUploadedFile $file)
	{
		if (!$file->saveAs( $this->getPath() )) {
			throw new ApiException(ApiController::ERR_UPLOAD_SERVER, 'Server problem, try again later');
		}

		return true;
	}
}
