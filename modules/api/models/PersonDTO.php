<?php

/**
 * @api {dto} / PersonDTO
 * @apiGroup DTO
 * @apiName PersonDTO
 * @apiDescription Сокращенная и обобщенная версия UserDTO и ProfileDTO с публичной информацией о пользователе
 *
 * @apiParam (Значения) {integer} id
 * @apiParam (Значения) {string} email (TODO: подумать безопасно ли публиковать это поле...)
 * @apiParam (Значения) {string} name
 * @apiParam (Значения) {string} secondname
 * @apiParam (Значения) {string} thirdname
 * @apiParam (Значения) {string} birthdate
 * @apiParam (Значения) {boolean} sex
 * @apiParam (Значения) {integer} rating
 * @apiParam (Значения) {string} image ссылка на аватарку
 */

class PersonDTO extends ApiModel {
	public $id			 = null;
	public $email		 = null;
	public $name		 = null;
	public $secondname	 = null;
	public $thirdname	 = null;
	public $birthdate	 = null;
	public $sex			 = null;
	public $rating		 = null;
	public $image		 = null;

	public function rules(){
		return array(
			array(implode(',', $this->attributeNames()), 'safe'),
		);
	}

	public function import($object)
	{
		if (!$object instanceof UserDTO) {
			return false;
		}

		$data = array_merge(
			$this->importDataMake($object, $this->attributeNames()),
			$this->importDataMake($object->profile, $this->attributeNames())
		);

		$data['id'] = $object->id;

		if (is_object($object->profile)) {
			if (is_object($object->profile->image)) {
				$data['image'] = $object->profile->image->getUrl();
			}
		}

		$this->setAttributes($data);

		return true;
	}
}
