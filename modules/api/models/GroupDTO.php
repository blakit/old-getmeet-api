<?php

/**
 * @api {dto} / GroupDTO
 * @apiGroup DTO
 * @apiName GroupDTO
 *
 * @apiParam (Значения) {integer} id
 * @apiParam (Значения) {string} name название группы
 * @apiParam (Значения) {string} description описание группы
 * @apiParam (Значения) {string} created_date дата создания
 * @apiParam (Значения) {PersonDTO} admin (readonly) создатель/админ группы
 * @apiParam (Значения) {integer} max_amout максимальное количество людей которые могут присоеденится к группе
 */
class GroupDTO extends Group
{
	protected function toJsonFilter($data)
	{
		$data = parent::toJsonFilter($data);

		if (isset($data['admin_user_id'])) {
			$objectId = $data['admin_user_id'];
			unset($data['admin_user_id']);

			$data['admin'] = $this->adminUser;
		}

		return $data;
	}

	protected function fromJsonFilter($data, $newObject = false)
	{
		$data = parent::fromJsonFilter($data);

		if (isset($data['admin_user_id'])) {
			unset($data['admin_user_id']);
		}

		if (isset($data['admin'])) {
			unset($data['admin']);
		}

		return $data;
	}
}
