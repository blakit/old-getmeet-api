<?php

/**
 * @api {dto} / ChatDTO
 * @apiGroup DTO
 * @apiName ChatDTO
 * @apiDescription Обратить внимание на то что в модели присутствует либо полный список пользователей-участников "users" либо их колличество "users_count".
 * Но обязательно будет одно из них.
 *
 * @apiParam (Значения) {integer} id
 * @apiParam (Значения) {string} name имя для чата
 * @apiParam (Значения) {integer} type=0 тип чата, 0 - private, 1 - event, 2 - group
 * @apiParam (Значения) {EventDTO} event (readonly) событие (если type=event)
 * @apiParam (Значения) {PersonDTO} admin (readonly) создатель чата
 * @apiParam (Значения) {array} [users] (readonly) участники чата (при возврате списка чатов это поле будет отстутствовать)
 * @apiParam (Значения) {array} [users_count] (readonly) кол-во участников чата (если отсутствует ChatDTO->users)
 */
class ChatDTO extends Chat
{
	private $outputFull = true;

	/** Формировать полную модель
	 *
	 * @param boolean $value
	 * @return ChatDTO
	 */
	public function setOutputFull($value = true)
	{
		$this->outputFull = $value;
		
		return $this;
	}

	protected function toJsonFilter($data)
	{
		$data = parent::toJsonFilter($data);

		if (array_key_exists('event_id', $data)) {
			unset($data['event_id']);

			if ($this->event instanceof EventDTO) {
				$this->event->setOutputFull($this->outputFull);
			}
			
			$data['event'] = $this->event;
		}

		if (array_key_exists('admin_id', $data)) {
			unset($data['admin_id']);
			$data['admin'] = $this->admin;
		}


		if (array_key_exists('cache_user', $data)) {
			unset($data['cache_user']);
		}


		if ($this->type == ChatDTO::TYPE_EVENT) {
			$users = array();

			if ($this->outputFull) {
				if ($this->event_id && $this->event) {
					$data['users'] = $users = $this->event->users;
				} else {
					$data['users'] = array();
				}
			} else {
				if ($this->event_id && $this->event) {
					$data['users_count'] = $users = $this->event->users_count;
				} else {
					$data['users_count'] = 0;
				}
			}
		} else {
			if ($this->outputFull) {
				$data['users'] = $this->users;
			} else {
				$data['users_count'] = $this->users_count;
			}
		}

		return $data;
	}


	protected function fromJsonFilter($data, $newObject = false)
	{
		$data = parent::fromJsonFilter($data);

		if (isset($data['admin_id'])) {
			unset($data['admin_id']);
		}

		if (isset($data['admin'])) {
			unset($data['admin']);
		}

		if (isset($data['event_id'])) {
			unset($data['event_id']);
		}

		if (isset($data['admin_id'])) {
			unset($data['admin_id']);
		}

		if (isset($data['event'])) {
			unset($data['event']);
		}

		if (isset($data['admin'])) {
			unset($data['admin']);
		}

		if (isset($data['type'])) {
			unset($data['type']);
		}

		if (isset($data['cache_user'])) {
			unset($data['cache_user']);
		}

		if (isset($data['users'])) {
			unset($data['users']);
		}

		if (isset($data['users_count'])) {
			unset($data['users_count']);
		}

		return $data;
	}
}
