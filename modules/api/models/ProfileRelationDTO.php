<?php

/**
 * @api {dto} / ProfileRelationDTO
 * @apiGroup DTO
 * @apiName ProfileRelationDTO
 * @apiDescription Состояние отношений пользователя
 *
 * @apiParam (Значения) {integer} id
 * @apiParam (Значения) {string} name для мужского пола
 * @apiParam (Значения) {string} name_female для женского пола
 */

class ProfileRelationDTO extends ProfileRelation
{
	
}