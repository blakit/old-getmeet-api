<?php

/**
 * @api {dto} / ProfileAppearanceDTO
 * @apiGroup DTO
 * @apiName ProfileAppearanceDTO
 * @apiDescription Внешность пользователя
 *
 * @apiParam (Значения) {integer} id
 * @apiParam (Значения) {boolean} sex
 * @apiParam (Значения) {string} name
 */

class ProfileAppearanceDTO extends ProfileAppearance
{
	
}