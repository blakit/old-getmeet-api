<?php
/**
 * @api {dto} / EventDTO
 * @apiGroup DTO
 * @apiName EventDTO
 *
 * @apiParam (Значения) {integer} id
 * @apiParam (Значения) {string} name название мероприятия
 * @apiParam (Значения) {string} description описание мероприятия
 * @apiParam (Значения) {boolean} private false - публичный, true - приватное
 * @apiParam (Значения) {integer} type тип мероприятия
 * @apiParam (Значения) {integer} lat широта
 * @apiParam (Значения) {integer} lon долгота
 * @apiParam (Значения) {string} address адрес мероприятия
 * @apiParam (Значения) {UnixTimestampUTC} created_date (readonly) время создания
 * @apiParam (Значения) {UnixTimestampUTC} start_date время начала события
 * @apiParam (Значения) {integer} duration продолжительность
 * @apiParam (Значения) {integer} amount количество мест
 * @apiParam (Значения) {integer} price стоимость
 * @apiParam (Значения) {integer} price_currency валюта
 * @apiParam (Значения) {integer} rating (deprecated) рейтинг
 * @apiParam (Значения) {object} [admin] (readonly) PersonDTO создатель мероприятия
 * @apiParam (Значения) {integer} category_id категория мероприятия
 * @apiParam (Значения) {object} [category] (readonly) EventCategoryDTO категория мероприятия
 * @apiParam (Значения) {object} [image] (readonly) ImageDTO картинка события
 * @apiParam (Значения) {array} [users] (readonly) PersonDTO список участников
 * @apiParam (Значения) {integer} auditory_male процентное значение мужского пола от 0 до 100 (если 0 - то все женского), null - ограничения нет
 * @apiParam (Значения) {integer} auditory_age_min минимальный возраст
 * @apiParam (Значения) {integer} auditory_age_max максимальный возраст
 * @apiParam (Значения) {string} hashtags хеш теги (через запятую)
 * @apiParam (Значения) {boolean} is_cancel true - событие отменено
 * @apiParam (Значения) {integer} rating_like (readonly) кол-во лайков события
 * @apiParam (Значения) {integer} rating_spam (readonly) кол-во жалоб на спам
 * @apiParam (Значения) {string} website Веб сайт
 * @apiParam (Значения) {string} phone Телефон
 */
class EventDTO extends Event {

	private $outputFull = true;

	/** Формировать полную модель
	 *
	 * @param boolean $value
	 * @return EventDTO
	 */
	public function setOutputFull($value = true)
	{
		$this->outputFull = $value;

		return $this;
	}
	
	protected function toJsonFilter($data)
	{
		$data = parent::toJsonFilter($data);

		if (array_key_exists('admin_user_id', $data)) {
			unset($data['admin_user_id']);
		}

		if (array_key_exists('image_id', $data)) {
			unset($data['image_id']);
		}

		if (array_key_exists('location', $data)) {
			unset($data['location']);
		}

		if (array_key_exists('cache_user', $data)) {
			unset($data['cache_user']);
		}

		if ($this->adminUser) {
			$data['admin'] = $this->adminUser->toPersonDTO();
		} else {
			$data['admin'] = null;
		}

		if ($this->category) {

			if ($this->category instanceof EventCategoryDTO) {
				$this->category->setOutputFull($this->outputFull);
			}
			
			$data['category'] = $this->category;
		} else {
			$data['category'] = null;
		}

		if ($this->image) {
			$data['image'] = $this->image;
		} else {
			$data['image'] = null;
		}

		if (!empty($data['created_date'])) {
			$data['created_date'] = DateHelper::ConvSql2Unix($data['created_date']);
		} else {
			$data['created_date'] = null;
		}

		if (!empty($data['start_date'])) {
			$data['start_date'] = DateHelper::ConvSql2Unix($data['start_date']);
		} else {
			$data['start_date'] = null;
		}

		if ($this->outputFull) {
			$data['users'] = $this->users;
		} else {
			$data['users_count'] = $this->users_count;
		}

		if (isset($data['hashtags'])) {
			$hashtags = str_replace('#', '', $data['hashtags']);
			$hashtags = explode(',', $hashtags);

			foreach($hashtags as $key => $tag) {
				$tag = trim($tag);
				if (empty($tag)) {
					unset($hashtags[$key]);
				}

				$tag .= '#' . $tag;

				$hashtags[$key] = $tag;
			}

			$data['hashtags'] = implode(',', $data['hashtags']);
		}

		return $data;
	}

	protected function fromJsonFilter($data, $newObject = false)
	{
		$data = parent::fromJsonFilter($data, $newObject);

		// User
		if (isset($data['admin_user_id'])) {
			unset($data['admin_user_id']);
		}

		if (isset($data['admin_user'])) {
			unset($data['admin_user']);
		}

		if (isset($data['adminUser'])) {
			unset($data['adminUser']);
		}

		if (isset($data['admin'])) {
			unset($data['admin']);
		}

		if (isset($data['rating_like'])) {
			unset($data['rating_like']);
		}

		if (isset($data['rating_spam'])) {
			unset($data['rating_spam']);
		}

		if (isset($data['cache_user'])) {
			unset($data['cache_user']);
		}

		if (isset($data['hashtags'])) {
			$hashtags = str_replace('#', '', $data['hashtags']);
			$hashtags = explode(',', $hashtags);

			foreach($hashtags as $key => $tag) {
				$tag = trim($tag);
				if (empty($tag)) {
					unset($hashtags[$key]);
				}

				$hashtags[$key] = $tag;
			}

			$data['hashtags'] = implode(',', $data['hashtags']);
		}

		// Image
		if (isset($data['image_id'])) {
			unset($data['image_id']);
		}

		if (isset($data['image'])) {
			unset($data['image']);
		}

		// Male quantity
		if (array_key_exists('auditory_male', $data) && is_numeric($data['auditory_male'])) {
			$data['auditory_male'] = intval($data['auditory_male']);
			
			if ($data['auditory_male'] > 100) {
				$data['auditory_male'] = 100;
			} elseif ($data['auditory_male'] < 0) {
				$data['auditory_male'] = 0;
			}
		} else {
			$data['auditory_male'] = null;
		}

		// Dates
		if (array_key_exists('created_date', $data)) {
			unset($data['created_date']);
		}

		if (!empty($data['start_date'])) {
			$data['start_date'] = DateHelper::ConvUnix2Sql($data['start_date']);
		} else {
			if ($newObject) {
				throw new ApiException(ApiController::ERR_OBJECT_INVALID, 'missed start_date');
			}
		}

		// Category
		$categoryId = 1;
		$category = null;
		
		if (isset($data['category'])) {
			if (isset($data['category']['id'])) {
				$categoryId = $data['category']['id'];
			}

			unset($data['category']);
		}

		if (isset($data['category_id'])) {
			$categoryId = $data['category_id'];
			unset($data['category_id']);
		}

		if (!$categoryId && !$newObject) {
			$categoryId = $this->category_id;
			$category = $this->category;
		}

		if ($newObject) {
			if (!$categoryId) {
				throw new ApiException(ApiController::ERR_OBJECT_INVALID, 'category of event missed');
			}

			$category = EventCategoryDTO::model()->findByPk($categoryId);
			if (!$category) {
				throw new ApiException(ApiController::ERR_OBJECT_INVALID, 'category of event unknown');
			}
		} else {
			if ($categoryId != $this->category_id) {
				$category = EventCategoryDTO::model()->findByPk($categoryId);

				if (!$category) {
					throw new ApiException(ApiController::ERR_OBJECT_INVALID, 'category of event unknown');
				}
			}
		}

		$this->category = $category;
		$this->category_id = $categoryId;


		// Тип события
		if (empty($data['type'])) {
			$data['type'] = substr($categoryId, 0, 1);
		}

		return $data;
	}
}
