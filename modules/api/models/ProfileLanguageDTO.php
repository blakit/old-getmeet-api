<?php

/**
 * @api {dto} / ProfileLanguageDTO
 * @apiGroup DTO
 * @apiName ProfileLanguageDTO
 * @apiDescription Языки пользователя
 *
 * @apiParam (Значения) {integer} id
 * @apiParam (Значения) {string} name
 */

class ProfileLanguageDTO extends ProfileLanguage
{
	
}