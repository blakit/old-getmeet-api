<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;

?>

<div class="tester">
	<h3>Test api</h3>
	<ul>
		<li><a href="<?= $this->createUrl('/api/tester/userLogin'); ?>">userGet</a></li>
		<li><a href="<?= $this->createUrl('/api/tester/userGet'); ?>">userGet</a></li>
		<li><a href="<?= $this->createUrl('/api/tester/userUpdate'); ?>">userUpdate</a></li>
		<li><a href="<?= $this->createUrl('/api/tester/userRegister'); ?>">userRegister</a></li>
	</ul>
</div>
