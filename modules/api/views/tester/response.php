<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;

$baseUrl = Yii::app()->baseUrl;
$cs		 = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl . '/js/renderjson.js');
$cs->registerCssFile($baseUrl . '/css/tester.css');
?>

<div class="tester">
<?php if (false) { ?>
	<h3>Raw Response</h3>
	<pre>
		<?= htmlspecialchars($response) ?>
	</pre>
<?php } ?>

<?php if (JsonHelper::decode($response)) { ?>
	<hr />
	<h3>Preview</h3>

	<div id="jsonOutput">

	</div>


	<script>
		try {
			var myJSONObject = JSON.parse('<?= addslashes($response) ?>');
		} catch (e) {
			var myJSONObject = []
		}

		renderjson.set_show_to_level(2);

		document.getElementById("jsonOutput").appendChild(
			renderjson(myJSONObject)
		);
	</script>
<?php } else { ?>
	<hr />
	<h3>Preview</h3>

	<div class="response_html">
		<?= $response ?>
	</div>
<?php } ?>
</div>
