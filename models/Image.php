<?php

/**
 * This is the model class for table "image".
 */
class Image extends AutoImage
{
	protected $allowedExtension = array('jpeg', 'jpg', 'png', 'gif');
	protected $allowedMaxSize = 4194304; //4MB

	protected static $pathUploadFile = array('upload', 'images');
	public static function pathUpload()
	{
		return Yii::getPathOfAlias('webroot') . '/' . implode('/', self::$pathUploadFile) . '/';
	}
	public static function pathUploadUrl()
	{
		return Yii::app()->getBaseUrl(true) . '/' . implode('/', self::$pathUploadFile) . '/';
	}
	protected static function pathRandName()
	{
		return md5(mt_rand(0, 999999999) . microtime());
	}


	public function getPath()
	{
		return self::pathUpload() . implode('/', $this->getFileNameSplited());
	}

	public function getUrl()
	{
		return self::pathUploadUrl() . implode('/', $this->getFileNameSplited());
	}

	
	protected function getFileNameSplited()
	{
		return [
			substr($this->uid, 0, 2),
			substr($this->uid, 2, 4),
			$this->uid . '.' . $this->ext,
		];
	}

	protected function getFileUid()
	{
		return 
			str_pad($this->user_id, 16, '0', STR_PAD_RIGHT) .
			substr(md5(mt_rand(0, 999999999) . microtime()), 0, 16);
	}

	
	protected function validateUploadFile(CUploadedFile $file)
	{
		if (!in_array($this->ext, $this->allowedExtension)) {
			return false;
		}

		if ($file->size > $this->allowedMaxSize) {
			return false;
		}

		return true;
	}

	protected function saveUploadFile(CUploadedFile $file)
	{
		if (!$file->saveAs( $this->getPath() )) {
			return false;
		}

		return true;
	}

	
	public function uploadFile(CUploadedFile $file)
	{
		if ($file->error) {
			return false;
		}

		$this->ext = strtolower($file->extensionName);
		$this->name = $file->name;
		$i = 0;

		while ($i++ < 100) {
			$this->uid = $this->getFileUid();
			
			if (!file_exists($this->getPath())) {
				//Если файла с таким именем еще нет
				break;
			}
		}

		if (!$this->name) {
			$this->name = $this->uid;
		}

		if (!$this->validateUploadFile($file)) {
			return false;
		}
		
		if (!file_exists( dirname($this->getPath()) )) {
			mkdir( dirname($this->getPath()) , 0777, true);
		}

		if (!$this->saveUploadFile($file)) {
			return false;
		}

		$this->date = DateHelper::TimeSql();

		return true;
	}

	public function delete()
	{
		if (file_exists($this->getPath())) {
			@unlink($this->getPath());
		}
		
		return parent::delete();
	}

	/**
	 *
	 * @param Image $image instance of Image or Id image
	 * @param User $user instance of User or Id user
	 * @return boolean
	 */
	public static function checkAccess($image, $user)
	{
		if ($image instanceof Image) {
			
		} elseif (is_numeric($image)) {
			$image = Image::model()->findByPk($image);
		} else {
			return false;
		}

		if (!$image) {
			return false;
		}

		if ($user instanceof User) {
			$userId = $user->id;
		} elseif (is_numeric($user)) {
			$userId = $user;
		} else {
			return false;
		}

		if (!$userId) {
			return false;
		}


		if ($image->user_id != $userId) {
			return false;
		}

		return true;
	}

	/**
	 *
	 * @param string $name
	 * @param User $user
	 * @param boolean $modelSave
	 * @return boolean|Image
	 */
	public static function upload($name, User $user = null, $modelSave = true)
	{
		$uploadedFile = CUploadedFile::getInstanceByName($name);
		
		if(!empty($uploadedFile)){
			/* @var $model Image */
			$className = get_called_class();
			$model = new $className();
			if ($user) {
				$model->user_id = $user->id;
			}
			
			if ($model->uploadFile($uploadedFile, $model->user_id)) {
				
				if ($modelSave) {
					if (!$model->save()) {
						return false;
					}
				}
				
				return $model;
			}

			return false;
		}

		return false;
	}
}
