<?php

/**
 * This is the model class for table "album".
 * 
 * @property AlbumImage[] $albumImages deprecated, use Album->images relation.
 * @property Image[] $images
 */
class Album extends AutoAlbum
{
	public function relations()
	{
		$result = parent::relations();

		return CMap::mergeArray(
			$result,
			array(
				'images' => array(self::MANY_MANY, 'Image', 'album_image(album_id, image_id)'),
			)
		);
	}

	public function behaviors()
	{
		return array('ESaveRelatedBehavior' => array(
				'class' => 'application.components.ESaveRelatedBehavior')
		);
	}
}
