<?php

/**
 * This is the model class for table "chat".
 *
 * The followings are the available columns in table 'chat':
 * @property integer $id
 * @property string $name имя для чата
 * @property integer $event_id
 * @property integer $admin_id создатель чата
 * @property integer $type тип чата, 0 private, 1 - event, 2 - group
 * @property string $cache_user Кеш списка юзеров (readonly), обновляется через тригеры
 * @property boolean $is_deleted
 *
 * The followings are the available model relations:
 * @property ChatUser[] $chatUsers
 * @property Event $event
 * @property User $admin
 * @property ChatImage[] $chatImages
 * @property User[] $users
 */
abstract class AutoChat extends AplicationActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'chat';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('event_id, admin_id, type', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>250),
			array('cache_user, is_deleted', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, event_id, admin_id, type, cache_user, is_deleted', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'chatUsers' => array(self::HAS_MANY, 'ChatUser', 'chat_id'),
			'event' => array(self::BELONGS_TO, 'Event', 'event_id'),
			'admin' => array(self::BELONGS_TO, 'User', 'admin_id'),
			'chatImages' => array(self::HAS_MANY, 'ChatImage', 'chat_id'),
			'users' => array(self::MANY_MANY, 'User', 'chat_exit(chat_id, user_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'имя для чата',
			'event_id' => 'Event',
			'admin_id' => 'создатель чата',
			'type' => 'тип чата, 0 private, 1 - event, 2 - group',
			'cache_user' => 'Кеш списка юзеров (readonly), обновляется через тригеры',
			'is_deleted' => 'Is Deleted',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('event_id',$this->event_id);
		$criteria->compare('admin_id',$this->admin_id);
		$criteria->compare('type',$this->type);
		$criteria->compare('cache_user',$this->cache_user,true);
		$criteria->compare('is_deleted',$this->is_deleted);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=null)
	{
		if (is_null($className)) {
			$className = get_called_class();
		}

		return parent::model($className);
	}
}
