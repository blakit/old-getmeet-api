<?php

/**
 * This is the model class for table "group".
 *
 * The followings are the available columns in table 'group':
 * @property integer $id
 * @property string $name название группы
 * @property string $description описание группы
 * @property string $created_date дата создания
 * @property integer $admin_user_id создатель/админ группы
 * @property integer $max_amout максимальное количество людей которые могут присоеденится к группе
 *
 * The followings are the available model relations:
 * @property User $adminUser
 * @property GroupUser[] $groupUsers
 */
abstract class AutoGroup extends AplicationActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'group';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('admin_user_id, max_amout', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>80),
			array('description', 'length', 'max'=>1024),
			array('created_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, description, created_date, admin_user_id, max_amout', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'adminUser' => array(self::BELONGS_TO, 'User', 'admin_user_id'),
			'groupUsers' => array(self::HAS_MANY, 'GroupUser', 'group_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'название группы',
			'description' => 'описание группы',
			'created_date' => 'дата создания',
			'admin_user_id' => 'создатель/админ группы',
			'max_amout' => 'максимальное количество людей которые могут присоеденится к группе',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('admin_user_id',$this->admin_user_id);
		$criteria->compare('max_amout',$this->max_amout);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=null)
	{
		if (is_null($className)) {
			$className = get_called_class();
		}

		return parent::model($className);
	}
}
