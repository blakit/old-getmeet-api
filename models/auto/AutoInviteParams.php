<?php

/**
 * This is the model class for table "invite_params".
 *
 * The followings are the available columns in table 'invite_params':
 * @property integer $id
 * @property integer $male_quantity процентное значение мужского пола от 0 до 100 (если 0 - то все женского)
 * @property integer $min_age минимальный возраст
 * @property integer $max_age максимальный возраст
 * @property double $lat
 * @property double $lon
 * @property integer $response_type тип ответа
 */
abstract class AutoInviteParams extends AplicationActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'invite_params';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('male_quantity, min_age, max_age, response_type', 'numerical', 'integerOnly'=>true),
			array('lat, lon', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, male_quantity, min_age, max_age, lat, lon, response_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'male_quantity' => 'процентное значение мужского пола от 0 до 100 (если 0 - то все женского)',
			'min_age' => 'минимальный возраст',
			'max_age' => 'максимальный возраст',
			'lat' => 'Lat',
			'lon' => 'Lon',
			'response_type' => 'тип ответа',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('male_quantity',$this->male_quantity);
		$criteria->compare('min_age',$this->min_age);
		$criteria->compare('max_age',$this->max_age);
		$criteria->compare('lat',$this->lat);
		$criteria->compare('lon',$this->lon);
		$criteria->compare('response_type',$this->response_type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=null)
	{
		if (is_null($className)) {
			$className = get_called_class();
		}

		return parent::model($className);
	}
}
