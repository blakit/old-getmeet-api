<?php

/**
 * This is the model class for table "event".
 *
 * The followings are the available columns in table 'event':
 * @property integer $id
 * @property string $name название мероприятия
 * @property string $description описание мероприятия
 * @property double $lat
 * @property double $lon
 * @property boolean $private false - публичный, true - приватное
 * @property integer $type тип мероприятия
 * @property string $created_date время создания 
 * @property string $start_date время начала события
 * @property integer $duration продолжительность события в секундах
 * @property integer $amount количество мест
 * @property integer $price стоимость
 * @property string $price_currency валюта
 * @property integer $rating рейтинг
 * @property integer $admin_user_id создатель мероприятия
 * @property integer $category_id категория мероприятия
 * @property string $location локация
 * @property integer $image_id Картинка события
 * @property string $address адрес мероприятия
 * @property integer $auditory_male процентное значение мужского пола от 0 до 100 (если 0 - то все женского), null - ограничения нет
 * @property integer $auditory_age_min минимальный возраст
 * @property integer $auditory_age_max максимальный возраст
 * @property string $hashtags хештеги
 * @property boolean $is_cancel событие отменено
 * @property integer $rating_like кол-во лайков события
 * @property integer $rating_spam кол-во жалоб на спам
 * @property string $cache_user
 * @property string $website веб-сайт
 * @property string $phone телефон
 *
 * The followings are the available model relations:
 * @property User $adminUser
 * @property Image $image
 * @property EventCategory $category
 * @property EventUser[] $eventUsers
 * @property User[] $users
 * @property Chat[] $chats
 * @property EventInvite[] $eventInvites
 * @property EventImage[] $eventImages
 * @property Userevent[] $userevents
 */
abstract class AutoEvent extends AplicationActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'event';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, start_date, admin_user_id, category_id', 'required'),
			array('type, duration, amount, price, rating, admin_user_id, category_id, image_id, auditory_male, auditory_age_min, auditory_age_max, rating_like, rating_spam', 'numerical', 'integerOnly'=>true),
			array('lat, lon', 'numerical'),
			array('name', 'length', 'max'=>100),
			array('price_currency', 'length', 'max'=>3),
			array('address, website', 'length', 'max'=>255),
			array('hashtags', 'length', 'max'=>250),
			array('phone', 'length', 'max'=>128),
			array('description, private, created_date, location, is_cancel, cache_user', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, description, lat, lon, private, type, created_date, start_date, duration, amount, price, price_currency, rating, admin_user_id, category_id, location, image_id, address, auditory_male, auditory_age_min, auditory_age_max, hashtags, is_cancel, rating_like, rating_spam, cache_user, website, phone', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'adminUser' => array(self::BELONGS_TO, 'User', 'admin_user_id'),
			'image' => array(self::BELONGS_TO, 'Image', 'image_id'),
			'category' => array(self::BELONGS_TO, 'EventCategory', 'category_id'),
			'eventUsers' => array(self::HAS_MANY, 'EventUser', 'event_id'),
			'users' => array(self::MANY_MANY, 'User', 'event_like(event_id, user_id)'),
			'chats' => array(self::HAS_MANY, 'Chat', 'event_id'),
			'eventInvites' => array(self::HAS_MANY, 'EventInvite', 'event_id'),
			'eventImages' => array(self::HAS_MANY, 'EventImage', 'event_id'),
			'userevents' => array(self::HAS_MANY, 'Userevent', 'event_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'название мероприятия',
			'description' => 'описание мероприятия',
			'lat' => 'Lat',
			'lon' => 'Lon',
			'private' => 'false - публичный, true - приватное',
			'type' => 'тип мероприятия',
			'created_date' => 'время создания ',
			'start_date' => 'время начала события',
			'duration' => 'продолжительность события в секундах',
			'amount' => 'количество мест',
			'price' => 'стоимость',
			'price_currency' => 'валюта',
			'rating' => 'рейтинг',
			'admin_user_id' => 'создатель мероприятия',
			'category_id' => 'категория мероприятия',
			'location' => 'локация',
			'image_id' => 'Картинка события',
			'address' => 'адрес мероприятия',
			'auditory_male' => 'процентное значение мужского пола от 0 до 100 (если 0 - то все женского), null - ограничения нет',
			'auditory_age_min' => 'минимальный возраст',
			'auditory_age_max' => 'максимальный возраст',
			'hashtags' => 'хештеги',
			'is_cancel' => 'событие отменено',
			'rating_like' => 'кол-во лайков события',
			'rating_spam' => 'кол-во жалоб на спам',
			'cache_user' => 'Cache User',
			'website' => 'веб-сайт',
			'phone' => 'телефон',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('lat',$this->lat);
		$criteria->compare('lon',$this->lon);
		$criteria->compare('private',$this->private);
		$criteria->compare('type',$this->type);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('start_date',$this->start_date,true);
		$criteria->compare('duration',$this->duration);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('price',$this->price);
		$criteria->compare('price_currency',$this->price_currency,true);
		$criteria->compare('rating',$this->rating);
		$criteria->compare('admin_user_id',$this->admin_user_id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('image_id',$this->image_id);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('auditory_male',$this->auditory_male);
		$criteria->compare('auditory_age_min',$this->auditory_age_min);
		$criteria->compare('auditory_age_max',$this->auditory_age_max);
		$criteria->compare('hashtags',$this->hashtags,true);
		$criteria->compare('is_cancel',$this->is_cancel);
		$criteria->compare('rating_like',$this->rating_like);
		$criteria->compare('rating_spam',$this->rating_spam);
		$criteria->compare('cache_user',$this->cache_user,true);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('phone',$this->phone,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=null)
	{
		if (is_null($className)) {
			$className = get_called_class();
		}

		return parent::model($className);
	}
}
