<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id уникальный идентификатор пользователя
 * @property string $push_id токен для пуш уведомлений
 * @property string $token токен доступа
 * @property boolean $logged true если пользователь авторизирован, и false если нет
 * @property integer $profile_id ссылка на профиль с пользовательскими данными
 * @property string $last_date время последней активности
 * @property boolean $status true - пользователь онлайн, false - офлайн
 * @property string $email Email пользователя
 * @property string $password Хеш пароля пользователя
 * @property string $vk_id
 * @property string $fb_id
 * @property string $tw_id ID Twitter
 * @property string $mm_id ID Мой Мир
 * @property boolean $is_deleted
 * @property string $date_registred дата регистрации пользователя в системе
 *
 * The followings are the available model relations:
 * @property Event[] $events
 * @property ChatUser[] $chatUsers
 * @property Image[] $images
 * @property EventUser[] $eventUsers
 * @property Event[] $events1
 * @property Chat[] $chats
 * @property EventInvite[] $eventInvites
 * @property GroupUser[] $groupUsers
 * @property Friend[] $friends
 * @property Friend[] $friends1
 * @property Group[] $groups
 * @property Album[] $albums
 * @property Chat[] $chats1
 * @property UserToken[] $userTokens
 * @property Profile $profile
 */
abstract class AutoUser extends AplicationActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('profile_id', 'numerical', 'integerOnly'=>true),
			array('push_id', 'length', 'max'=>1024),
			array('token', 'length', 'max'=>200),
			array('email', 'length', 'max'=>80),
			array('password', 'length', 'max'=>64),
			array('vk_id, fb_id, tw_id, mm_id', 'length', 'max'=>256),
			array('logged, last_date, status, is_deleted, date_registred', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, push_id, token, logged, profile_id, last_date, status, email, password, vk_id, fb_id, tw_id, mm_id, is_deleted, date_registred', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'events' => array(self::HAS_MANY, 'Event', 'admin_user_id'),
			'chatUsers' => array(self::HAS_MANY, 'ChatUser', 'user_id'),
			'images' => array(self::HAS_MANY, 'Image', 'user_id'),
			'eventUsers' => array(self::HAS_MANY, 'EventUser', 'user_id'),
			'events1' => array(self::MANY_MANY, 'Event', 'event_like(user_id, event_id)'),
			'chats' => array(self::HAS_MANY, 'Chat', 'admin_id'),
			'eventInvites' => array(self::HAS_MANY, 'EventInvite', 'user_id'),
			'groupUsers' => array(self::HAS_MANY, 'GroupUser', 'user_id'),
			'friends' => array(self::HAS_MANY, 'Friend', 'user_id'),
			'friends1' => array(self::HAS_MANY, 'Friend', 'friend_id'),
			'groups' => array(self::HAS_MANY, 'Group', 'admin_user_id'),
			'albums' => array(self::HAS_MANY, 'Album', 'user_id'),
			'chats1' => array(self::MANY_MANY, 'Chat', 'chat_exit(user_id, chat_id)'),
			'userTokens' => array(self::HAS_MANY, 'UserToken', 'user_id'),
			'profile' => array(self::BELONGS_TO, 'Profile', 'profile_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'уникальный идентификатор пользователя',
			'push_id' => 'токен для пуш уведомлений',
			'token' => 'токен доступа',
			'logged' => 'true если пользователь авторизирован, и false если нет',
			'profile_id' => 'ссылка на профиль с пользовательскими данными',
			'last_date' => 'время последней активности',
			'status' => 'true - пользователь онлайн, false - офлайн',
			'email' => 'Email пользователя',
			'password' => 'Хеш пароля пользователя',
			'vk_id' => 'Vk',
			'fb_id' => 'Fb',
			'tw_id' => 'ID Twitter',
			'mm_id' => 'ID Мой Мир',
			'is_deleted' => 'Is Deleted',
			'date_registred' => 'дата регистрации пользователя в системе',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('push_id',$this->push_id,true);
		$criteria->compare('token',$this->token,true);
		$criteria->compare('logged',$this->logged);
		$criteria->compare('profile_id',$this->profile_id);
		$criteria->compare('last_date',$this->last_date,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('vk_id',$this->vk_id,true);
		$criteria->compare('fb_id',$this->fb_id,true);
		$criteria->compare('tw_id',$this->tw_id,true);
		$criteria->compare('mm_id',$this->mm_id,true);
		$criteria->compare('is_deleted',$this->is_deleted);
		$criteria->compare('date_registred',$this->date_registred,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=null)
	{
		if (is_null($className)) {
			$className = get_called_class();
		}

		return parent::model($className);
	}
}
