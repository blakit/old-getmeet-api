<?php

/**
 * This is the model class for table "profile".
 *
 * The followings are the available columns in table 'profile':
 * @property integer $id
 * @property boolean $logged
 * @property string $nickname
 * @property string $name имя пользователя
 * @property string $secondname фамилия
 * @property string $thirdname отчество
 * @property string $birthdate дата рождения
 * @property boolean $sex пол, true - мужской, false - женский
 * @property string $about о себе
 * @property string $phone телефон
 * @property string $email email
 * @property integer $rating рейтинг пользователя
 * @property double $lat
 * @property double $lon
 * @property string $location местоположение
 * @property integer $appearance_id
 * @property integer $relation_id
 * @property integer $image_id аватарка пользователя
 * @property string $address адрес
 * @property string $language языки пользователя
 * @property integer $category_fav любимая категория
 * @property string $like люблю
 * @property string $image_list все аватрки пользователя
 *
 * The followings are the available model relations:
 * @property ProfileAppearance $appearance
 * @property ProfileRelation $relation
 * @property Image $image
 * @property User[] $users
 * @property Userevent[] $userevents
 */
abstract class AutoProfile extends AplicationActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'profile';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('rating, appearance_id, relation_id, image_id, category_fav', 'numerical', 'integerOnly'=>true),
			array('lat, lon', 'numerical'),
			array('nickname, name, secondname, thirdname', 'length', 'max'=>80),
			array('about', 'length', 'max'=>1024),
			array('email', 'length', 'max'=>100),
			array('address', 'length', 'max'=>256),
			array('like', 'length', 'max'=>128),
			array('logged, birthdate, sex, phone, location, language, image_list', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, logged, nickname, name, secondname, thirdname, birthdate, sex, about, phone, email, rating, lat, lon, location, appearance_id, relation_id, image_id, address, language, category_fav, like, image_list', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'appearance' => array(self::BELONGS_TO, 'ProfileAppearance', 'appearance_id'),
			'relation' => array(self::BELONGS_TO, 'ProfileRelation', 'relation_id'),
			'image' => array(self::BELONGS_TO, 'Image', 'image_id'),
			'users' => array(self::HAS_MANY, 'User', 'profile_id'),
			'userevents' => array(self::HAS_MANY, 'Userevent', 'profile_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'logged' => 'Logged',
			'nickname' => 'Nickname',
			'name' => 'имя пользователя',
			'secondname' => 'фамилия',
			'thirdname' => 'отчество',
			'birthdate' => 'дата рождения',
			'sex' => 'пол, true - мужской, false - женский',
			'about' => 'о себе',
			'phone' => 'телефон',
			'email' => 'email',
			'rating' => 'рейтинг пользователя',
			'lat' => 'Lat',
			'lon' => 'Lon',
			'location' => 'местоположение',
			'appearance_id' => 'Appearance',
			'relation_id' => 'Relation',
			'image_id' => 'аватарка пользователя',
			'address' => 'адрес',
			'language' => 'языки пользователя',
			'category_fav' => 'любимая категория',
			'like' => 'люблю',
			'image_list' => 'все аватрки пользователя',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('logged',$this->logged);
		$criteria->compare('nickname',$this->nickname,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('secondname',$this->secondname,true);
		$criteria->compare('thirdname',$this->thirdname,true);
		$criteria->compare('birthdate',$this->birthdate,true);
		$criteria->compare('sex',$this->sex);
		$criteria->compare('about',$this->about,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('rating',$this->rating);
		$criteria->compare('lat',$this->lat);
		$criteria->compare('lon',$this->lon);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('appearance_id',$this->appearance_id);
		$criteria->compare('relation_id',$this->relation_id);
		$criteria->compare('image_id',$this->image_id);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('language',$this->language,true);
		$criteria->compare('category_fav',$this->category_fav);
		$criteria->compare('like',$this->like,true);
		$criteria->compare('image_list',$this->image_list,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=null)
	{
		if (is_null($className)) {
			$className = get_called_class();
		}

		return parent::model($className);
	}
}
