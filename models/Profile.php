<?php

/**
 * This is the model class for table "profile".
 *
 * @property array $image_list все аватрки пользователя
 *
 * @property User $user
 *
 * @property Userevent[] $userevents deprecated
 * @property User[] $users deprecated
 */
class Profile extends AutoProfile
{
	/**
	 * Your brilliant code here
	 */

	public function relations()
	{
		$result = parent::relations();

		$unsetRelation = ['userevents', 'users', 'image'];

		foreach ($unsetRelation as $val) {
			if (array_key_exists($val, $result)){
				unset($result[$val]);
			}
		}

		return CMap::mergeArray(
			$result,
			array(
				'image' => array(self::BELONGS_TO, 'Image', 'image_id', 'alias'=>'profile_image'),
				'user'  => array(self::HAS_ONE, 'User', 'profile_id', 'alias'=>'profile_user'),
			)
		);
	}

	/**
	 *
	 * @param User|integer $user
	 * @return boolean
	 */
	function findByUser($user)
	{
		if (is_numeric($user)) {
			$user = User::model()->findAllByPk($user);
		}

		if (!$user || !$user instanceof User) {
			return false;
		}

		$profile = Profile::model()->findByPk($user->profile_id);

		return $profile;
	}


	public function onAfterConstruct($event)
	{
		parent::onAfterConstruct($event);

		$this->image_list = array();
	}


	protected function afterFind()
	{
		parent::afterFind();

		if ($this->image_list) {
			$this->image_list = PostgreHelper::convSqlToArray($this->image_list);
		}

		if (!$this->image_list) {
			$this->image_list = array();
		}

		$this->image_list = array_map('intval', $this->image_list);
		$this->image_list = array_unique($this->image_list);
	}

	protected function afterSave()
	{
		parent::afterSave();

		if ($this->image_list) {
			$this->image_list = PostgreHelper::convSqlToArray($this->image_list);
		}

		if (!$this->image_list) {
			$this->image_list = array();
		}

		$this->image_list = array_map('intval', $this->image_list);
		$this->image_list = array_unique($this->image_list);
	}

	protected function beforeSave()
	{
		if (!parent::beforeSave()) {
			return false;
		}


		$images = array();
		if (is_array($this->image_list)) {
			$images = $this->image_list;
		} elseif(is_numeric($this->image_list)) {
			$images = array($this->image_list);
		} elseif( ($images = PostgreHelper::convSqlToArray($this->image_list)) ) {
			$images = array($this->image_list);
		} else {
			$images = array();
		}

		$images = array_map('intval', $images);
		$images = array_unique($images);

		if (!is_array($images) || !count($images)) {
			$this->image_list = null;
		} else {
			$this->image_list = PostgreHelper::convArrayToSql($this->image_list);
		}

		return true;
	}

	
}
