<?php

/**
 * This is the model class for table "group".
 *
 * @property GroupUser[] $groupUsers deprecated, use Group->users relation.
 * @property User[] $users
 */
class Group extends AutoGroup
{
	public function relations()
	{
		$result = parent::relations();

		return CMap::mergeArray(
			$result,
			array(
				'users' => array(self::MANY_MANY, 'User', 'group_user(group_id, user_id)'),
			)
		);
	}

	public function behaviors()
	{
		return array('ESaveRelatedBehavior' => array(
				'class' => 'application.components.ESaveRelatedBehavior')
		);
	}
}
