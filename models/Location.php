<?php


class Location extends CFormModel {
	const PREG = '#^POINT\((\d+(?:\.\d+)?) (\d+(?:\.\d+)?)\)$#i';
	const DEFAULT_RADIUS = 200;

	public $lon = null;
	public $lat = null;
	public $radius = self::DEFAULT_RADIUS;

	public function rules()
    {
        return array(
			array('lon', 'numerical', 'min' => -180, 'max' => 180),
			array('lat', 'numerical', 'min' => -90, 'max' => 90),
			array('radius', 'numerical', 'integerOnly' => true),
		);
	}

	public static function isGeoString($string)
	{
		return preg_match(self::PREG, $string);
	}

	public function fromGeoString($geotext)
	{
		if (preg_match(self::PREG, $geotext, $mth)) {
			$lon = (float)$mth[2];
			$lat = (float)$mth[1];

			return $this->fromCoords($lon, $lat);
		}

		return false;
	}

	public function fromCoords($lon, $lat, $radius = self::DEFAULT_RADIUS)
	{
		$this->lon = (float)$lon;
		$this->lat = (float)$lat;
		$this->radius = (float)$radius;

		return true;
	}

	public function toSqlString()
	{
		return (
			'ST_Point(' .
				$this->format($this->lon) . ', ' .
				$this->format($this->lat) .
			')'
		);
	}

	public function toGeoString()
	{
		return (
			'POINT(' .
				$this->format($this->lon) . ' ' .
				$this->format($this->lat) .
			')'
		);
	}

	private function format($number)
	{
		if ($number && is_numeric($number)) {
			return trim(round(number_format($number, 15, '.', ''), 6), '0');
		} else {
			return '0';
		}
	}
}
