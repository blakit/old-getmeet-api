<?php

/**
 * This is the model class for table "chat".
 *
 * @property ChatUser[] $chatUsers deprecated, use Chat->users relation.
 * @property ChatImage[] $chatImages deprecated, use Chat->images relation.
 *
 * @property User[] $users
 * @property Image[] $images
 * @property integer $users_count
 */
class Chat extends AutoChat
{
	const TYPE_PRIVATE = 0;
	const TYPE_EVENT = 1;
	const TYPE_GROUP = 2;

		/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		$result = parent::relations();

		$unsetRelation = ['chatImages', 'chatUsers', 'users', 'users1'];

		foreach ($unsetRelation as $val) {
			if (array_key_exists($val, $result)){
				unset($result[$val]);
			}
		}

		return CMap::mergeArray(
			$result,
			array(
				'users' => array(self::MANY_MANY, 'User', 'chat_user(chat_id, user_id)', 'alias'=>'chat_users'),
				'images' => array(self::MANY_MANY, 'Image', 'chat_image(chat_id, image_id)', 'alias'=>'chat_images'),
				'users_count' => array(self::STAT, 'User', 'chat_user(chat_id, user_id)'),
				'usersExit' => array(self::MANY_MANY, 'User', 'chat_exit(chat_id, user_id)', 'alias'=>'chat_users_exit'),
			)
		);
	}

	public function findByUsers($users, $type = null)
	{
		$criteria = $this->buildCriteria($users, $type);

		$object = self::model()->find($criteria);

		return $object;
	}

	public function findAllByUsers($users, $type = null)
	{
		$criteria = $this->buildCriteria($users, $type);

		$object = self::model()->findAll($criteria);

		return $object;
	}

	protected function buildCriteria($users, $type = null)
	{
		$idList = $users;
		$idList = array_unique($idList);
		sort($idList);
		
		$criteria = new CDbCriteria();
		$criteria->params = array();

		$placement = array_fill( 0, count($idList), '?' );
		$criteria->addCondition('ARRAY[' . implode(',', $placement) . ']::integer[] @> cache_user');
		$criteria->params = array_merge($criteria->params, $idList);

		$criteria->addCondition('array_length(cache_user, 1) = ?');
		$criteria->params = array_merge($criteria->params, array(count($idList)));

		if (!is_null($type)) {
			$criteria->addCondition('type = ?');
			$criteria->params = array_merge($criteria->params, array($type));
		}

		$criteria->addCondition('is_deleted = false');

		return $criteria;
	}

	public static function createPrivate($creator, $user, &$created = false)
	{
		$object = ChatDTO::model()->findByUsers(array($creator->id, $user->id), ChatDTO::TYPE_PRIVATE);

		if (!$object) {
			$object = new ChatDTO();
			$object->admin_id = null;
			$object->type = ChatDTO::TYPE_PRIVATE;
			$object->save();

			$users = array(
				$creator,
				$user,
			);

			foreach ($users as $user) {
				$chatUser = new ChatUser();
				$chatUser->chat_id = $object->id;
				$chatUser->user_id = $user->id;
				$chatUser->save();
			}

			$created = true;
		} else {
			if ( ($item = ChatExit::model()->find(' chat_id = ? AND user_id = ? ', array($object->id, $creator->id))) ) {
				$item->delete();
			}
		}

		return $object;
	}
}
