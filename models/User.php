<?php

/**
 * This is the model class for table "user".
 *
 * @property deprectated $eventUsers deprectated
 * @property deprectated $groupUsers deprectated
 *
 * @property Group[] $groups группы в которых учавствует пользователь
 * @property Group[] $groupsOwner группы созданные пользователем
 *
 * @property Event[] $events события в которых учавствует пользователь
 * @property Event[] $eventsOwner события созданные пользователем
 *
 * @property User[] $friends
 */
class User extends AutoUser
{
	public static $registredSocial = array('vk', 'fb', 'tw', 'mm');

	public function relations()
	{
		$result = parent::relations();

		$unsetRelation = ['events', 'events1', 'chats1', 'groups', 'friends', 'friends1'];

		foreach ($unsetRelation as $val) {
			if (array_key_exists($val, $result)){
				unset($result[$val]);
			}
		}

		$result = array_merge(
			$result,
			array(
				'groupsOwner' => array(self::HAS_MANY, 'Group', 'user_id'),
				'groups' => array(self::MANY_MANY, 'Group', 'group_user(user_id, group_id)'),
				
				'eventsOwner' => array(self::HAS_MANY, 'Event', 'user_id'),
				'events' => array(self::MANY_MANY, 'Event', 'event_user(user_id, event_id)'),
				
				'friends' => array(self::MANY_MANY, 'User', 'friend(user_id, friend_id)'),
			)
		);

		return $result;
	}

	public function findByToken($token)
	{
		return $this->find('token=?', [$token]);
	}

	public static function tokenGenerate()
	{
		return md5(PasswordHelper::randomString(32));
	}

	public function getTokenPush()
	{
		$userToken = UserToken::model()->find(' user_id = :user_id', array('user_id' => $this->id));

		return $userToken;
	}

	public function getLimitSend()
	{
		$profile = $this->profile;

		if ($profile->rating > 100) {
			return 25;
		}

		if ($profile->rating > 50) {
			return 20;
		}

		if ($profile->rating > 20) {
			return 15;
		}

		if ($profile->image_id) {
			return 10;
		}

		return 5;
	}
}
