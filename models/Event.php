<?php

/**
 * This is the model class for table "event".
 *
 * @property Chat[] $chats deprecated, use Event->chat relation.
 * @property EventUser[] $eventUsers deprecated, use Event->users relation.
 * @property EventImage[] $eventImages deprecated, use Event->images relation.
 * @property Userevent[] $userevents deprecated, use Event->users relation.
 *
 * @property Image[] $images
 * @property User[] $users
 * @property Chat $chat
 * @property integer $users_count
 */
class Event extends AutoEvent
{
	const TYPE_SPORT		 = 1; // Спорт
	const TYPE_EVENTS		 = 2; // Мероприятия
	const TYPE_BLITZ		 = 3; // Блиц
	const TYPE_ACTIVITY		 = 4; // Активности

	public function relations()
	{
		$result = parent::relations();

		$unsetRelation = ['eventUsers', 'eventImages', 'userevents', 'chats', 'users', 'images'];

		foreach ($unsetRelation as $val) {
			if (array_key_exists($val, $result)){
				unset($result[$val]);
			}
		}

		return CMap::mergeArray(
			$result,
			array(
				'chat' => array(self::BELONGS_TO, 'Chat', 'event_id', 'alias'=>'event_chat'),
				'users' => array(self::MANY_MANY, 'User', 'event_user(event_id, user_id)', 'alias'=>'event_users'),
				'images' => array(self::MANY_MANY, 'Image', 'event_image(event_id, image_id)', 'alias'=>'event_images'),
				'users_count' => array(self::STAT, 'User', 'event_user(event_id, user_id)'),
			)
		);
	}

	public function behaviors()
	{
		return array('ESaveRelatedBehavior' => array(
				'class' => 'application.components.ESaveRelatedBehavior')
		);
	}
}
