<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h3>Admin</h3>
<ul>
	<li><a href="<?= $this->createUrl('admin/album/'); ?>">album</a></li>
	<li><a href="<?= $this->createUrl('admin/event/'); ?>">event</a></li>
	<li><a href="<?= $this->createUrl('admin/group/'); ?>">group</a></li>
	<li><a href="<?= $this->createUrl('admin/invite/'); ?>">invite</a></li>
	<li><a href="<?= $this->createUrl('admin/inviteparams/'); ?>">inviteparams</a></li>
	<li><a href="<?= $this->createUrl('admin/layouts/'); ?>">layouts</a></li>
	<li><a href="<?= $this->createUrl('admin/user/'); ?>">User</a></li>
	<li><a href="<?= $this->createUrl('admin/profile/'); ?>">Profile</a></li>
</ul>


