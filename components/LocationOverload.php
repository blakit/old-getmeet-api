<?php

trait LocationOverload {

	private $locationModel = 'Location';
	private $locationColumn = 'location';
	private $locationEnabled = null;

	private $locationObject = null;

	/*
	 *	//Implement it to override location model class
	 *	protected function locationModelDefault() {
	 *		return 'Location';
	 *	}
	 *
	 * //Implement it to enable/disable override location by default
	 * 	protected function locationEnabledDefault() {
	 *		return false;
	 *	}
	 *
	 * //Implement it to override column location name
	 * 	protected function locationColumnDefault() {
	 *		return 'location';
	 *	}
	 */

	public function __construct($scenario='insert')
	{
		if (method_exists($this, 'locationModelDefault')) {
			$this->locationModel = $this->locationModelDefault();
		}

		if (method_exists($this, 'locationEnabledDefault')) {
			$this->locationEnabled = $this->locationEnabledDefault();
		}

		if (method_exists($this, 'locationColumnDefault')) {
			$this->locationColumn = $this->locationColumnDefault();
		}
		
		return parent::__construct($scenario);
	}

	/**
	 * @return Location
	 */
	protected function locationObject() {
		if (is_null($this->locationObject)) {
			$model = $this->locationModel;
			
			$this->locationObject = new $model();
		}

		return $this->locationObject;
	}

	protected function afterFind()
	{
		if (is_null($this->locationEnabled) || $this->locationEnabled) {
			if ($this->hasAttribute($this->locationColumn)) {
				$hash = $this->getAttribute($this->locationColumn);

				if ($this->locationObject()->fromGeoString($hash)) {
					$this->locationEnabled(true);

					$this->{$this->locationColumn} = $this->locationObject();
				} else {
					$this->locationEnabled(false);
				}
			}
		}
		
		return parent::afterFind();
	}

	protected function beforeSave()
	{
		if ($this->locationEnabled) {
			$this->location = new CDbExpression(
				$this->locationObject()->toSqlString()
			);
		}
		
		return parent::beforeSave();
	}

	protected function afterSave()
	{
		if ($this->locationEnabled) {
			$this->location = $this->locationObject();
		}

		return parent::afterSave();
	}

	public function locationEnabled($enable = true) {
		$this->locationEnabled = $enable;
		
		return $this;
	}

	public function locationColumn($columnName = 'location') {
		$this->locationColumn = $columnName;
		
		return $this;
	}

	protected function query($criteria, $all=false)
	{
		if ($this->locationEnabled) {
			if (!empty($this->locationColumn) && $this->hasAttribute($this->locationColumn)) {
				$criteria->select = $criteria->select . ", ST_AsText(" . $this->locationColumn . ") AS " . $this->locationColumn . "";
			}
		}

		return parent::query($criteria, $all);
	}
}