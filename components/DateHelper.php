<?php


class DateHelper{
	const FORM_WERB_NOMINATIVE=1;
	const FORM_WERB_GENITIVE=2;
	const FORM_WERB_ACCUSATIVE=3;

	const USER_DATETIME=1;
	const USER_DATE=2;
	const USER_TIME=3;


	/** Type of processing time zones:<br>
	 * default - using functions PHP<br>
	 * class - using this class<br>
	 * @var string */
	protected static $timezone_type='default';

	/** Time shift for the time zone.<br>
	 * Only if $timezone_type set as "class"
	 * @var integer */
	protected static $timezone_shift=null;

	protected static $timezone_inited=false;

	static function NormalizeTimePeriod($unix_period, $form_werb=self::FORM_WERB_NOMINATIVE){
		$ar=array();
		$ar['year']=31536000;
		$ar['month']=2592000;
		$ar['week']=604800;
		$ar['day']=86400;
		$ar['hour']=3600;
		$ar['minute']=60;
		$ar['second']=1;


		$narr=array();
		$buf=$unix_period;
		foreach($ar as $key => $len){
			if($unix_period==0){
				break;
			}

			if($unix_period<$len){
				continue;
			}
			$buf=(int)($unix_period / $len);
			$unix_period=$unix_period % $len;
			$narr[$key]=$buf;
		}

		$str='';
		$names=self::TimePeriodFormWerbs($form_werb);

		foreach($narr as $key => $int){
			if($int<=0)continue;

			$name='unknown';
			$lem=(self::TimePeriodLemattaInt($int)-1);

			if(array_key_exists($key, $names)){
				if(array_key_exists($lem, $names[$key])){
					$name=$names[$key][$lem];
				}
			}

			$str.=$int.' '.$name.' ';
		}

		return trim($str);
	}

	protected static function TimePeriodFormWerbs($form_werb){
		switch($form_werb){
			case self::FORM_WERB_NOMINATIVE:
				$names=array(
					'year'	=>array('год', 'года', 'лет'),
					'month'	=>array('месяц', 'месяца', 'месяцев'),
					'week'	=>array('неделя', 'недели', 'недель'),
					'day'	=>array('день', 'дня', 'дней'),
					'hour'	=>array('час', 'часа', 'часов'),
					'minute'=>array('минута', 'минуты', 'минут'),
					'second'=>array('секунда', 'секунды', 'секунд'),
				);
				break;

			case self::FORM_WERB_GENITIVE:
				$names=array(
					'year'	=>array('года', 'лет', 'лет'),
					'month'	=>array('месяца', 'месяцев', 'месяцев'),
					'week'	=>array('недели', 'недель', 'недель'),
					'day'	=>array('дня', 'дней', 'дней'),
					'hour'	=>array('часа', 'часов', 'часов'),
					'minute'=>array('минуты', 'минут', 'минут'),
					'second'=>array('секунды', 'секунд', 'секунд'),
				);
				break;

			case self::FORM_WERB_ACCUSATIVE:
				$names=array(
					'year'	=>array('год', 'года', 'лет'),
					'month'	=>array('месяц', 'месяца', 'месяцев'),
					'week'	=>array('неделю', 'недели', 'недель'),
					'day'	=>array('день', 'дня', 'дней'),
					'hour'	=>array('час', 'часа', 'часов'),
					'minute'=>array('минуту', 'минуты', 'минут'),
					'second'=>array('секунду', 'секунды', 'секунд'),
				);
				break;
		}

		return $names;
	}

	protected static function TimePeriodLemattaInt($int){
		$int_a=$int % 10;
		$int_b=(int)($int /10);

		if(($int_b % 10)==1){
			return 3;
		}

		if($int_a==1){
			return 1;
		}

		if($int_a>=2 && $int_a<=4){
			return 2;
		}

		if(($int_a>=5 && $int_a<=9) || $int_a==0){
			return 3;
		}

		return 1;
	}

	static function TZoneGet(){
		if(self::TZoneIsClass()){
			return (int)self::$timezone_shift;
		}else{
			return date_default_timezone_get();
		}
	}

	/**
	 * @param string $timezone ID of the new time zone
	 * @return boolean
	 */
	static function TZoneSet($timezone){
		$tmp=self::TZoneParse($timezone, $type);

		if(!$tmp){
			return false;
		}

		self::$timezone_inited=true;

		if($type==self::$timezone_type){
			if($type==='default'){
				if(self::TZoneGet()===$timezone){
					return true;
				}
			}
		}

		if($type==='default'){
			self::$timezone_type='default';
			return date_default_timezone_set($timezone);
		}else{
			self::$timezone_type='class';
			self::$timezone_shift=(int)$timezone;

			return @date_default_timezone_set('UTC');
		}

		return false;
	}

	/**
	 * @return boolean
	 */
	static function TZoneInit(){
		if(self::$timezone_inited){
			return true;
		}

		$timezone = null;

		if(!empty($timezone)){
			self::$timezone_inited=true;

			return self::TZoneSet($timezone);
		}else{
			return false;
		}
	}

	/**
	 * @param string $timezone
	 * @param string $type
	 * @return boolean
	 */
	protected static function TZoneParse($timezone, &$type){
		if(preg_match('#^[\+|\-]?\d{2,4}$#', $timezone)){
			$type='class';
			return true;
		}elseif(preg_match('#^([A-Za-z0-9\_]+\/?){2,3}$#', $timezone)){
			$type='default';
			return true;
		}else{
			return false;
		}
	}

	/**
	 * @return bollean
	 */
	protected static function TZoneIsClass(){
		return (self::$timezone_type==='class');
	}

	/**
	 * @return integer
	 */
	static function TZoneShift(){
		static $default_tzone=null;
		static $default_shift=null;

		self::TZoneInit();

		if(self::TZoneIsClass()){
			return (((int)self::$timezone_shift)*60);
		}else{
			$currnet_tz=self::TZoneGet();
			if($default_tzone!==$currnet_tz){
				$tmp=date('Z', self::TimeUnix());
				$default_tzone=$currnet_tz;
				$default_shift=$tmp;
			}

			return $default_shift;
		}
	}



	/**
	 * @return integer
	 */
	static function TimeUnix(){
		return time();
	}

	/**
	 * @param boolean $add_time
	 * @return string
	 */
	static function TimeUser($add_time=true){
		return self::ConvUnix2User(self::TimeUnix(), $add_time);
	}

	/**
	 * @return string
	 */
	static function TimeSql(){
		return self::ConvUnix2Sql(self::TimeUnix());
	}

	static function ConvSql2User($sql, $return_part=self::USER_DATETIME){
		if(!self::IsValidSql($sql)){
			return false;
		}

		return self::ConvUnix2User(self::ConvSql2Unix($sql), $return_part);
	}

	static function ConvUser2Sql($User){
		if(!self::IsValidUser($User)){
			return false;
		}

		return self::ConvUnix2Sql(self::ConvUser2Unix($User));
	}

	static function ConvUnix2Sql($unix){
		if(!self::IsInt($unix)){
			$unix=0;
		}

		$time=gmdate('Y-m-d H:i:s', $unix);

		return $time;
	}

	static function ConvSql2Unix($sql){
		if(!self::IsValidSql($sql, $parse))return 0;

		$year	=$parse[1];
		$month	=$parse[2];
		$day	=$parse[3];

		$hour	=$parse[5];
		$minute	=$parse[6];
		$second	=$parse[7];

		$newtime=gmmktime($hour, $minute, $second, $month, $day, $year);
		if($newtime){
			return $newtime;
		}else{
			return 0;
		}
	}

	public static function ConvUser2Unix($user){
		if(!self::IsValidUser($user, $parse))return 0;

		$day	=$parse[1];
		$month	=$parse[2];
		$year	=$parse[3];

		$hour	=$parse[5];
		$minute	=$parse[6];
		$second	=$parse[7];


		if(self::TZoneIsClass()){
			$newtime=gmmktime($hour, $minute, $second, $month, $day, $year);
			$newtime=$newtime-self::TZoneShift();
		}else{
			$newtime=mktime($hour, $minute, $second, $month, $day, $year);
		}

		if($newtime>=0){
			return $newtime;
		}else{
			return 0;
		}
	}

	public static function ConvUnix2User($unix, $return_part=self::USER_DATETIME){
		if(!self::IsInt($unix)){
			$unix=0;
		}

		self::TZoneInit();

		if(self::TZoneIsClass()){
			$unix=$unix+self::TZoneShift();

			switch($return_part){
				case self::USER_DATETIME: return gmdate('d/m/Y H:i:s', $unix);
				case self::USER_DATE: return gmdate('d/m/Y', $unix);
				case self::USER_TIME: return gmdate('H:i:s', $unix);

				default: return gmdate('d/m/Y H:i:s', $unix);
			}
		}else{
			switch($return_part){
				case self::USER_DATETIME: return date('d/m/Y H:i:s', $unix);
				case self::USER_DATE: return date('d/m/Y', $unix);
				case self::USER_TIME: return date('H:i:s', $unix);

				default: return date('d/m/Y H:i:s', $unix);
			}
		}
	}

	/**
	 *
	 * @param integer $year
	 * @param integer $month
	 * @param integer $day
	 * @param integer $hour
	 * @param integer $minute
	 * @param integer $second
	 * @return boolean
	 */
	protected static function IsValidDate($year, $month, $day, $hour, $minute, $second){
		if(!self::IsInt($day) || $day<1 || $day>31)return false;
		if(!self::IsInt($month) || $month<1 || $month>12)return false;
		if(!self::IsInt($year) || $year<1500 || $year>3000)return false;

		if(!self::IsInt($hour) || $hour<0 || $hour >23)return false;
		if(!self::IsInt($minute) || $minute<0 || $minute >59)return false;

		//Не в каждой минуте 60 секунд. Может оказаться 59 и 61.
		//Подобное возможно из-за "Секунды координации"
		if(!self::IsInt($second) || $second<0 || $second >60)return false;

		if(!checkdate($month, $day, $year))return false;

		return true;
	}

	/**
	 * @param string $sql
	 * @param array $parse
	 * @return boolean
	 */
	public static function IsValidSql($sql, &$parse=array()){
		if(!is_string($sql)){
			return false;
		}

		if(!preg_match('#^([0-9]{4})\-([0-9]{1,2})\-([0-9]{1,2})( ([0-9]{1,2})\:([0-9]{1,2})\:([0-9]{1,2}))#i', $sql, $parse)){
			return false;
		}

		$year	=$parse[1];
		$month	=$parse[2];
		$day	=$parse[3];

		if(!empty($parse[4])){
			$hour	=$parse[5];
			$minute	=$parse[6];
			$second	=$parse[7];
		}else{
			$parse[4]=0;
			$parse[5]=0;
			$parse[6]=0;
			$parse[7]=0;

			$hour	=0;
			$minute	=0;
			$second	=0;
		}

		return self::IsValidDate($year, $month, $day, $hour, $minute, $second);
	}

	/**
	 * @param string $user
	 * @param array $parse
	 * @return boolean
	 */
	public static function IsValidUser($user, &$parse=array()){
		if(!is_string($user)){
			return false;
		}

		if(!preg_match('#^([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{4})( ([0-9]{1,2})\:([0-9]{1,2})\:([0-9]{1,2}))?$#i', $user, $parse)){
			return false;
		}

		$day	=$parse[1];
		$month	=$parse[2];
		$year	=$parse[3];

		if(!empty($parse[4])){
			$hour	=$parse[5];
			$minute	=$parse[6];
			$second	=$parse[7];
		}else{
			$parse[4]=0;
			$parse[5]=0;
			$parse[6]=0;
			$parse[7]=0;

			$hour	=0;
			$minute	=0;
			$second	=0;
		}

		return self::IsValidDate($year, $month, $day, $hour, $minute, $second);
	}


	public static function SearchDayStart($unix = null, $return_gmtime = false){
		if(is_null($unix) || !self::IsInt($unix)){
			$unix=self::TimeUnix();
		}

		if($return_gmtime){
			$a=explode('/', gmdate('d/m/Y', $unix));
		}elseif(self::TZoneIsClass()){
			$unix=$unix + self::TZoneShift();

			$a=explode('/', gmdate('d/m/Y', $unix));
		}else{
			$a=explode('/', date('d/m/Y', $unix));
		}

		$day	=$a[0];
		$month	=$a[1];
		$year	=$a[2];

		if($return_gmtime){
			return gmmktime(0,0,0, $month, $day, $year);
		}elseif(self::TZoneIsClass()){
			$time=gmmktime(0,0,0, $month, $day, $year);

			$time = $time-self::TZoneShift();

			return $time;
		}else{
			return mktime(0,0,0, $month, $day, $year);
		}
	}

	public static function SearchDayEnd($unix = null, $return_gmtime = false){
		if(is_null($unix) || !self::IsInt($unix)){
			$unix=self::TimeUnix();
		}

		if($return_gmtime){
			$a=explode('/', gmdate('d/m/Y', $unix));
		}elseif(self::TZoneIsClass()){
			$unix=$unix + self::TZoneShift();

			$a=explode('/', gmdate('d/m/Y', $unix));
		}else{
			$a=explode('/', date('d/m/Y', $unix));
		}

		$day	=$a[0];
		$month	=$a[1];
		$year	=$a[2];

		if($return_gmtime){
			return gmmktime(23, 59, 59, $month, $day, $year);
		}elseif(self::TZoneIsClass()){
			$time=gmmktime(23, 59, 59, $month, $day, $year);

			$time = $time-self::TZoneShift();

			return $time;
		}else{
			return mktime(23, 59, 59, $month, $day, $year);
		}
	}

	public static function SearchWeekStart($unix=null){
		if(is_null($unix) || !self::IsInt($unix)){
			$unix=self::TimeUnix();
		}

		if(self::TZoneIsClass()){
			$unix=$unix+self::TZoneShift();
			$tod=gmdate("N", $unix)-1;
		}else{
			$tod=date("N", $unix)-1;
		}

		$mon=strtotime('-'.$tod.' days', $unix);
		$a=getdate($mon);

		if(self::TZoneIsClass()){
			$time=gmmktime(0,0,0, $a['mon'], $a['mday'], $a['year']);
			return $time-self::TZoneShift();
		}else{
			return mktime(0,0,0, $a['mon'], $a['mday'], $a['year']);
		}
	}

	public static function SearchWeekEnd($unix=null){
		if(is_null($unix) || !self::IsInt($unix)){
			$unix=self::TimeUnix();
		}

		if(self::TZoneIsClass()){
			$unix=$unix+self::TZoneShift();
			$tod=gmdate("N", $unix)-1;
		}else{
			$tod=date("N", $unix)-1;
		}

		$mon=strtotime('-'.$tod.' days', $unix);
		$a=getdate($mon);

		if(self::TZoneIsClass()){
			$time=gmmktime(23,59,59, $a['mon'], $a['mday']+6, $a['year']);
			return $time-self::TZoneShift();
		}else{
			return mktime(23,59,59, $a['mon'], $a['mday']+6, $a['year']);
		}
	}


	public static function SearchMonthStart($unix = null){
		if(is_null($unix) || !self::IsInt($unix)){
			$unix=self::TimeUnix();
		}

		if(self::TZoneIsClass()){
			$unix=$unix+self::TZoneShift();
			$a=explode('/', gmdate('d/m/Y', $unix));
		}else{
			$a=explode('/', date('d/m/Y', $unix));
		}

		if(self::TZoneIsClass()){
			$time=gmmktime(0,0,0,$a[1],1,$a[2]);
			return $time-self::TZoneShift();
		}else{
			return mktime(0,0,0,$a[1],1,$a[2]);
		}
	}


	public static function SearchMonthEnd($unix=null){
		if(is_null($unix) || !self::IsInt($unix)){
			$unix=self::TimeUnix();
		}

		if(self::TZoneIsClass()){
			$unix=$unix+self::TZoneShift();
			$a=explode('/', gmdate('t/m/Y', $unix));
		}else{
			$a=explode('/', date('t/m/Y', $unix));
		}

		if(self::TZoneIsClass()){
			$time=gmmktime(23,59,59, $a[1], $a[0], $a[2]);
			return $time-self::TZoneShift();
		}else{
			return mktime(23,59,59, $a[1], $a[0], $a[2]);
		}
	}

	public static function IsInt($value){
		return ((is_numeric($value) && $value>0) || (is_string($value) && preg_match('#^[0-9]+$#', $value)));
	}
}
