<?php

use Pubnub\Pubnub;



class PubnubHelper
{
	private static $pubnub = null;

	/**
	 *
	 * @param type $param
	 * @return \Pubnub\Pubnub
	 */
	public static function instance()
	{
		if (is_null(self::$pubnub)) {
			self::$pubnub = new Pubnub(
				Yii::app()->params['pubnub-pub-key'],
				Yii::app()->params['pubnub-sub-key'],
				Yii::app()->params['pubnub-sec-key'],
				false
			);
		}

		return self::$pubnub;
	}

	/**
	 *
	 * @param type $chatId
	 * @param type $data
	 * @return boolean
	 */
	public static function publish($chatId, $message, $userId)
	{
/**
String id;
String chatName;
int authorId;
String text;
String photo;
long timestamp;
*/
		$chatName = '#chat' . $chatId;
		
		$data = [
			'id'		 => self::generateRandomString(),
			'chatName'	 => $chatName,
			'authorId'	 => $userId,
			'text'		 => $message,
			'phote'		 => '',
			'timestamp'	 => DateHelper::TimeUnix(),
		];

		try {
			$result = self::instance()->publish($chatName, $data);
		} catch (Exception $ex) {
			Yii::log($ex->getCode() . ': '. $ex->getMessage(), CLogger::LEVEL_WARNING);
			
			if ($ex->getPrevious()) {
				$ex = $ex->getPrevious();
				Yii::log($ex->getCode() . ': '. $ex->getMessage(), CLogger::LEVEL_WARNING);
			}
			
			$result = false;
		}


		if (!$result) {
			return false;
		}

		return true;
	}

	private static function generateRandomString($length = 32) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';

		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		
		return $randomString;
	}
}
