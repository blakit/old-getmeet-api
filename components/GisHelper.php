<?php

class GisHelper extends CComponent
{
	public static function sqlRadius(Location $location, $columnName = 'location')
	{
		return "ST_DWithin(" . $location->toSqlString() . ", " . $columnName . ", '" . addslashes($location->radius) . "', TRUE)";
	}

}