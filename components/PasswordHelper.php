<?php

class PasswordHelper extends CPasswordHelper
{
	public static function hashPassword($password,$cost=13)
	{
		$hash = password_hash($password, PASSWORD_BCRYPT);

		if (!$hash) {
			throw new RuntimeException();
		}

		return $hash;
	}

	public static function verifyPassword($inputPassword, $storedPassword)
	{
		return password_verify($inputPassword, $storedPassword);
	}

	public static function randomString($length = 20) {
		$characters			 = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength	 = strlen($characters);
		$randomString		 = '';
		
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[mt_rand(0, $charactersLength - 1)];
		}
		
		return $randomString;
	}

	public static function isHash($hash) {
		return (preg_match('#^\$2a\$.{56}$#i', $hash));
	}
}