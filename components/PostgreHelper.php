<?php

class PostgreHelper extends CApplicationComponent {

	public static function convSqlToArray($string, $multidemension = false)
	{
		if ($multidemension) {
			return self::pg_array_parse_multidemension($string);
		} else {
			return self::pg_array_parse($string);
		}
	}

	public static function convArrayToSql($array)
	{
		return self::to_pg_array($array);
	}

	private static function pg_array_parse($literal)
	{
		if ($literal == '') return;
		preg_match_all('/(?<=^\{|,)(([^,"{]*)|\s*"((?:[^"\\\\]|\\\\(?:.|[0-9]+|x[0-9a-f]+))*)"\s*)(,|(?<!^\{)(?=\}$))/i', $literal, $matches, PREG_SET_ORDER);
		$values = [];
		foreach ($matches as $match) {
			$values[] = $match[3] != '' ? stripcslashes($match[3]) : (strtolower($match[2]) == 'null' ? null : $match[2]);
		}
		return $values;
	}

	private static function to_pg_array($set) {
		settype($set, 'array'); // can be called with a scalar or array
		$result = array();
		foreach ($set as $t) {
			if (is_array($t)) {
				$result[] = to_pg_array($t);
			} else {
				$t = str_replace('"', '\\"', $t); // escape double quote
				if (! is_numeric($t)) // quote only non-numeric values
					$t = '"' . $t . '"';
				$result[] = $t;
			}
		}
		return '{' . implode(",", $result) . '}'; // format
	}

	private static function pg_array_parse_multidemension($s, $start = 0, &$end = null) {
		if (empty($s) || $s[0] != '{') return null;
		$return = array();
		$string = false;
		$quote='';
		$len = strlen($s);
		$v = '';
		for ($i = $start + 1; $i < $len; $i++) {
			$ch = $s[$i];

			if (!$string && $ch == '}') {
				if ($v !== '' || !empty($return)) {
					$return[] = $v;
				}
				$end = $i;
				break;
			} elseif (!$string && $ch == '{') {
				$v = pg_array_parse($s, $i, $i);
			} elseif (!$string && $ch == ','){
				$return[] = $v;
				$v = '';
			} elseif (!$string && ($ch == '"' || $ch == "'")) {
				$string = true;
				$quote = $ch;
			} elseif ($string && $ch == $quote && $s[$i - 1] == "\\") {
				$v = substr($v, 0, -1) . $ch;
			} elseif ($string && $ch == $quote && $s[$i - 1] != "\\") {
				$string = false;
			} else {
				$v .= $ch;
			}
		}

		return $return;
	}

}
