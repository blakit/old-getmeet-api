<?php

class JsonHelper extends CApplicationComponent {

	public static function encode($data)
	{
		try {
			return json_encode($data, JSON_UNESCAPED_UNICODE);
		} catch (Exception $ex) {
			throw  $ex->getPrevious();
		}
		
	}

	public static function decode($data)
	{
		if (!is_scalar($data)) {
			return false;
		}

		try {
			$result = json_decode($data, true, JSON_UNESCAPED_UNICODE);

			if (json_last_error()) {
				throw new Exception(json_last_error_msg(), json_last_error());
			}
			
		} catch (Exception $ex) {
			Yii::log(__METHOD__ . ': [' . $ex->getCode() . '] ' . $ex->getMessage(), 'error');
			
			return false;
		}

		return $result;
	}

}
